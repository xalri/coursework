#!/bin/zsh

docker run -P -v /home/xal/js/:/app 20596d031029 wintersmith build
cd build
sed -i 's/src="/src="./g' ./*.html
sed -i 's/href="/href="./g' ./*.html
cd ..
