---
title: 1.1 Operators
template: index.jade
---

## Equality

In JavaScript, `5 == "5"`. The `==` operator is a type-converting equality
operator, it will try to convert the operands to be the same type then check
if they're equal.

Boolean values can be cast (implicitly) to and from numbers and even strings,
so `'0' == false`.

Javascript has both a value representing nothing (the keyword `null`, which is
an object), and a value representing an uninitialised variable
(the keyword `undefined`, which is not an object and has no type).

```javascript
    null == undefined` // This is true, `undefined` is cast to `null`.
```
Sometimes people write `if (foo == null)` to check if a value has been initialised
(`undefined` will be cast to `null`, so this works, but also includes the case
where the value has been explicitly set to null). A better way to check if
a value is initialised is `if (foo)`.

There is another operator, `===`, which implements strict equality, and is much
more intuitive.

```javascript
    null === undefined; // false
    0 === '0'           // false
    0 === 0             // true
```

## Comparison

Other comparison operators works as you'd expect:

```javascript
    5 < 5   // false
    5 <= 5  // true
```

## String Concatenation

One of the good things about implicit type casting is that it makes building
strings easy. You can use the `+` operator to add things to strings:

```javascript
    "a number: " + 3 + ", an array: " + ["a","b","c"];
    // Evaluates to "a number: 3, an array: a,b,c"
```

## Arithmetic operators

Javascript has all the basic mathematical operators, here's a quick reference:

```javascript
    a + b;    // (where a and b are numbers) addition
    a - b;    // subtraction
    a * b;    // multiplication
    a / b;    // division
    a % b;    // modulus
              // Assignment operators exist for all the above:
              // +=, -=, *=, /=, and %=

    a++;      // increment
    a--;      // decrement
```

As is usual with JavaScript, there are a couple of quirks here:

 * `+` is also used for string concatenation, so if either of the operators
   is not a number, it doesn't perform addition.
 * Subtracting a string from a string gives NaN (Not A Number), because JavaScript.
 * Subtracting a number from a string casts the string to a number.
