---
title: 1.3 Functions
template: index.jade
---

Functions are a special type of Object. As `Object`s, you can do `Object`y
things like assigning functions to variables or passing functions as
parameters to other functions.

Functions can be declared in three ways:

```javascript
    function foo(a, b){ /*...*/ }

    let bar = function(a, b){ /*...*/ }

    let baz = (a,b) => { /*...*/ }
```

With the arrow syntax, you can ommit the braces if you only need a single
statement, and the brackets if you have one parameter

```javascript
    let double = x => 2 * x;
    double(4) // 8
```

### Default parameters

