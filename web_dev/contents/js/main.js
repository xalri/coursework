function toggleNav(){
  var nav = window.getComputedStyle(document.getElementById('nav'));
  if (nav.visibility == "hidden"){
    document.getElementById('nav').style.visibility = "visible";
    document.getElementById('main').style.paddingLeft = "320px";
  } else {
    document.getElementById('main').style.paddingLeft = "20px";
    document.getElementById('nav').style.visibility = "hidden";
  }
}
