---
title: The JavaScript Language
template: index.jade
---

Hello world! This is a website about javascript >_>

```javascript
  var pages = [];
  for(var p in contents){
    if(p != 'css' && p != 'js' && p != 'index.md'){
      pages.push(contents[p]);
    }
  }
  pages.sort(function(p1,p2){return p1.name < p2.name;})
  pages.unshift(contents['index.md']);

  var navNo = function(title){ return (title.split(' '))[0]; };
  var navTitle = function(title){ return title.split(' ').splice(1).join(' '); }

  var makeNavTitle = function(title){
    if(title[1] != '.'){ return title; }
    return "<span class='number'>" + navNo(title) + "</span> " + navTitle(title);
  };
```
