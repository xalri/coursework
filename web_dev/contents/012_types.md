---
title: 1.2 Type System
template: index.jade
---

There are 6 types of data in JavaScript:

 * Numbers, represented as a double precision float;
 * Strings, which are usually UTF-16;
 * Booleans, true or false;
 * Objects;
 * Symbols, unique and immutable types used as identifiers;
 * and `undefined`.

Note that there is no integer type.

There are also a few types of Object which are integrated deeply enough
into the language that they might as well be their own types:

 * Arrays,
 * Functions,
 * and Regular expressions.

You can get the type of a variable using the `typeof` operator.

```javascript
    let x;
    typeof x;          // "undefined"
    typeof "Hello o/"; // "string"
```

## Objects

Objects are collections of named variables:

```javascript
    let email = {
        recipient: "name@example.com",
        subject:   "Hello",
        body:      "Hello World o/"
    };

    let point = { x: 0, y: 0 };
    point.x = 10;
    point.y = 15;
```

Name:value pairs are called properties, in the above example you would say
that `point` has properties `x` and `y`.

Properties can be of any type, including functions. Functions inside objects
are called methods. Methods have access to the other properties of the object
they're in through the `this` keyword.

```javascript
    let square = {
        length: 0,
        perimeter: function(){
            return 4 * this.length;
        }
    }

    square.length = 10;
    console.log(square.perimeter()); // 40

    // Properties can also be accessed with the notation `object["property"]`.
    square["length"] = 5;
```

You can declare empty objects and add properties to them later.

```javascript
    let foo = {};
    foo.newProperty = "Bar";
```

The names of object properties can be computed dynamically:

```javascript
    let foo = {
        ["greeting" + "1"]: "Hello World",
        [bar()]: 42
    };
```

## Arrays

Arrays are collections of values. Values in arrays are called elements, and can
be accessed with their position in the array.

Arrays are zero-indexed, the first element has the index 0, the second 1, and
so on.

```javascript
    let foo = [2, 3, 5, 7, 11, 13];
    console.log(foo[3]); // 11

    let bar = [];
    bar.push(3);
    bar.push(6);
    console.log(bar); // "3,6"
```

You can create an array with a predefined length using `new Array(length)`.
All the values start as `undefined`.

Using destructuring assignment you can get multiple variables out of arrays
in one statement:

```javascript
    let foo = [2, 3, 5];
    let [a, b, c] = foo; // a = 2, b = 3, c = 5
```
