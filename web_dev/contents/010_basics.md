---
title: 1.0 Basic Syntax & Semantics
template: index.jade
---

JavaScript shares much of it's syntax with languages like Java and C (blocks
of code are defined with `{` and `}`, statements end in `;`, and so on).

## Variables

Variables in JavaScript are declared with the keyword `let`. Variables are
mutable, and can be changed using the `=` operator. You can assign a value
to a variable in the same statement you declare it.

```javascript
    let x;     // Declaring a variable `x`
    x = 10;

    let y = 5; // Declaring a variable 'y' with the value 5
    y = 4;     // Change the value of y to 4

    // You can declare and initialise multiple variables in a
    // single statement
    let a = 5, b = 10, c = 15;
```

When you declare a variable with `let`, it's scope is the enclosing block:

```javascript
function foo(){
    // `i` is not visible here
    for(let i = 0; i < 5; i++){
        // `i` exists inside the for loop
    }
    // After the loop, `i` is not avaliable
}
```

### `var` keyword

There's another way to declare variables with a slightly diffferent behaviour,
the `var` keyword. Variabled declared with `var` are scoped to the *function*,
rather than the *enclosing block*, and are always available from the start
of the function.

```javascript
function foo(){
    // x exists here, but it doesn't have any value.
    // This is called variable hoisting - variables declared with
    // var are availiable through the function, even before they're
    // declared.
    if(true){
        var x = 10;
    }
    console.log("x is "+x); // x is visible outside the if block.
}
```

When you declare a variable using `var`, it's always 'hoisted' to the top of
the function, so the example above is equivilent to:

```javascript
    function foo(){
        var x;
        if(true){
            x = 10;
        }
        console.log("x is "+x);
    }
```

It's better to use `let` for temporary variables inside functions, the behaviour
is much more intuitive.

### Global variables

It's possible to use variables without declaring them.

```javascript
    x = 42;
```

Variables created like this have global scope, so it's considered bad practice
to do this - and is more often than not a source of bugs rather than an
intentional behaviour.

Global variables can also be created using the `var` keyword in global scope:

```javascript
var x = 42;
function foo(){
    console.log(x); // x is global, it can be accessed from anywhere
}
```

### Variable types

Note that we don't use types to declare variables, just `let`. Types in JavaScript
are loose, meaning the type of a variable can change. (See the later section on
types for more information)

```javascript
    let x = 5;       // Here, x is a number
    x = "hello o/";  // Now it's a string.
```

We also have implicit type coercion:
```javascript
    let x = 5;
    console.log("x is: " + x);  // x is converted into a string
```

## Functions

Functions are (usually) declared and called like this:
```javascript
    function double(x){
        return x * 2;
    }
    double(5); // == 10
```

Functions are first-class in JavaScript, you can do interesting things with them
like assigning them to variables, using them as function parameters, and storing
them in data structures, more on this later.

## Control Flow

### If statement

As you'd expect, the `if` statement branches your program based on whether or
not the expression inside the brackets is true.

```javascript
    let x = 5;
    if (x == 1){
        // x is 1
    } else {
        // x is not 1
    }
```

You don't need to braces if it's only a single statement, this is useful for
for else if:

```javascript
    if (x == 1)
        x += 1;
    else if (x == 5)
        x -= 1;
```

### Switch statement

If you want to do a different thing for many possible values of an expression,
you can use the switch statement:


```javascript
    switch(x){
        case 1: /* x is 1 */ break;
        case 2: /* x is 2 */ break;
        /* ... */
    }
```

### Loops

There are two types of `for` loop, the C style (where you declare a variable,
a condition, and a statement to run each iteration), and iterating over
every value in an array or object.

```javascript
    for (let i = 0; i < 5; i++){
        // runs for i = 0,1,2,3, and 4
    }

    let fruit = ["Apple", "Banana", "Pear"]; // An array
    for (let f in fruit){
        // f is "Apple", then "Banana", etc..
    }
```

`while` and `do` are simple loop structures, `while` is evaluated while the
expression in the brackets is true, for `do while` the expression is checked
at the end of the code inside, so it always runs at least once.

```javascript
    while(true){
        // Will it ever end..?
    }

    do{
        console.log("Hello o/");
    } while(false)
```
