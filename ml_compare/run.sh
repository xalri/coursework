#!/bin/bash
java \
   -cp out:$(find ./lib -name "*.jar" | tr "\n" ":") \
   Biocomputing.Control $*
