
package Biocomputing;
import io.jenetics.*;
import io.jenetics.engine.Engine;
import io.jenetics.engine.EvolutionResult;
import io.jenetics.ext.SingleNodeCrossover;
import io.jenetics.ext.util.Tree;
import io.jenetics.prog.ProgramChromosome;
import io.jenetics.prog.ProgramGene;
import io.jenetics.prog.op.*;
import io.jenetics.util.ISeq;
import io.jenetics.engine.Codec;
import io.jenetics.util.RandomRegistry;
import org.math.plot.Plot2DPanel;

import javax.swing.*;
import java.util.*;
import java.util.function.Function;

/** A classifier based on genetic programming */
public class ClassifierGP extends Classifier implements Cloneable {
	List<Integer> classes;
	ProgramGene<Double> program;
	List<Util.Range> attrRange; // Used to plot the data

	/** The operations to use in our programs */

	private ClassifierGP() { }

	/** Map the result of the program to a class. */
	public static int getClass(double x, List<Integer> classes) {
		return classes.get(Math.max(Math.min((int)(x * classes.size() - 0.5), classes.size() - 1), 0));
	}

	/** Create a GP classifier from some training data */
	public static ClassifierGP train(InstanceSet training) {
		List<Integer> classes = Util.listClasses(training);
		List<Util.Range> attrRange = Util.attrRange(training);

		// Leaf nodes: one for each input attribute and one random value.
		List<Op<Double>> leavesList = new ArrayList<>();
		for(int i = 0; i < training.getInstances()[0].numAtt - 1; i++) {
			leavesList.add(Var.of(Character.toString((char)('a' + i)), i));
		}
		leavesList.add(EphemeralConst.of(() -> (RandomRegistry.getRandom().nextDouble() - 0.5) * 5));
		final ISeq<Op<Double>> leaves = ISeq.of(leavesList);

		// Define the error function. Takes a program and returns the number of elements
		// of the training set it mis-classifies.
		Function<ProgramGene<Double>, Double> error = program -> Arrays.stream(training.getInstances())
				  .mapToDouble(sample -> {
				  	  // Create a Double[] of the attributes
					  Double[] attrs = Arrays.stream(sample.getRealAttributes()).boxed().toArray(Double[]::new);

					  int expected = sample.getClassValue();
					  int actual = getClass(program.eval(attrs), classes);

					  return (actual == expected ? 0 : 1);
				  }).sum() + program.size() * 0.01;

		// Create a Codec.
		Codec<ProgramGene<Double>, ProgramGene<Double>> codec = Codec.of(
				  Genotype.of(ProgramChromosome.of(
							 5, // Initial tree depth
							 f -> f.getRoot().size() <= 500, // Chromosome validator
							 // Operators:
							 ISeq.of(
										new Op<>() {
											@Override public String name() { return "?"; }
											@Override public int arity() { return 3; }
											@Override public Double apply(Double[] doubles) {
												return doubles[0] > 0 ? doubles[1] : doubles[2];
											}
											@Override public String toString() { return "?"; }
										},
										new Op<>() {
											@Override public String name() { return "length"; }
											@Override public int arity() { return 2; }
											@Override public Double apply(Double[] doubles) {
												return Math.sqrt(doubles[0] * doubles[0] + doubles[1] * doubles[1]);
											}
											@Override public String toString() { return "length"; }
										},
										MathOp.ADD,
										MathOp.SUB,
										MathOp.MUL,
										MathOp.SIN
							 ),
							 leaves
				  )),
				  Genotype::getGene);

		// Build the Engine
		Engine<ProgramGene<Double>, Double> engine = Engine
				  .builder(error, codec)
				  .populationSize(4000)
				  .offspringFraction(0.65)
				  .alterers(
							 new Mutator<>(),
							 new SingleNodeCrossover<>()
				  )
				  .minimizing()
				  .build();

		ProgramGene<Double> program = engine.stream()
				  .limit(100)
				  .peek(result -> System.out.println(result.getBestFitness()))
				  .collect(EvolutionResult.toBestGenotype())
				  .getGene();

		// Create a classifier from the best program.
		ClassifierGP classifier = new ClassifierGP();
		classifier.program = program;
		classifier.classes = classes;
		classifier.attrRange = attrRange;

		return classifier;
	}

	public int classifyInstance(Instance ins) {
		Double[] attrs = Arrays.stream(ins.getRealAttributes()).boxed().toArray(Double[]::new);
		return getClass(program.eval(attrs), classes);
	}

	public void printClassifier() {
		System.out.println(Tree.toString(program));
	}

	public int classifierClass() { return -1; }

	/** Plots the result of the program for the range of the training set.
	 * Only works for 2D data. */
	public void plot2D() {
		Util.plot2D(attrRange, (x, y) -> getClass(program.eval(x, y), classes));
	}
}
