package Biocomputing;

import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.nnet.learning.MomentumBackpropagation;
import org.neuroph.util.TransferFunctionType;

import java.util.Arrays;
import java.util.List;

/** A classifier using a multi-layer perceptron */
public class ClassifierMLP extends Classifier {
	private MultiLayerPerceptron mlp;
	private List<Integer> classes;
	List<Util.Range> attrRange;
	private int inputs;

	private ClassifierMLP() {}

	/** Train an MLP to classify data in some training set */
	public static ClassifierMLP train(InstanceSet data) {
		List<Integer> classes = Util.listClasses(data);
		List<Util.Range> attrRange = Util.attrRange(data);

		// Number of input attributes
		int inputs = classes.size();

		MultiLayerPerceptron mlp = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, inputs, 20, 20, 1);
		mlp.randomizeWeights();

		DataSet training = new DataSet(inputs , 1);
		for(Instance i : data.getInstances()) {
			double[] attrs = Arrays.stream(i.getRealAttributes()).limit(inputs).toArray();
			for(int j = 0; j < inputs; j++) attrs[j] = attrRange.get(j).normalize(attrs[j]);

			training.add(new DataSetRow(attrs, new double[]{
					  ((double) classes.indexOf(i.getClassValue()) + 0.5) / inputs
			}));
		}

		mlp.setLearningRule(new BackPropagation());
		mlp.getLearningRule().setLearningRate(0.5);
		mlp.getLearningRule().setMaxIterations(5000);
		mlp.getLearningRule().setMaxError(0.001);
		mlp.getLearningRule().addListener(event -> {
			BackPropagation bp = (BackPropagation) event.getSource();
			if (event.getEventType() != LearningEvent.Type.LEARNING_STOPPED)
				System.out.println(bp.getCurrentIteration() + ". iteration : "+ bp.getTotalNetworkError());
		});

		System.out.println("Training..");
		mlp.learn(training);

		ClassifierMLP c = new ClassifierMLP();
		c.mlp = mlp;
		c.classes = classes;
		c.inputs = inputs;
		c.attrRange = attrRange;

		 mlp.save("data_1.nnet");

		return c;
	}


	@Override public int classifyInstance(Instance ins) {
		double[] attrs = Arrays.stream(ins.getRealAttributes()).limit(inputs).toArray();
		for(int j = 0; j < inputs; j++) attrs[j] = attrRange.get(j).normalize(attrs[j]);
		mlp.setInput(attrs);
		mlp.calculate();
		double d = mlp.getOutput()[0] * inputs - 0.5;
		return classes.get(Math.min(Math.max((int)Math.round(d), 0), classes.size() - 1));
	}

	@Override public void printClassifier() {
		System.out.println(mlp);
		//mlp.getWeights()
	}

	@Override public int classifierClass() {
		return -1;
	}

	/** Plots the result of the program for the range of the training set.
	 * Only works for 2D data. */
	public void plot2D() {
		Util.plot2D(attrRange, (x, y) -> {
			mlp.setInput(attrRange.get(0).normalize(x), attrRange.get(1).normalize(y));
			mlp.calculate();
			double d = mlp.getOutput()[0] * inputs - 0.5;
			return Math.min(Math.max((int)Math.round(d), 0), classes.size() - 1);
		});
	}
}
