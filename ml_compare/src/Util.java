package Biocomputing;

import org.math.plot.Plot2DPanel;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

class Util {
	static class Range {
		Range(double min, double max) { this.min = min; this.max = max; }
		double min, max;

		public double normalize(double val) {
			return (val - min) / (max - min);
		}
	}

	static List<Integer> listClasses(InstanceSet training) {
		// Search through the samples to create a list of classes
		return Arrays.stream(training.getInstances())
				  .map(Instance::getClassValue)
				  .distinct()
				  .collect(Collectors.toList());
	}

	static List<Range> attrRange(InstanceSet training) {
		List<Integer> classes = listClasses(training);

		// The range of each attribute, used to plot the result.
		List<Range> attrRange = classes.stream().map(_x -> new Range(1000, -1000)).collect(Collectors.toList());
		for (Instance i : training.getInstances()) {
			for (int j = 0; j < classes.size(); j++) {
				double attr = i.getRealAttribute(j);
				if (attr < attrRange.get(j).min) attrRange.get(j).min = attr;
				if (attr > attrRange.get(j).max) attrRange.get(j).max = attr;
			}
		}

		return attrRange;
	}

	/** Plots the result of the program for the range of the training set.
	 * Only works for 2D data. */
	static void plot2D(List<Range> attrRange, BiFunction<Double, Double, Integer> f) {
		System.out.println("Starting plot");
		Plot2DPanel plot = new Plot2DPanel();
		System.out.println("etc");

		int size = 200;

		List<Double> XA = new ArrayList<>();
		List<Double> YA = new ArrayList<>();
		List<Double> XB = new ArrayList<>();
		List<Double> YB = new ArrayList<>();

		double xmin = attrRange.get(0).min;
		double xmul = attrRange.get(0).max - attrRange.get(0).min;
		double ymin = attrRange.get(1).min;
		double ymul = attrRange.get(1).max - attrRange.get(1).min;

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				double x = xmin + xmul * i / (double) size;
				double y = ymin + ymul * j / (double) size;
				int c = f.apply(x, y);
				if (c == 0) { XA.add(x); YA.add(y); }
				else { XB.add(x); YB.add(y); }
			}
		}

		if(XA.size() > 0)
			plot.addScatterPlot("class0",
					  XA.stream().mapToDouble(x->x).toArray(),
					  YA.stream().mapToDouble(x->x).toArray());

		if(XB.size() > 0)
			plot.addScatterPlot("class1",
					  XB.stream().mapToDouble(x->x).toArray(),
					  YB.stream().mapToDouble(x->x).toArray());

		plot.setAutoBounds();

		JFrame frame = new JFrame("plot");
		frame.setContentPane(plot);
		frame.setVisible(true);
	}
}
