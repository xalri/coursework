---
title: "Comparing Nature-Inspired Classification Algorithms"
author: "Lewis Hallam"
geometry: margin=2.8cm
fontsize: 11pt
output: pdf_document
abstract: |
    Genetic programming and multi-layer perceptions are compared for
    use in classification. Genetic programming fails to find an
    accurate classifier for the given data. In contrast, an MLP trained
    in 1/10th the time gives a high accuracy. Visualisations show that the
    MLP has knowledge of the patterns in the data, where the genetic program
    does not.
---

# Genetic Programming

## Justification

Arbitrary functions can be defined as trees of operators and variables, called
program trees.
The nodes of the tree represent operators, they take values from their children as input
and produce another value as output. The leaves are variables or constants. To evaluate
a tree given some values for the variables, the root node is evaluated, which recursively
evaluates it's children to produce a value.
A program tree with a particular set of operators is capable of approximating any
function

In genetic programming a genetic algorithm is used to evolve a program
tree to perform some task.

Genetic algorithms are often used for classification tasks, so genetic programming was
chosen as one of the algorithms to evaluate in this project. The advantage of program
trees as a knowledge representation is that it's easy to understand the knowledge they
encode.

## Implementation

The implementation is based on the Java library `jenetics`, and it's genetic programming
module `jenetics.prog`.

A new class is added, `ClassifierGP`. An instance of the class contains a program tree used
to classify elements. The program uses continuous values, so `ClassifierGP` has a static
method to map a double to a class.
To classify an element we evaluate the `program`, passing the sample's attributes as the 
program variables, then map the result to a class.

`ClassifierGP` includes a static method which uses `jenetics` to construct a classifier using
a given training set. 

We define the set of possible leaf nodes to include a variable representing each input attribute
of the instances of the training set, and a node representing a random value. The range of the
random node can be changed.
The error function is defined as a lambda, which counts the number of instances of the training
set a given program incorrectly classifies. A penalty is also applied based on the size of the
program tree, we add some multiple of `program.size()` to the error value. A `Codec` is 
initialised which defines the structure of the programs. In the codec we set the initial depth
of the tree, the maximum depth of the tree, and the set of operators used as inner nodes.

Finally we run the genetic algorithm for a set number of iterations and choose the program
which gives the highest fitness (lowest error).

The resultant program can be used to classify samples by evaluating it with given attributes.

## Tuning

To attempt to improve the results the following parameters were tuned:

 - The set of operators used to build program trees: Fewer operators gives
 a much smaller search space, so a higher probability of the genetic algotirhm
 finding a solution with low error, but it needs to be possible to represent
 a solution with the included operators. Adding operators such as the tertiary
 operator(if the first operand is positive return the second, otherwise return
 the third), a continuous 'xor' (<https://stackoverflow.com/a/46654956>), magnitude
 ($sqrt(a^2+b^2)$), etc, increased the time taken to stop improving
 on a solution, and the solution itself didn't use the additional operators.
 
 - The population size. With a larger population size the algorithm converged on
 a solution in fewer iterations, but each iteration was significantly slower.
 The algorithm was able to find a non-linear solution with the population size
 set to 5000.
 
 - The initial tree size. A larger initial tree size slows down the algorithm
 significantly. The program trees produced were larger but the error rate didn't
 change.
 
 - The maximum tree size. A small (~50) maximum tree size always gave a linear
 solution. Increasing the maximum tree size slowed down the algorithm but improved
 the accuracy of the generated classifier significantly.
 
 - The penalty for tree size. The penalty had to be reduced for larger solutions
 to survive, with a high penalty a linear solution was always generated.
 
 - The range of the random values. With larger ranges the algorithm stops improving in the
 same amount of time, but the solutions are slightly more complex without increasing
 the fitness.
 
 - The number of iterations. For a population size of 4000, increasing the number of
 iterations had no effect past 300.

## Results

The algorithm fails to fully capture the pattern in the data.
The series of images below shows
the class the genetic program assigns to sampled points across the range of the training data
for the first data set, with increasing population size and number of iterations left to right. Each
colour represents a different class.

![](gp2.png){width=200px}
![](gp3.png){width=200px}
![](gp4.png){width=200px}

The genetic algorihtm converges on a solution in ~10 minutes, and the solutions correctly classify
90% of the points in the first data set. In the second data set, with extra parameters, the algorithm
converges in the same amount of time and produces a classifier with 87% accuracy.
The produced programs are relatively large trees, so evaluating them is inefficient. 

Better classifiers than this seem to exist in the solution space. The
pattern we observe in the data could be approximated with an equation of
the form $sin(x + a) + by$. With the addition of the tertiary operator
a solution which correctly classifies points in the smaller circles may
be of the form:

```
(a - length(x - b, y - c))?
   class1 :
   (a - length(x - b, y - d))?
      class2 :
      sin(e - y) + fx
```

Where $a, b, c, d, e, f$ are constants.

The genetic algorithm fails to arrive at this solution because it's form is
difficult to arrive at _incrementally_ -- the search space is large and 
sub-trees of the solution will give a low fitness, and are unlikely to survive
for long. If a solution with the correct general form was found the genetic
algorithm could optimize it's parameters, but the only way the form would be
found is by chance. The solution space is too large for that to be feasible.

One possible improvement would be to use the same technique shown in the frameworks'
included example -- multiple classifiers are used with each one handling only one
class and classifying a sub-set of the remaining points.
Normalising the input is another option to explore for improving the results.

To conclude this section, the results of applying genetic programming to this
classification problem were poor.
It's possible that the failure of the algorithm to produce a good classifier is
due to a bad set of parameters and operations. With such a large set of parameters
and possible operations to try I don't think I've managed to explore a
representative sample of the possibilities.

# Multi-Layer Perceptron

## Justification

A multi-layer perceptron is a type of feedforward neural network. MLPs have the ability to
distinguish data which is not linearly seperable: the universal approximation shows that
multi-layer perceptrons can approximate any function.
I chose this algorithm mostly due to the current popularity of neural networks in artificial
intelligence.

The knowledge of an MLP is encoded in the weights of the connections. This has the disadvantage of being
opaque to a person, particularly compared to the programs which could be produced by genetic
programming; It's difficult to extract knowledge of the data from a set of connection weights.

## Implementation

The implementation is much simpler than that for genetic programming. We use the `neuroph`
java library, and simply convert between the formats of the framework and neuroph.

We first set up the MLP, with the number of input attributes giving the number of nodes in
the input layer, a single hidden layer being added with 40 nodes, and a single node in the
output layer.

Then the training set is created from the `InstanceSet` provided by the framework. We normalise
the input and class values to be between 0 and 1. We set the learning rule to be back propegation,
and give a maximum number of iterations of 1000. Calling `.learn` on the MLP with the training
set iteratively updates the weightings to give the best results for the training set. 

To classify a sample, we set the input nodes of the MLP to the sample's input attributes, call
`.compute()`, then map the normalised class value back to an integer class.

## Results

Only minimal tuning was necessary to get accurate results: The maximum number of iterations was
increased from an initial value of 1000 to 10000, which slightly increased the accuracy.
These images show the class the MLP assigns to sampled points across the range of the training data
for the first and fourth data sets.

![](mlp1.png){width=200px}
![](mlp2.png){width=200px}

The MLP clearly encodes some knowledge of the patterns in the data. The time taken to train the
neural network was also less than the time needed for the genetic algorithm to converge on a 
solution. The MLP was trained with 10000 iterations, which took ~45 seconds. It correctly classifies
99% of the first data set and 98% of the second. The additional parameters in the second data set
doesn't increase the time taken to train the MLP, and there's little loss in accuracy. Overall
the performance of multi-layer perceptrons in this classification task is impressive, especially
considering that only minimal tuning was required to produce these results.

# Comparison

The MLP was able to classify the data with a much higher accuracy than the genetic
program, and training was significantly faster.

Algorithm Data set Time(seconds) Accuracy
--------- -------- ------------- --------
GP        1        523s           92.0%
GP        2        403s           87%
GP        3        257s           90.0%
GP        4        284s           92.0%
MLP       1        46s            99.0%
MLP       2        46s            98.0%
MLP       3        85s            98.4%
MLP       4        85s            96.3%

Based on the findings of this project (both the code necessary to implement each algorithm
with java libraries and the results shown above), I would recommend multi-layer perceptrons
over genetic programming for simple classification tasks. Genetic programming's one advantage
of a more easily understood knowledge representation would only apply if the knowledge
encoded in it was useful in the classification task. In contrast MLP classification is
easy to implement and gives accurate results in very little time.

