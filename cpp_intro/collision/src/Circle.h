#pragma once

#include "Shape.h"

class Circle: public Shape {
public:
    float r;

    explicit Circle(float radius = 1.0, Vec pos = Vec{0.0, 0.0}, Vec vel = Vec{0.0, 0.0});

    /// The circle's radius
    float radius() const override;

    /// Test for an intersection with another shape
    bool intersects(const Shape &other) const override;
};

inline std::ostream& operator<<(std::ostream& os, const Circle& c)  {
    os << "Circle{ pos: " << c.position
       << ", vel: " << c.velocity
       << ", radius: " << c.r
       << " }";
    return os;
}
