#include "Circle.h"
#include "Square.h"

Circle::Circle(float radius, Vec pos, Vec vel): Shape(pos, vel), r(radius) {}

/// The circle's radius
float Circle::radius() const { return r; }

/// Test for an intersection with another shape
bool Circle::intersects(const Shape &other) const {
    if (auto* square = dynamic_cast<const Square*>(&other)) {
        // circle-square is implemented by the Square class, delegate to that.
        return other.intersects(dynamic_cast<const Shape&>(*this));
    }
    // The default impl uses bounding circles, so no need to re-implement here.
    // Fall-back to the default for any yet-undefined shapes.
    return Shape::intersects(other);
}
