#pragma once

#include "Shape.h"
#include <iostream>

class Square: public Shape {
public:

    float width, height;

    explicit Square(
            float width = 1.0,
            float height = 1.0,
            Vec pos = Vec{ 0.0, 0.0 },
            Vec vel = Vec{ 0.0, 0.0 });

    /// The box's outer bounding radius
    float radius() const override;

    /// The box's inner bounding radius
    float inner_radius() const;

    /// Test for an intersection with another shape
    bool intersects(const Shape &other) const override;

    /// Get the square's bounding box
    Bounds bounds() const;
};

inline std::ostream& operator<<(std::ostream& os, const Square& s)  {
    os << "Square{ pos: " << s.position
       << ", vel: " << s.velocity
       << ", width: " << s.width
       << ", height: " << s.height
       << " }";
    return os;
}
