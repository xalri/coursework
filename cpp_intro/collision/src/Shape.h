#pragma once

#include "Vec.h"
#include "Bounds.h"

class Shape {
public:
    Vec position, velocity;

    explicit Shape(Vec pos = Vec{ 0.0, 0.0 }, Vec vel = Vec{ 0.0, 0.0 });

    /// Update the shape
    virtual void update(Bounds bounds);

    /// The radius of the shape's bounding circle, used for
    /// broad-phase collision detection
    virtual float radius() const = 0;

    /// Test for an intersection with another shape
    virtual bool intersects(const Shape &other) const;
};

std::ostream& operator<<(std::ostream& os, const Shape& v);
