#include <algorithm>
#include <cstddef>
#include <ctime>
#include <functional>
#include <iostream>
#include <random>
#include <set>
#include <vector>
#include <memory>
#include "Shape.h"
#include "Square.h"
#include "Circle.h"

#ifdef USE_SFML
#include <SFML/Graphics.hpp>
#endif

int main() {
#ifdef USE_SFML
    auto window = sf::RenderWindow{sf::VideoMode{500, 500}, "Shapes"};
    window.setView(sf::View{
            sf::Vector2{50.0f, 50.0f},
            sf::Vector2{100.0f, 100.0f}
    });
    window.setVerticalSyncEnabled(true);
#endif

    auto bounds = Bounds { {0.0f, 100.0f}, {0.0f, 100.0f} };
    auto shapes = std::vector<std::shared_ptr<Shape>>{};

    std::cout << "Generating shapes.." << std::endl;

    auto rng = std::default_random_engine{};
    auto pos_dist = std::uniform_real_distribution<float>{ 0.0f, 100.0f };
    auto vel_dist = std::uniform_real_distribution<float>{ 0.1f, 0.7f };
    auto size_dist = std::uniform_real_distribution<float>{ 6.0f, 10.0f };

    rng.seed(time(0));

    for (int i = 0; i < 20; i++) {
        shapes.push_back(std::make_shared<Square>(
                size_dist(rng),
                size_dist(rng),
                Vec{pos_dist(rng), pos_dist(rng)},
                Vec{vel_dist(rng), vel_dist(rng)}));

        shapes.push_back(std::make_shared<Circle>(
                size_dist(rng) / 2.0f,
                Vec{pos_dist(rng), pos_dist(rng)},
                Vec{vel_dist(rng), vel_dist(rng)}));
    }

    // The set of shapes to be removed at the end of each tick. Kept in
    // descending order so elements are deleted from the back of the array.
    auto to_remove = std::set<std::size_t, std::greater<>>{};

    std::cout << "Running simulation with " << shapes.size() << "shapes.." << std::endl;

    while(shapes.size() > 1) {
        for (auto& shape : shapes) shape->update(bounds);

        // To reduce the number of collisions we need to test, we sort the shapes
        // by ascending minimum x position. (The sorting itself is relative cheap,
        // libc++ does ~2N comparisons for unchanged data)
        std::sort(shapes.begin(), shapes.end(), [](auto &a, auto &b) -> bool {
            return a->position.x - a->radius() < b->position.x - b->radius();
        });

        to_remove.clear();
        for(std::size_t i = 0; i < shapes.size(); i++) {
            // We check for collisions starting at the next left-most until the
            // shapes are separated by a distance greater than this shape's
            // diameter.
            auto d = shapes[i]->radius() * 2;
            for(auto j = i + 1; j < shapes.size(); j++) {
                if (shapes[j]->position.x - shapes[i]->position.x > d) break;

                if (shapes[i]->intersects(*shapes[j])) {
                    std::cout << "Collision: " << std::endl
                              << *shapes[i] << std::endl
                              << *shapes[j] << std::endl;

                    to_remove.insert(i);
                    to_remove.insert(j);
                    break;
                }
            }
        }

        // Remove collided shapes, starting at the end of the array
        for(std::size_t i : to_remove) { shapes.erase(shapes.begin() + i); }

#ifdef USE_SFML
        window.clear();
        for (std::size_t i = 0; i < shapes.size(); i++) {
            auto& shape = shapes[i];
            if (auto *square = dynamic_cast<const Square *>(&*shape)) {
                auto r = sf::RectangleShape{sf::Vector2{square->width, square->height}};
                r.setOrigin(square->width / 2.0f, square->height / 2.0f);
                r.move(square->position.x, square->position.y);
                window.draw(r);
            }
            if (auto *circle = dynamic_cast<const Circle *>(&*shape)) {
                auto c = sf::CircleShape{circle->r};
                c.setOrigin(circle->r, circle->r);
                c.move(circle->position.x, circle->position.y);
                window.draw(c);
            }
        }
        window.display();
#endif
    }

    std::cout << "The end." << std::endl;
}

