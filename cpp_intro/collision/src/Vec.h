#pragma once
#include <cmath>
#include <ostream>

/// A simple 2D Vector
struct Vec {
    float x, y;

    /// The squared magnitude of the vector
    inline float magnitude_sq() const {
        return x * x + y * y;
    }

    /// The magnitude of the vector
    inline float magnitude() const {
        return std::sqrt(magnitude_sq());
    }

    /// Get a unit vector pointing in the same direction
    inline Vec normalize() const {
        return Vec{x / magnitude(), y / magnitude()};
    }

    inline Vec operator*(float v) const {
        return Vec{x * v, y *v};
    }

    inline Vec operator+(const Vec& other) const {
        return Vec{x + other.x, y + other.y};
    }

    inline Vec operator-(const Vec& other) const {
        return Vec{x - other.x, y - other.y};
    }
};

inline std::ostream& operator<<(std::ostream& os, const Vec& v)  {
    os << "(" << v.x << "," << v.y << ")";
    return os;
}
