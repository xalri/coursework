#include "Square.h"
#include "Circle.h"

Square::Square(float width, float height, Vec pos, Vec vel):
        Shape(pos, vel), width(width), height(height) {}

/// The box's outer bounding radius
float Square::radius() const {
    return (float) sqrt((height * height / 4.0f) + (width * width / 4.0f));
}

/// The box's inner bounding radius
float Square::inner_radius() const {
    return ((width < height) ? width : height) / 2.0f;
}

/// Test for an intersection with another shape
bool Square::intersects(const Shape &other) const {
    if (auto* square = dynamic_cast<const Square*>(&other)) {
        // Simple bounding box collision
        return this->bounds().intersects(square->bounds());
    }
    if (auto* circle = dynamic_cast<const Circle*>(&other)) {
        // First we test against the outer bounding circle, if it's outside
        // this we know there's no collision.
        if (!Circle(radius(), position).intersects(other)) return false;

        // Then test against the inner bounding circle, if it's inside then
        // there's definitely a collision.
        if (Circle(inner_radius(), position).intersects(other)) return true;

        // In the case where the circle is between our inner and outer bounding
        // circles, we find the point on the square closest to the circle and test
        // the circles distance to that.
        auto bounds = this->bounds();
        auto diff = circle->position - position;
        auto close = Vec{
                bounds.x.contains(circle->position.x) ?
                    circle->position.x :
                    (diff.x < 0) ? bounds.x.min : bounds.x.max,
                bounds.y.contains(circle->position.y) ?
                    circle-> position.y :
                    (diff.y < 0) ? bounds.y.min : bounds.y.max
        };
        return (circle->position - close).magnitude_sq() < circle->r * circle->r;
    }
    // Fallback to the default for any yet-undefined shapes.
    return Shape::intersects(other);
}

Bounds Square::bounds() const {
    return Bounds {
            {position.x - width / 2.0f,  position.x + width / 2.0f},
            {position.y - height / 2.0f, position.y + height / 2.0f}
    };
};
