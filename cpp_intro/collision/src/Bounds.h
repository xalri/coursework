#pragma once

/// An axis-aligned bounding box
struct Bounds {
    /// A range of values
    struct Range {
        float min, max;

        /// Check if this range intersects with another.
        inline bool intersects(const Range &other) const {
            return !(min > other.max || max < other.min);
        }

        /// Check if a point is inside the range.
        inline bool contains(float value) const {
            return value > min && value < max;
        }
    } x, y;

    /// Test if this AABB intersects with another
    inline bool intersects(const Bounds &other) const {
        return x.intersects(other.x) && y.intersects(other.y);
    }

    /// Test if a point is inside this AABB
    inline bool contains(const Vec value) const {
        return x.contains(value.x) && y.contains(value.y);
    }
};

inline std::ostream& operator<<(std::ostream& os, const Bounds::Range& r)  {
    os << "[" << r.min << " -> " << r.max << "]";
    return os;
}

inline std::ostream& operator<<(std::ostream& os, const Bounds& b)  {
    os << "Bounds{ x: " << b.x << ", y: " << b.y << " }";
    return os;
}
