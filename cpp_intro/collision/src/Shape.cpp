#include "Shape.h"
#include "Square.h"
#include "Circle.h"

Shape::Shape(Vec pos, Vec vel): position(pos), velocity(vel) {}

/// Update the shape
void Shape::update(Bounds bounds) {
    position = position + velocity;
    // Flip if we move out of bounds
    if (!bounds.x.contains(position.x)) { velocity.x *= -1.0; }
    if (!bounds.y.contains(position.y)) { velocity.y *= -1.0; }
};

/// Test for an intersection with another shape
bool Shape::intersects(const Shape &other) const {
    // Default implementation uses bounding circles, derived classes overwrite
    // this with more precise behaviour.
    auto separation = (radius() + other.radius()) * (radius() + other.radius());
    return (this->position - other.position).magnitude_sq() < separation;
}

std::ostream& operator<<(std::ostream& os, const Shape& v) {
    if (auto *square = dynamic_cast<const Square *>(&v)) os << *square;
    else if (auto *circle = dynamic_cast<const Circle *>(&v)) os << *circle;
    else os << "Unknown shape";
    return os;
}
