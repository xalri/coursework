#include "Vector3D.h"
#include "Bin.h"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

TEST_CASE("Constructing bins gives the correct capacity", "[bin]") {
    REQUIRE( Bin().capacity() == 0 );
    REQUIRE( Bin(0).capacity() == 0 );
    REQUIRE( Bin(20).capacity() == 20 );
    REQUIRE( Bin(128).capacity() == 128 );
}

TEST_CASE("Bin copy constructor works", "[bin]") {
    auto source = Bin(20);
    source.add(1, 2, 3);
    auto dest = Bin(source);
    REQUIRE( dest.capacity() == 20 );
    REQUIRE( dest.size() == 1 );

    SECTION("Copied bins don't share data") {
        source.remove(0);
        source.add(2, 3, 4);
        REQUIRE(source.getX(0) == 2);
        source.~Bin();
        REQUIRE(dest.getX(0) == 1);
    };
}

TEST_CASE("Bin assignment operator works", "[bin]") {
    auto source = Bin(20);
    source.add(1, 2, 3);
    Bin dest;
    dest = source;
    REQUIRE( dest.capacity() == 20 );
    REQUIRE( dest.size() == 1 );

    SECTION("Assigned bins don't share data") {
        source.remove(0);
        source.add(2, 3, 4);
        REQUIRE(source.getX(0) == 2);
        REQUIRE(dest.getX(0) == 1);
    };
}

TEST_CASE("Vectors can be removed from bins", "[bin]") {
    auto bin = Bin(20);
    bin.add(1, 2, 3);
    bin.add(2, 3, 4);
    bin.add(5, 6, 7);
    REQUIRE(bin.size() == 3);

    SECTION("Shift remove") {
        bin.remove(1);
        REQUIRE(bin.size() == 2);
        REQUIRE(bin.getX(0) == 1);
        REQUIRE(bin.getX(1) == 5);
    }

    SECTION("Swap remove") {
        bin.swap_remove(0);
        REQUIRE( bin.size() == 2 );
        REQUIRE( bin.getX(0) == 5 );
        REQUIRE( bin.getX(1) == 2 );
    }
}

TEST_CASE("Bins automatically resize in power-of-two increments", "[bin]") {
    auto bin = Bin();
    REQUIRE(bin.capacity() == 0 );
    bin.add(0, 0, 0);
    REQUIRE(bin.capacity() == 1 );
    bin.add(0, 0, 0);
    REQUIRE(bin.capacity() == 2 );
    bin.add(0, 0, 0);
    REQUIRE(bin.capacity() == 4 );
    bin.add(0, 0, 0);
    REQUIRE(bin.capacity() == 4 );
    bin.add(0, 0, 0);
    REQUIRE(bin.capacity() == 8 );
}

TEST_CASE("Getting data from bins works", "[bin]") {
    auto bin = Bin();
    bin.add(1, 2, 3);
    bin.add(4, 5, 6);
    REQUIRE( bin.getX(0) == 1 );
    REQUIRE( bin.getY(0) == 2 );
    REQUIRE( bin.getZ(0) == 3 );
    REQUIRE( bin.getX(1) == 4 );
    REQUIRE( bin.getY(1) == 5 );
    REQUIRE( bin.getZ(1) == 6 );
}

TEST_CASE("Vector construction is correct", "[vector]") {
    auto v = Vector3D(1.0f, 2.0f, 3.0f);
    auto v2 = Vector3D(v);

    REQUIRE( v2.getX() == 1.0f );
    REQUIRE( v2.getY() == 2.0f );
    REQUIRE( v2.getZ() == 3.0f );
}

TEST_CASE("Vector << operator format", "[vector]") {
    auto ss = std::stringstream();
    ss << Vector3D(2.0f, 3.0f, 4.0f);
    REQUIRE( ss.str() == "(2,3,4)");
}

TEST_CASE("Basic vector operations are correct", "[vector]") {
    auto v1 = Vector3D(2.0f, 3.0f, 4.0f);
    auto v2 = Vector3D(-2.0f, 3.0f, 4.0f);

    SECTION("magnitudeSq") {
        REQUIRE(v1.magnitudeSq() == Approx(29.0f));
        REQUIRE(v2.magnitudeSq() == Approx(29.0f));
    }

    SECTION("magnitude") {
        REQUIRE(v1.magnitude() == Approx(std::sqrt(29.0f)));
        REQUIRE(v2.magnitude() == Approx(std::sqrt(29.0f)));
    }

    SECTION("dot/scalar product") {
        REQUIRE(v1.dot(v2) == Approx(21.0f));
        REQUIRE(v2.dot(v1) == Approx(21.0f));
    }

    SECTION("cross/vector product") {
        REQUIRE(v1.cross(v2) == Vector3D(0.0f, -16.0f, 12.0f));
        REQUIRE(v2.cross(v1) == Vector3D(0.0f, 16.0f, -12.0f));
    }

    SECTION("normal") {
        REQUIRE(Vector3D(2, 0, 0).normal(Vector3D(0, 2, 0)) == Vector3D(0, 0, 1));
        REQUIRE(Vector3D(0, 2, 0).normal(Vector3D(2, 0, 0)) == Vector3D(0, 0, -1));
    }

    SECTION("normalise") {
        auto m = v1.magnitude();
        REQUIRE(v1.normalize() == Vector3D(2.0 / m, 3.0 / m, 4.0 / m));
        REQUIRE(Vector3D(1.0, 0.0, 0.0).normalize() == Vector3D(1.0, 0.0, 0.0));
        REQUIRE(Vector3D(-1.0, 0.0, 0.0).normalize() == Vector3D(-1.0, 0.0, 0.0));
    }

    SECTION("multiplySelf") {
        v1.multiplySelf(2.0f);
        REQUIRE(v1 == Vector3D(4.0, 6.0, 8.0));
        v1.multiplySelf(-1.0f);
        REQUIRE(v1 == Vector3D(-4.0, -6.0, -8.0));
    }

    SECTION("divideSelf") {
        v1.divideSelf(2.0f);
        REQUIRE(v1 == Vector3D(1.0, 1.5, 2.0));
        v1.divideSelf(-1.0f);
        REQUIRE(v1 == Vector3D(-1.0, -1.5, -2.0));
    }

    SECTION("operators") {
        REQUIRE(v1 + v2 == Vector3D(0.0, 6.0, 8.0));
        REQUIRE(v1 - v2 == Vector3D(4.0, 0.0, 0.0));
        REQUIRE(v1 * v2 == Approx(21.0));
        REQUIRE(v1 % v2 == Vector3D(0.0, -16.0, 12.0));
        REQUIRE(v1 == v1);
        REQUIRE(v1 != Vector3D(2.0, 3.0, 0.0));
        REQUIRE(v1 != Vector3D(2.0, 0.0, 4.0));
        REQUIRE(v1 != Vector3D(0.0, 3.0, 4.0));
    }
};
