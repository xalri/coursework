#pragma once

#include "Vector3D.h"

/** A contiguous growable container of `Vector3D`s.
 * Vectors in the `Bin` are accessed by their index, where the first has
 * index 0.
 * When constructed with a capacity of 0 no memory is allocated. Adding
 * an element will only result in an allocation if the existing capacity
 * is insufficient, at which point the capacity is doubled. */
class Bin {
public:
	/** Create an empty bin */
	Bin();

	/** Create a bin with the given capacity */
	explicit Bin(unsigned capacity);

	/** Create a copy of the given bin */
	Bin(const Bin& other);

	/** Destructor, frees the bin's memory */
	~Bin();

	Bin& operator=(const Bin& other);

	/** The number of vectors in the bin */
	inline unsigned size() const { return len; }

	/** The capacity of the bin -- the number of elements which the bin
	 *  can contain without resizing */
	inline unsigned capacity() const { return cap; }

	/** Get the x component of the vector at index a.
	  * Throws an out_of_range exception if the given index is invalid*/
	float getX(unsigned a) const;

	/** Get the y component of the vector at index a.
	  * Throws an out_of_range exception if the given index is invalid*/
	float getY(unsigned a) const;

	/** Get the y component of the vector at index a.
	  * Throws an out_of_range exception if the given index is invalid*/
	float getZ(unsigned a) const;

	/** Add a vector to the bin */
	void add(float x, float y, float z);

	/** Remove a vector given it's index, shifts following vectors back.
	  * Throws an out_of_range exception if the given index is invalid*/
	void remove(unsigned a);

	/** Remove a vector given it's index by swapping it with the final vector
	 * to avoid shifting any other data. Faster than `remove` but doesn't
	 * preserve order.
	 * Throws an out_of_range exception if the given index is invalid*/
	void swap_remove(unsigned a);

private:
	unsigned len;
	unsigned cap;
	Vector3D* data;
};