#include "Bin.h"
#include <new>
#include <stdexcept>

Bin::Bin() : cap(0), len(0), data(nullptr) {}

Bin::Bin(const unsigned capacity): cap(capacity), len(0), data(nullptr) {
	if (capacity > 0) {
		data = static_cast<Vector3D*>(::operator new(sizeof(Vector3D) * capacity));
	}
}

Bin::Bin(const Bin & other): cap(other.cap), len(other.len), data(nullptr) {
    if (cap > 0) {
		data = static_cast<Vector3D*>(::operator new(sizeof(Vector3D) * other.cap));
		for (auto i = 0; i < len; i++) new(data + i) Vector3D(other.data[i]);
	}
}

Bin::~Bin() {
	::operator delete(data);
	data = nullptr;
}

Bin& Bin::operator=(const Bin& other) {
	if (this != &other) {
		::operator delete(data);
		data = nullptr;
		cap = other.cap;
		len = other.len;
		if(cap > 0) {
			data = static_cast<Vector3D *>(::operator new(sizeof(Vector3D) * cap));
			for (auto i = 0; i < len; i++) new(data + i) Vector3D(other.data[i]);
		}
	}
	return *this;
}

float Bin::getX(const unsigned a) const {
	if (a < len) return data[a].getX();
	else throw std::out_of_range("Bin range check");
}

float Bin::getY(const unsigned a) const {
	if (a < len) return data[a].getY();
	else throw std::out_of_range("Bin range check");
}

float Bin::getZ(const unsigned a) const {
	if (a < len) return data[a].getZ();
	else throw std::out_of_range("Bin range check");
}

void Bin::add(const float x, const float y, const float z) {
	if (cap > len) {
		len++;
		data[len - 1] = Vector3D(x, y, z);
	} else {
		// Not enough capacity; reallocate & copy old data then try again.
		auto oldData = data;
		auto oldCapacity = cap;
		if (cap == 0) cap = 1; else cap *= 2;
		data = static_cast<Vector3D*>(::operator new(sizeof(Vector3D) * cap));

		for (auto i = 0; i < oldCapacity; i++) new (data + i) Vector3D(oldData[i]);
		::operator delete(oldData);
		add(x, y, z);
	}
}

void Bin::remove(const unsigned a) {
	if (a < len) {
		len--;
		for (auto i = a; i < len; i++) data[i] = data[i + 1];
	}
	else throw std::out_of_range("Bin range check");
}

void Bin::swap_remove(const unsigned a) {
	if (a < len) {
		len--;
		data[a] = data[len];
	}
	else throw std::out_of_range("Bin range check");
}
