#pragma once

#include <cmath>
#include <iostream>

/** A 3 Dimentional vector */
class Vector3D {
public:
    /** Create a new vector with the given x, y, and z components */
	Vector3D(const float x, const float y, const float z): x(x), y(y), z(z) { }

	// Explicit default copy constructor & destructor
	Vector3D(const Vector3D& other) = default;
	~Vector3D() = default;

	/** The vector's X component */
	inline float getX() const { return x; }

	/** The vector's Y component */
	inline float getY() const { return y; }

	/** The vector's Z component */
	inline float getZ() const { return z; }

    /** The square of the vector's magnitude */
	inline float magnitudeSq() const {
		return (x*x) + (y*y) + (z*z);
	}

	/** The vector's magnitude */
	inline float magnitude() const {
		return std::sqrt(magnitudeSq());
	}

    /** The dot (scalar) product of this vector with another */
	inline float dot(const Vector3D& o) const {
		return (x*o.x) + (y*o.y) + (z*o.z);
	}

	/** The cross (vector) product of this vector with another */
	inline Vector3D cross(const Vector3D& o) const {
		return Vector3D((y*o.z) - (z*o.y), (z*o.x) - (x*o.z), (x*o.y) - (y*o.x));
	}

	/** Get the unit vector which points in the same direction as this vector */
	inline Vector3D normalize() const {
		auto s = magnitude();
		return Vector3D(x / s, y / s, z / s);
	}

	/** Get the unit vector perpendicular to the plane formed by this vector
	 * and another */
	inline Vector3D normal(const Vector3D & other) const {
		return (*this % other).normalize();
	}

	/** Multiply each element by the given value */
	inline void multiplySelf(float a) {
		x *= a; y *= a; z *= a;
	}

	/** Divide each element by the given value */
	inline void divideSelf(float a) {
		x /= a; y /= a; z /= a;
	}

	/** Element-wise addition of vectors */
	inline Vector3D operator+(const Vector3D & other) const {
		return Vector3D(x + other.x, y + other.y, z + other.z);
	}

	/** Element-wise subtration of vectors */
	inline Vector3D operator-(const Vector3D & other) const {
		return Vector3D(x - other.x, y - other.y, z - other.z);
	}

	/** Multiply the vector by a float */
	inline Vector3D operator*(float f) const {
		auto v = *this;
		v.multiplySelf(f);
		return v;
	}

	/** Divide the vector by a float */
	inline Vector3D operator/(float f) const {
		auto v = *this;
		v.divideSelf(f);
		return v;
	}

	/** Calculate the dot (scalar) product of two vectors */
	inline float operator*(const Vector3D& other) const {
		return dot(other);
	}

	/** Calculate the cross (vector) product of two vectors */
	inline Vector3D operator%(const Vector3D& other) const {
		return cross(other);
	}

	/** Test if two vectors have equal components */
    inline bool operator==(const Vector3D& other) const {
        return x == other.x && y == other.y && z == other.z;
    }

	/** Test if two vectors have differing components */
	inline bool operator!=(const Vector3D& other) const {
		return !(*this == other);
	}

private:
	float x, y, z;
};

/** Write a string representation of the vector to an output stream */
inline std::ostream& operator<<(std::ostream& os, const Vector3D& v)  {
    os << "(" << v.getX() << "," << v.getY() << "," << v.getZ() << ")";
    return os;
}