#!/bin/sh

qemu-system-i386 \
   -drive file=./minix321_main.cow,format=qcow2 \
   -m 1G \
   -net user,hostfwd=tcp::2222-:22 \
   -net nic \
   -enable-kvm
