#!/bin/sh

qemu-system-x86_64 \
   -drive file=./minix321_main.cow,format=qcow2 \
   -m 2G \
   -net user,hostfwd=tcp::2222-:22 \
   -net nic \
   -enable-kvm
