/*
 * Replace the following string of 0s with your student number
 * 150184050
 */
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/types.h>
#include <pwd.h>
#include <time.h>
#include <ctype.h>
#include <wordexp.h>
#include <dirent.h>

#include "filecmdrlib.h"

#define MODE_S_LEN  11
#define TIME_S_LEN  17

/*
 * is_user_exec helper function to test user execute permission for given
 * file mode and owner uid and gid.
 * Uses getuid() to get the uid of the calling process and getgid() to get the
 * gid of the calling process.
 * This function is not part of the filecmdrlib interface.
 */
bool is_user_exec(mode_t mode, uid_t ouid, gid_t ogid) {
    if (ouid == getuid())
        return mode & S_IXUSR;

    if (ogid == getgid())
        return mode & S_IXGRP;

    return mode & S_IXOTH;
}

/*
 * The following functions are defined in filecmdrlib.h and need to be
 * modified to comply with their definitions and the coursework specification.
 */

int execfile(char *path) {
    printf("Enter any arguments to %s: ", path);

    char *arg_string = NULL;
    size_t len = 0;
    ssize_t read = 0;
    wordexp_t p;

    // Read the arguments from stdin
    if ((read = getline(&arg_string, &len, stdin)) == -1) return -1;

    // wordexp gives an error when parsing '\n', so replace it with '\0'
    arg_string[read - 1] = '\0';

    // Parse the read line into argv, expanding environmental variables, path
    // wildcards, etc, in the same way a shell would using wordexp.
    if (wordexp(path, &p, 0) != 0) return -1;
    if (wordexp(arg_string, &p, WRDE_APPEND) != 0) return -1;
    if (arg_string) free(arg_string);

    // Execute the program with the given arguments
    return execv(p.we_wordv[0], p.we_wordv);
}

int listdir(char *path) {
    // Open the directory
    struct dirent *f;
    DIR *d = opendir(path);
    if (!d) return -1;

    // Remove any extra '/' from the end of the directory path
    if(path[strlen(path) - 1] == '/') path[strlen(path) - 1] = '\0';

    while ((f = readdir(d)) != NULL) {
        // Combine the file name with the path of the directory
        // using a variable length stack allocated string
        char full_path[strlen(path) + strlen(f->d_name) + 1];
        strcpy(full_path, path);
        strcat(full_path, "/");
        strcat(full_path, f->d_name);

        // Display information for the file.
        printfinf(full_path);
    }

    closedir(d);
    if (errno != 0) return -1;
    return 0;
}

int listfile(char *path) {
    // Open the file for reading
    FILE *f = fopen(path, "r");
    if (!f) return -1;

    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    // Read the file line by line, printing each line.
    while ((read = getline(&line, &len, f)) != -1) {
        // getline includes the newline character, so no need to add it.
        printf("%s", line);
    }

    fclose(f);
    if (line) free(line);
    return 0;
}

char *mode2str(mode_t mode, uid_t ouid, gid_t ogid) {
    // Check that the mode is valid
    if (mode > MODE_MAX || mode < MODE_MIN) {
        errno = EDOM;
        return NULL;
    }

    // Allocate memory for the mode string, with the default value. We will
    // replace any missing permissions with '-'
    char *mode_s = (char *) calloc(MODE_S_LEN, sizeof(char));
    if (!mode_s) return NULL;
    strcpy(mode_s, "orwxrwxrwx");

    // Set the file type character.
    if (S_ISREG(mode))
        // For a regular file, check if it's executable
        mode_s[0] = (is_user_exec(mode, ouid, ogid))? 'e' : 'f';
    else if (S_ISDIR(mode))
        mode_s[0] = 'd';
    else if (S_ISLNK(mode))
        mode_s[0] = 'l';

    // Set the char for any missing permissions to '-'
    for (int i = 1; i < 10; i++)
        if ((mode & ( 01000 >> i )) == 0)
            mode_s[i] = '-';

    return mode_s;
}

int printfinf(char *path) {
    if(!path || path[0] == '\0') return -1;

    struct stat s;
    if (lstat(path, &s) < 0) return FTYPE_ERR;

    char *mode_s = mode2str(s.st_mode, s.st_uid, s.st_gid);
    if (!mode_s) return FTYPE_ERR;

    char *time = time2str(s.st_mtime);
    if (!time) return FTYPE_ERR;

    struct passwd *pw = getpwuid(s.st_uid);
    if (!time) return FTYPE_ERR;

    printf("%s %s %12ld %s %s\n", mode_s, pw->pw_name, (long) s.st_size, time, path);

    int ret = FTYPE_OTH;
    // Switch on the first character of the mode string, which gives the type.
    // (avoids repeating the getuid and getgid calls of re-checking mode_s)
    switch (mode_s[0]) {
        case 'd': ret = FTYPE_DIR; break;
        case 'e': ret = FTYPE_EXE; break;
        case 'f': ret = FTYPE_REG; break;
        case 'l': ret = FTYPE_LNK; break;
    }

    if (time) free(time);
    if (mode_s) free(mode_s);
    return ret;
}

char *time2str(time_t time) {
    char *time_s = (char *) calloc(TIME_S_LEN, sizeof(char));
    if (!time_s) return NULL;

    struct tm *t = localtime(&time);
    if (!strftime(time_s, TIME_S_LEN, "%d/%m/%Y %R", t)) {
        if(time_s) free(time_s);
        return NULL;
    };

    return time_s;
}

int user_prompt() {
    int result = tolower(getchar()) == 'y';
    fseek(stdin, 0, SEEK_END);
    return result;
}

int useraction(int ftype, char *path) {
    if(!path || path[0] == '\0') return -1;
    if(ftype != FTYPE_DIR && ftype != FTYPE_EXE && ftype != FTYPE_REG)
        return 0;

    static const char *action_prompt[] = {
        "Do you want to list the directory %s (y/n): ",
        "Do you want to execute %s (y/n): ",
        "Do you want to list the file %s (y/n): "
    };
    printf(action_prompt[ftype], path);

    int (*f[])(char *) = { listdir, execfile, listfile };
    if (user_prompt()) return (*f[ftype])(path);

    return 0;
}

