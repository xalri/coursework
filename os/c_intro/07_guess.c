// A simple guessing game.
// The users picks any integer -- the program then tries to guess this number,
// asking the user whether it's guess is correct, too large, or too small.
//
// FIrst, a range is established by guessing 0, +-10, +-100, and so on, untill
// the user has answered both > and < once.
// When we have this range, we can bisect is, decreasing the search space by
// (N/2)+1 each iteration -- so after establishing a range we can find the
// value in worst case O(log N)

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// Read the user's response (=, > or <) from stdin. If it's equal, print out
// the ending message and exit the program. Otherwise return the character we
// read (< or >).
char check_response(long guess);

int main(void) {

   // Ask the user to pick a number for us to guess
   char buffer[128];
   printf("Pick a whole number -- press enter to continue.");
   scanf("*");

   printf("I'm going to guess your number. For each number I guess, respond with \n"
           "'=' if I'm right, '<' if your number is lower than mine, and '>' if your \n"
           "number is greater than my guess.\n\n");

   // Find the sign of the number
   long guess = 0;
   printf("My first guess is 0. ");
   char resp = check_response(0);
   if (resp == '<') guess = -10;
   else guess = 10;

   // establish an upper/lower bound
   long bound = 0;
   while (bound == 0) {
      printf("I guess that your number is %ld. ", guess);
      char resp = check_response(guess);
      // If we guess a positive value and it's less than their number,
      // or we guess a negative value and it's greater then their number
      // we have a known range of 0 to whatever we guessed.
      if ((resp == '<' && guess > 0) || (resp == '>' && guess < 0))
         bound = guess;
      else
         guess *= 10;
   }

   // Now we have a known range, we can use a binary search over the integers:
   // first establish the range: a < n < b
   long a = bound > 0 ? (guess / 10) + 1 : bound + 1;
   long b = bound < 0 ? (guess / 10) - 1 : bound - 1;
   while (1) {
      if (a == b)
         printf("Ok, I've got it for sure now.\n");
      guess = (a + b) / 2;
      printf("I guess %ld. ", guess);
      char resp = check_response(guess);
      if (a == b) {
         // our bounds are equal but we haven't found the value -- one of the
         // user's inputs was incorrect.
         printf("Hmm, something went wrong, I give up.\n");
         exit(1);
      }
      if (resp == '<') b = guess - 1;
      else a = guess + 1;
   }
}

// Read the user's response (=, > or <) from stdin. If it's equal, print out
// the ending message and exit the program. Otherwise return the character we
// read (< or >).
char check_response(long guess) {
   char buffer[8];
   while (1) {
      scanf("%127s", buffer);
      if (buffer[0] == '=') {
         printf("I got it! You're number was %ld!\n"
                 "Thanks for playing o/\n", guess);
         exit(0);
      }
      if (buffer[0] == '<') {
         printf("So your number is less than %ld.\n", guess);
         return '<';
      }
      if (buffer[0] == '>') {
         printf("So your number is more than %ld.\n", guess);
         return '>';
      }
      printf("Please enter =, <, or >: ");
   }
}
