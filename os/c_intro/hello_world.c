// Headers are included using #include, <> for external libraries, "" for local
// headers
#include <stdio.h>

// Functions must be pre-declared, as C is parsed in a single pass
void array_init();
void multidim_array();

// The main function takes an array of strings (argv) -- the arguments to the
// program, and the number of arguments (argc)
int main (int argc, char** argv) {
   array_init();
   multidim_array();
   return 0;
}

void array_init() {
   // Arrays are an indexed sequence of value.
   // (dynamic) array types are pointers to the start of the block of memory -- indexing
   // is pointer arithatic. a[x] == *(a+x);
   // .. and pointer arithmatic depends on the size of the pointer type.
   // stack allocated arrays are odd. Is the size part of the type? (sizeof etc)
   // so, if a is an array of ints: a+1 is sizeof(int) further along in memory,
   // than the start of the array, or, the next value in the array.

   // Arrays can be initialised using initialiser lists,
   // which can be any length up to the size of the array.
   // Any undeclared values are 0
   // Stack allocated -- only exists within their enclosing block
   int an_array[10] = {0, 10, 20, 30, 40};

   // Array values are accessed by index
   printf("an_array[3] = %d\n", an_array[3]);

   // This works, just don't do it ._.
   // (clang is smart and gives you a warning, gcc shows nothing)
   an_array[50] = 10;

   // Variable sized arrays:
   // Variable arrays can't be initialised with initialiser lists.
   // Some say you should only declare variables after an opening brace
   // (as in the ANSI C (C90) standard). C99 onwards allows declaring variables
   // anywhere a statement can be, which I prefer.
   size_t x = 42;
   int var_array[x];

   for (size_t i = 0; i < x; i++) {
      var_array[i] = 2 * i;
   }

   printf("var_array[14] = %d\n", var_array[14]);
}

void multidim_array() {
   // Arrays can be nested to form multidimentional arrays:

   int nested[10][10] = {
      {1, 2, 3, 4 /* ... */},
      {1, 2, 3, 4 /* ... */},
      /* .... */
   };

   printf("nested[1][2] = %d\n", nested[1][2]);
}
