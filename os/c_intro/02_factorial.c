#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
// ^ pre-processor macros start wiith '#'
// others are #define, #if, #endif, etc.

// Functions must be predeclared as C is parsed in a single pass.
// (called function prototypes)
uint64_t rfactorial (int n);
uint64_t factorial (int n);

int main (int argc, char** argv) {
   if (argc != 2) {
      printf("This program requires one argument.\n");
      printf("Usage: factorial <value>\n");
      return 1;
   }

   int n = atoi(argv[1]);
   uint64_t result = rfactorial(n);
   if (result == -1) {
      printf("The factorial of a negative is undefined\n");
      return 1;
   }
   printf("%lld\n", result);

   return 0;
}

/** Recursively calculate the factorial of the given value.
 *  If the value is negative returns -1 */
uint64_t rfactorial (int n) {
   // Error case -- the factorial of a negative is undefined.
   if (n < 0) return -1;
   // Base case -- the factorial of 0 is defined to be 1
   if (n == 0) return 1;
   // Recursive case, factorial x = x * factorial (x - 1) where x > 0
   uint64_t result = rfactorial(n - 1) * (uint64_t) n;
   return result;
}


/** Iteratively calculate the factorial of the given value.
 *  If the value is negative returns -1 */
uint64_t factorial (int n) {
   // Error case -- the factorial of a negative is undefined.
   if (n < 0) return -1;
   // Base case -- the factorial of 0 is defined to be 1
   if (n == 0) return 1;

   uint64_t result = n;
   for (int i = n - 1; i > 0; i--) result *= i;

   return result;
}
