#include <stdio.h>

static char CASE_MASK = 0b00100000;

char flip_char(char c) {
   return c ^ CASE_MASK;
}
char to_lower(char c) {
   return c | CASE_MASK;
}
char to_upper(char c) {
   return c & ~CASE_MASK;
}

int main (int argc, char** argv) {
   char flipped_a = flip_char('a');
   char flipped_z = flip_char('Z');
   printf("flipped '%c',  '%c'\n", flipped_a, flipped_z);
   printf("to_upper '%c',  '%c'\n", to_upper('a'), to_upper('A'));
   printf("to_lower '%c',  '%c'\n", to_lower('a'), to_lower('A'));
}
