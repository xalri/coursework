#include <stdio.h>

// Sum the elements in the given array a, size n
int sum_array (int a[], size_t n);

// Sum all the even-indexed elements of the given array
//  a[0] + a[2] + a[4] + ...
int sum_even (int a[], size_t n);

// Find the maximum value in an array
int array_max (int a[], size_t n);

int main (int argc, char** argv) {
   int a[5] = {0, 1, 2, 4, 2};
   printf(" sum_array(a, 5) = %d\n",sum_array(a, 5));
   printf(" sum_even(a, 5) = %d\n",sum_even(a, 5));
   printf(" array_max(a, 5) = %d\n",array_max(a, 5));
   return 0;
}

int sum_array (int a[], size_t n){
   int sum = 0;
   for (int i = 0; i < n; i++) {
      sum += a[i];
   }
   return sum;
}

int sum_even (int a[], size_t n) {
   int sum = 0;
   for (int i = 0; i < n; i += 2) {
      sum += a[i];
   }
   return sum;
}

int array_max (int a[], size_t n) {
   int max = a[0];
   for (int i = 0; i < n; i++) {
      max = (a[i] > max)? a[i] : max;
   }
   return max;
}
