#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

// Pointers, Memory, and assorted things ._.

int main(void) {
   // T*  <- A type: A pointer to a value type T
   // &x  <- An R value: The address of x
   // *x  -> An L value: dereference x (get the value at the address at x)

   int i = 300;
   // Don't do that thing  v
   void* p = (void*) &i; // C compilers can allow implicit casts, but don't do that.

   // Pointers to pointers to pointers..
   int* pp = &i;
   int** ppp = &pp; // This is sometimes useful.
   int*** pppp = &ppp; // This probably isn't ._.

   // ^ all this is static memory allocation - everything must be known at
   // compile time (with the exception of VLAs, but let's pretend they don't
   // exist.)

   // The alternative is dynamic allocation (`man calloc`)
   // x = malloc(...) /* .... */ free(x)
   // allocated memory should always eventually be freed.
   // malloc doesn't take a type -- takes a size.
   // sizes are machine dependent: use sizeof(T) or sizeof x
   // sizeof(T): size_t
   // Can be null, check before using.

   int* p2 = malloc(sizeof(int) * 20);
   // ^ That's an implicit type conversion from void*. yay C.
   // also calloc, realloc, and the stack allocation thing.

   // Now we have memory we can do things with :3
   // not guaranteed to be contiguous? would with calloc.
   *(p2 + 19) = 50;
   assert(p2[19] == 50);

   free(p2); // double frees are undefined, like many things..

   // Don't invoke Undefined Behaviour, very bad things will happen..
   // ==============

   // Primitive types:
   // int, float, double, char
   // integer qualifiers:
   // signed, unsigned, long, short

   // The type refered to by a pointer defines the size of the memory it's
   // pointing to -- affects pointer arithmatic as well as deref.
   // Sizes are also important for bitwise operators.

   // Yay, C.
   char c = 'a';
   int* cp = (int*) &c; // whoops.
   *cp;

   // Prefer passing a pointer than passing values directly.

   // Pointers allow shared mutable memory, which is cool, just be careful of
   // ownership (whose responsibility is it to free? do we know no one's still
   // using it?), aliasing, etc. Oh hey, threads are a thing, did you think
   // about threads when making that struct?
   // See, you could think about all that... Or you could use rust, where the
   // compiler is smart, and let's you do all the manual memory management while
   // _telling_ you when you make these mistakes.

   // or, in fewer words: pointers are sharp :3


   // and now, let's segfault :3
   // (cast '42' to a pointer, and try to deref. We probably don't own this
   // part of memory, so it segfaults)
   // also, -Wall -Wextra doesn't catch this. so that's a thing.
   int x = *(int*)(42);
   // [1]    2035 segmentation fault (core dumped)  ./pointers
   // oops.
   // Isn't C fun.
}

char* oops() {
   char c = 'a';
   // Let's try returning a reference to a local variable.
   return &c;
   // Oh hey, the compiler cuaght it, without even -Wall! As a warning..
   // I g that's why we have -pedantic or whatever it's called?
}

