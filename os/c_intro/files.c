#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>

void fork_test();

int main (int argc, char** argv) {

   /** System calls and stuff ._. */

   // we're inside a user program
   FILE *f = fopen("hai","rw"); // < blocking / synchronous I/O
   // when we call fopen, there's some code in a system library
   // which in turn makes OS calls, using kernel privilages (can't do it in
   // user mode -- only kernel can access FS, also memory of each process)
   // which returns to us a file handle (via the stack, like all other fn calls) :3
   // ^ Switching process

   // Some other poisx syscalls:
   // fork, execve, waitpid, exit, ..
   fork_test();

   /** Back to files ._. (everything is a file, etc) */

   // fopen can fail -- we need to check that `f` actually exists.
   if (f == NULL) {
      printf("File 'hai' doesn't exist (probably) ._.\n");
      // return 1 to indicate an error.
      return 1;
   }
   // here we know f exists, which is cool.

   // TODO: reading / writing / seeking (/get position) .
   // hahaha

   struct stat statbuf;
   int status = stat("hai", &statbuf);
   if (!status) {
      printf("'hai' is %ld bytes\n", (long) statbuf.st_size);
   } else {
      perror("stat failed D:");
   }

   // Files have to be explicity closed, no RAII here :(.
   fclose(f);

   return 0;
}

/** Forking processes */
void fork_test() {
   // shells do this. hmm.
   pid_t  pid = fork();
   if (pid == 0) {
      printf("I'm the child :3\n");
      // exit so we don't try to access the file concurrently o.o
      exit(0);
   }

   printf("I'm the parent :3\n");
   // wait for the child to exit
   waitpid(pid, NULL, 0);
}
