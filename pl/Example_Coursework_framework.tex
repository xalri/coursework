\documentclass[a4paper,11pt,vdm]{article}
\usepackage[utf8]{inputenc}
\usepackage[ampersand]{easylist}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{parskip,graphicx,textcomp,amsmath,natbib,amsfonts,vdm}
% Title Page
\title{CSC3321: Understanding Programming Languages\\
	Coursework}
\author{Troy Astarte, Ellen Morris, Sam Price
	}
\date{May 2015}

		% CHANGE THESE TO BUILD NEW RELATIONS

		\def\SOSRule#1#2{\begin{vdm}\begin{formula}\raisebox{-0.5\baselineskip}{\mbox{$\begin{array}[b]{l}#1\\ \hline #2\end{array}$}}\end{formula}\end{vdm}}
		\def\Rslp{\buildrel sl'\over\longrightarrow}
		\def\Rsl{\buildrel sl\over\longrightarrow}
		\def\Rst{\buildrel st\over\longrightarrow}
		\def\Rpar{\buildrel par\over\longrightarrow}
		\def\Rs{\buildrel s\over\longrightarrow}
		\def\Re{\buildrel e\over\longrightarrow}
		\def\Rex{\buildrel ex\over\longrightarrow}
		\def\Ret{\buildrel et\over\longrightarrow}
		\def\Ri{\buildrel i\over\longrightarrow}
		\def\Rp{\buildrel p\over\longrightarrow}
		\def\Rlhv{\buildrel lhv\over\longrightarrow}
\begin{document}
\maketitle
\tableofcontents
	\section{Task A}
	An extension of the \textbf{Base} language to include a $For$ statement.\\
	\subsection{$For$ statement abstract syntax}
	First, the $Stmt$ type is extended:

	{\color{ForestGreen}
		\type{Stmt}{Assign | If | While | For}
	}

	Next, the type is defined:

	{\color{ForestGreen}
		\begin{record}{For}
			control: ForExpr\\
			body: \seqof{Stmt}
		\end{record}

		\begin{record}{ForExpr}
			startval: Assign\\
			endval: RelExpr\\
			increment: ArithExpr
		\end{record}
	}

	\subsection{Assumptions}

	Using $ForExpr$ it is assumed that the statement will be semantically logical---the control variable used in the $startval$ statement will be the same as in the $endval$ expression, and the $increment$ expression. It is assumed also that this variable will be local to the $For$ loop and not available outside its scope.

	\section{Task B}
	An extension of \textbf{Base} to include a $String$ type and two associated operations: $ConcatExpr$ and $SubstringExpr$.

	\subsection{$String$ abstract syntax}
	 First, the existing $ScalarType$, $ScalarValue$, and $Expr$ constructs are extended to include the new functionality:

	{\color{ForestGreen}
		\type{ScalarType}{\const{IntTp} | \const{BoolTp} | \const{StringTp}}
		\type{ScalarValue}{\Int | \Bool | String}
		\type{String}{\seqof{Char}}
		\type{Expr}{ArithExpr | RelExpr | ConcatExpr | SubstringExpr | Id | ScalarValue}
	}

	Now, the new operations are defined:

	{\color{ForestGreen}
		\begin{record}{ConcatExpr}
			string1: Expr\\
			string2: Expr
		\end{record}

		\begin{record}{SubstringExpr}
			superstring: String\\
			start: \Nat\\
			end: \Nat
		\end{record}
	}

	\subsection{$String$ type context conditions}
	In order to correctly apply context conditions, the $c-type$ function is extended, first for the base case of $String$:

	{\color{Red}
		\begin{fn}{c-type}{e, tpm}
			\signature{
				Expr \x TypeMap \to  ( \const{IntTp} | \const{BoolTp} | \const{StringTp} | \const{Error})
			}
			\hbox{given by cases in full \textbf{Base} spec and below}
		\end{fn}
	}

	For the base $String$ case:

	{\color{Red}
%		\begin{vdm}
		$
		e \in String \Implies c-type(e, tpm) = \const{StringTp}
		$
%		\end{vdm}
	}

	...and then for the expressions:

	{\color{Red}

		\begin{fn}{c-type}{mk-ConcatExpr(s1, s2), tpm} \\
			\If {c-type(s1, tpm) = \const{StringTp} \And c-type(s2, tpm) = \const{StringTp}}
			\Then {\const{StringTp}}
			\Else {\const{Error}}
			\Fi
		\end{fn}

		\begin{fn}{c-type}{mk-SubstringExpr(s, start, end), tpm} \\
			\If{
				\begin{formbox}
					c-type(s, tpm) = \const{StringTp} \And\\
					c-type(start, tpm) = \const{IntTp} \And\\
					c-type(end, tpm) = \const{IntTp} \And\\
					start \ge 1 \And end \leq \len{s} \And end \geq start
				\end{formbox}
			}
			\Then {\const{StringTp}}
			\Else {\const{Error}}
			\Fi
		\end{fn}

	}

	\subsection{Assumptions}
	We have made the decision to define strings as a sequence of characters; the $Char$ type is not defined but can be assumed to be the set of printable characters in some arbitrary encoding (e.g.\ UTF). We are assuming that sub-sequence indexing is used to pick out characters from strings, and that the indexing is one-based. We have also made the decision that $SubstringExpr$ cannot be utilised on strings of length zero (empty strings); if this is tried, the context conditions will return an \const{Error} type.

	\section{Task C}

	A further extension of the \textbf{Base} language to include a multiple assign operation.

	\subsection{$MultiAssign$ abstract syntax}

	Firstly, the $Stmt$ type is extended:

	{\color{ForestGreen}
		\type{Stmt}{Assign | MultiAssign | If | While | For}
	}

	Now, the type is defined:

	{\color{ForestGreen}
		\begin{record}{MultiAssign}
			lhs: Id*\\
			rhs: Expr*
		\end{record}
	}

	\subsection{$MultiAssign$ context conditions}

	The $wf-Stmt$ function is extended, adding a further case for the $MultiAssign$ $Stmt$ type:

	{\color{Red}
		\begin{fn}{wf-Stmt}{mk-MultiAssign(lhs, rhs), tpm}
			\begin{formbox}
				\elems{lhs} \subseteq \dom{tpm} \And\\
				\len{lhs} = \len{rhs} \And\\
				\forall{i \in \inds{lhs}}{c-type(lhs(i)) = c-type(rhs(i))} \And \\
				\forall{j, k \in \inds{lhs}}{lhs(j) = lhs(k) \implies j = k}
			\end{formbox}
		\end{fn}
	}

	\subsection{$MultiAssign$ semantics}

	The semantics of the $MultiAssign$ statement is given below:

	{\color{RoyalBlue}
		\SOSRule
		{	values: \seqof{ScalarValue} \\
			\forall{ i \in \inds{lhs}}{(rhs(i), \sigma) \Rex values(i) }}
		{
			(mk-MultiAssign(lhs,rhs), \sigma) \Rst \\
			\hspace{9em}\sigma \owr \map{lhs(k) \mapsto values(k) \in lhs \x values | k \in \inds{lhs} }}
	}

	\subsection{Assumptions}

	We have made the decision here to enforce the uniqueness of left-hand side identifiers, so that multiple values cannot be assigned to the same identifier. The context conditions also enforce an equal number of identifiers and values for assignation to prevent the ambiguous situation of assignment with fewer values than identifiers.

	It is worth noting that all values and expressions are evaluated in the same $\Sigma$, allowing for the case of swapping the values of variables without overwriting.

	\section{Task D}

	An extension of COOL to include two new types of $Stmt$---$RemoteRead$ and $RemoteWrite$---which allow objects to read from and write to variables in remote objects without using method calls.

	\subsection{$RemoteRead$ and $RemoteWrite$ abstract syntax}

	The abstract syntax, as provided in the specification, is given below:

	{\color{ForestGreen}
		\begin{record}{RemoteRead}
			lhs: Id\\
			object: Id\\
			variable: Id
		\end{record}

		\begin{record}{RemoteWrite}
			object: Id\\
			variable: Id\\
			rhs: Expr
		\end{record}
	}

	\subsection{$RemoteRead$ and $RemoteWrite$ context conditions}

	Context conditions are beyond the scope of this assignment, but we have discussed the issue, and the basic checks on typing and existence of objects and fields would still be written as expected. Introducing this kind of statement, however, does require changing the overall rules for $wf-Stmt$: without this addition, the function signature is:

	{\color{Red}
		\begin{fn}{wf-Stmt}{s, ctps, v-env}
			\signature{
				Stmt \x ClassTypes \x VarEnv \to  \Bool
			}
			\hbox{given by cases}
		\end{fn}
	}

	The sticking point is the $VarEnv$ parameter here: this refers to the variable environment of the object executing the statement in one of its methods. In order to perform well-formedness checks on the $RemoteRead$ and $RemoteWrite$ statements, the variable environment of both objects must be known. To introduce this would require a rewriting of all the $wf-Stmt$ functions to take this into account.

	\subsection{$RemoteRead$ and $RemoteWrite$ semantics}

	The semantic rules for $RemoteRead$ are as follows:

	{\color{RoyalBlue}
		\SOSRule
		{	O(a) = mk-ObjectInfo(c\sb{a}, \sigma\sb{a}, mk-Active\\
			\hspace{13em}(\seq{mk-RemoteRead(lhs, obj, var)} \sconc  rl, clt)) \\
			b = \sigma\sb{a}(obj)\\
			O(b) = mk-ObjectInfo(c\sb{b}, \sigma\sb{b}, mode)\\
			\sigma\sb{a} ' = \sigma\sb{a} \owr \map{lhs \mapsto \sigma\sb{b}(var)}\\
			aobj' = mk-ObjectInfo(c\sb{a}, \sigma\sb{a}', mk-Active(rl, clt))
			}
		{
			(C, O) \Rst O \owr \map{a \mapsto aobj'}
		}
	}

	For $RemoteWrite$, the rules are:

	{\color{RoyalBlue}
		\SOSRule
		{	O(a) = mk-ObjectInfo(c\sb{a}, \sigma\sb{a}, mk-Active\\
			\hspace{13em}(\seq{mk-RemoteWrite(obj, var, rhs)} \sconc  rl, clt)) \\
			b = \sigma\sb{a}(obj)\\
			O(b) = mk-ObjectInfo(c\sb{b}, \sigma\sb{b}, \const{Idle})\\
			(rhs, \sigma\sb{a}) \Rex v\\
			\sigma\sb{b}' = \sigma\sb{b} \owr \map{var \mapsto v}\\
			aobj' = mk-ObjectInfo(c\sb{a}, \sigma\sb{a}, mk-Active(rl, clt))\\
			bobj' = mk-ObjectInfo(c\sb{b}, \sigma\sb{b}', \const{Idle})
		}
		{
			(C, O) \Rst O \owr \map{a \mapsto aobj', b \mapsto bobj'}
		}
	}

	\subsection{Assumptions}

	We have made an important decision about the construction of the $RemoteRead$ and $RemoteWrite$ statements in regards to their synchronisation: while $RemoteRead$ can be executed on an object in \emph{any} mode of operation (Idle, Wait, or Active), $RemoteWrite$ may only be executed on an Idle object.

	We took the decision to make $RemoteWrite$ operable only on Idle objects to avoid writing to variables while they are being used in execution, and thereby causing unpredictable behaviour of Active objects. This does, however, mean that the client object (executing the $RemoteWrite$ statement) must halt execution if the target object is not Idle, and this situation could lead to deadlocks. The programmer must exercise caution to avoid this scenario.

	In contrast, $RemoteRead$ may be executed on an object in any mode of operation. While this does not cause any risk of the target object executing strangely, as none of its variables are changed, there is a danger that two sequential $RemoteRead$s to the same object variable could result in different answers, if the target object is changed in any way (such as by its own execution, or a $RemoteWrite$ statement). Again, the programmer must take care to prevent this from occurring.



\end{document}
