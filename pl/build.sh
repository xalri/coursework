#!/usr/bin/env bash
pandoc -V classoption:vdm --highlight-style=espresso -o $1.pdf $1.md
