---
title: "CSC3321: Understanding Programming Languages Coursework"
author:
- "Lewis Hallam"
- "Yuan Shen Hsieh"
- "Atakan Izdas"
- "Fuou Qian"
date: May 2018
geometry: margin=3cm
fontsize: 11pt
output: pdf_document
header-includes:
 - \usepackage{vdm}
 - \usepackage[usenames,dvipsnames]{xcolor}
 - \linespread{1.1}
...

\def\SOSRule#1#2{\begin{vdm}\begin{formula}\raisebox{-0.5\baselineskip}{\mbox{$\begin{array}[b]{l}#1\\ \hline #2\end{array}$}}\end{formula}\end{vdm}}
\def\Rslp{\buildrel sl'\over\longrightarrow}
\def\Rsl{\buildrel sl\over\longrightarrow}
\def\Rst{\buildrel st\over\longrightarrow}
\def\Rpar{\buildrel par\over\longrightarrow}
\def\Rs{\buildrel s\over\longrightarrow}
\def\Re{\buildrel e\over\longrightarrow}
\def\Rex{\buildrel ex\over\longrightarrow}
\def\Ret{\buildrel et\over\longrightarrow}
\def\Ri{\buildrel i\over\longrightarrow}
\def\Rp{\buildrel p\over\longrightarrow}
\def\Rlhv{\buildrel lhv\over\longrightarrow}

# Task A
_Extending __Base__ with conditional expressions._

The abstract syntax definition is modified to include conditional
expressions, unary operators, and more relational operators:

   \begin{record}{BaseProgram}
      vars: Id \mapsto Type \\
      body: \seqof{Stmt}
   \end{record}

   \type{Type}{IntTp | BoolTp}

   \type{Stmt}{Assign | If | While}

   \begin{record}{Assign}
      lhs: Id \\
      rhs: Expr
   \end{record}

   \begin{record}{If}
      test: Expr \\
      then: \seqof{Stmt} \\
      else: \seqof{Stmt}
   \end{record}

   \begin{record}{While}
      test: Expr \\
      body: \seqof{Stmt} \\
   \end{record}

   \type{Expr}{BinaryExpr | UnaryExpr | RelExpr | CondExpr | Id | \Int | \Bool }

   \begin{record}{BinaryExpr}
      lhs: Expr \\
      operator: Plus | Minus \\
      rhs: Expr \\
   \end{record}

   \begin{record}{UnaryExpr}
      operator: Negate \\
      operand: Expr \\
   \end{record}

   \begin{record}{RelExpr}
      lhs: Expr \\
      operator: Equals | NotEquals | Less | Greater | LessEqual | GreaterEqual \\
      rhs: Expr \\
   \end{record}

   \begin{record}{CondExpr}
      test: Expr \\
      then: Stmt \\
      else: Stmt
   \end{record}


The operands of a $CondExpr$ are $Expr$s, so statements such as the following are
valid:

$$
k \leftarrow \text{ if } (\text{ if } i \geq 0 \text{ then } i \text{ else } j \text{ fi }) \geq 0 \text{ then } 7 \text{ else } 42 \text{ fi }
$$

A $CondExpr$ is valid if $test$ evaluates to a boolean, and $then$ and $else$ both
evaluate to the same type.


# Taks B

_Adding arrays to __Base__._

To the abstract syntax we add the record types $ArrayType$ (representing the type of an
array, denoted $[\text{int}; 4]$ for an array of integers length $4$) and $ArrayValue$
(a literal array $[2, 4, 1, -1]$), and the $Subscript$ binary operator ($A[i]$ gives
the $i$th element of an array $A$, where $i$ is an expression which evaluates
to an integer).
Arrays are of fixed length and can't have a length of zero.
Nested arrays are allowed ($[[1, 2], [3,4]]$ is a value of type $[[\text{int}; 2]; 2]$)


\type{Type}{\const{IntTp} | \const{BoolTp} | ArrayTp}

   \begin{record}{ArrayTp}
      type: Type \\
      length: \mathbb{Z}^{+} \\
   \end{record}

   \type{Expr}{BinaryExpr | UnaryExpr | RelExpr | CondExpr | Id | Value }

   \type{Value}{ ArrayValue | \Int | \Bool }

   \begin{record}{ArrayValue}
      data: \seqof{Value} \\
   \end{record}

   \begin{record}{BinaryExpr}
      lhs: Expr \\
      operator: Plus | Minus | Subscript \\
      rhs: Expr \\
   \end{record}


## Context conditiona

$c-type$ is extended to give the type of an $ArrayValue$:

   \begin{fn}{c-type}{e, tpm}
      \signature{
         Expr \x TypeMap \to  ( \const{Type} | \const{Error})
      }
      ...
   \end{fn}

   \begin{fn}{c-type}{mk-ArrayValue(data), tpm} \\
      \If \forall {e \in data} c-type(e) = c-type(data(1)) \\
      \Then mk-ArrayType(c-type(data(1)), card(data) \\
      \Else Error
      \Fi
   \end{fn}

   \begin{fn}{c-type}{mk-BinaryExpr(lhs, Subscript, rhs), tpm} \\
      \If{
         \begin{formbox}
            ArrayType(type, len) = c-type(lhs, tpm) \And\\
            c-type(rhs, tpm) = \const{IntTp}
         \end{formbox}
      }
      \Then type
      \Else Error
      \Fi
   \end{fn}

# Task C

_Adding a $Case$ statement to __Base__._

We define the $Case$ statement to run the action of the first clause (in the order
they're defined) where the test evaluates to true:

   \SOSRule {
   }{
      (mk-Case([]), \sigma) \Rst \sigma
   }

   \SOSRule {
      (test, \sigma) \Rex true \\
      (action, \sigma) \Rex \sigma'
   }{
      (mk-Case([mk-Clause(test, action)] \curvearrowright rest), \sigma) \Rst \sigma'
   }

   \SOSRule {
      (test, \sigma) \Rex false \\
      (rest, \sigma) \Rsl \sigma'
   }{
      (mk-Case([mk-Clause(test, action)] \curvearrowright rest), \sigma) \Rst \sigma'
   }

The $test$ of a clause must evaluate to true.

# Task D

The semantics of $TryMethCall$ are idential to $MethCall$, except there's an
extra case to run the alternative if the server isn't $\const{Ready}$.


   \SOSRule {
      cobj = O(c) \\
      mk-ObjInfo(cl\sb{c}, \sigma\sb{c} \\
      mk-Run([mk-TryMethCall(lhs, obj, meth, args, alternative)] \curvearrowright rl\sb{c}, k\sb{c})) = cobj \\
      sobj = O(\sigma\sb{c}(obj)) \\
      mk-ObjInfo(cl\sb{s}, \sigma\sb{s}, \const{Ready}) = sobj \\
      mk-ClBlock(\textit{vm}, \textit{mm}) = C(cl@s) \\
      cobj' = mk-ObjInfo(cl@c, \sigma@c, mk-Wait(lhs, rl@c, k@c)) \\
      \sigma'@s = \sigma@s \owr \map{(mm(meth).ps)(i) \mapsto \sigma@c(args(i)) | i \in \inds args} \\
      sobj' = mk-ObjInfo(cl@s, \sigma'@s, mk-Run(mm(meth).body, c))
   }{
      (C, O) \Rst O \owr \map{ c \mapsto cobj', \sigma@c(obj) \mapsto sobj' }
   }

   \SOSRule {
      cobj = O(c) \\
      mk-ObjInfo(cl\sb{c}, \sigma\sb{c} \\
      mk-Run([mk-TryMethCall(lhs, obj, meth, args, alternative)] \curvearrowright rl\sb{c}, k\sb{c})) = cobj \\
      sobj = O(\sigma\sb{c}(obj)) \\
      mk-ObjInfo(cl\sb{s}, \sigma\sb{s}, mk-Run(...)) = sobj \\
      cobj' = mk-ObjInfo(cl@c, \sigma@c, mk-Run(alternative \curvearrowright rl, k@c)) \\
   }{
      (C, O) \Rst O \owr \map{ c \mapsto cobj' }
   }

   \SOSRule {
      cobj = O(c) \\
      mk-ObjInfo(cl\sb{c}, \sigma\sb{c} \\
      mk-Run([mk-TryMethCall(lhs, obj, meth, args, alternative)] \curvearrowright rl\sb{c}, k\sb{c})) = cobj \\
      sobj = O(\sigma\sb{c}(obj)) \\
      mk-ObjInfo(cl\sb{s}, \sigma\sb{s}, mk-Wait(...)) = sobj \\
      cobj' = mk-ObjInfo(cl@c, \sigma@c, mk-Run(alternative \curvearrowright rl, k@c)) \\
   }{
      (C, O) \Rst O \owr \map{ c \mapsto cobj' }
   }
