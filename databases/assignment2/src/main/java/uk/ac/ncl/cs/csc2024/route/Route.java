/**
 * csc2024-hibernate-assignment
 *
 * Copyright (c) 2015 Newcastle University
 * Email: <h.firth@ncl.ac.uk/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ncl.cs.csc2024.route;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import uk.ac.ncl.cs.csc2024.busstop.BusStop;
import uk.ac.ncl.cs.csc2024.operator.Operator;

import javax.persistence.*;
import java.util.Set;

/**
 * Hibernate Operator Entity
 *
 * Task: Create fields, methods and annotations which implicitly define an appropriate database table schema for
 * Operator records.
 *
 * @author hugofirth
 */
@Entity
@NamedQueries({
        @NamedQuery(name = Route.SELECT_ALL, query =
                "select r from Route r order by r.number asc"),

        @NamedQuery(name = Route.INCLUDING_STATION, query =
                "select r from Route r "+
                "where r.start.description = 'Railway Station' or "+
                      "r.destination.description = 'Railway Station'"),

        @NamedQuery(name = Route.OK_FREQUENCY, query =
                "select sum(r.frequency / r.operators.size) from Route r "+
                "left join r.operators operators "+
                "where operators.name = 'OK Travel'"),
})
@Table(name = "route")
public class Route {
    public static final String SELECT_ALL = "Route.selectAll";
    public static final String INCLUDING_STATION = "Route.includingStation";
    public static final String OK_FREQUENCY = "Route.okFrequency";

    @Id public String number;
    public int frequency;
    @ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    public BusStop start;
    @ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    public BusStop destination;

    @ManyToMany( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinTable( name = "route_operators",
                joinColumns = @JoinColumn( name="route_number" ),
                inverseJoinColumns = @JoinColumn( name="operator_name" ) )
    public Set<Operator> operators;

    public String getNumber() { return this.number; }
    public int getFrequency() { return this.frequency; }
    public BusStop getStart() { return this.start; }
    public BusStop getDestination() { return this.destination; }
    public Set<Operator> getOperators() { return this.operators; }

    public static DetachedCriteria routesContainingStop(String description) {
        DetachedCriteria stop = DetachedCriteria.forClass(BusStop.class)
                .setProjection(Property.forName("id"))
                .add(Property.forName("description").eq(description));

        return DetachedCriteria.forClass(Route.class, "r")
                .add(Restrictions.or(
                        Property.forName("r.start").in(stop),
                        Property.forName("r.destination").in(stop)
                ));
    }

    public static DetachedCriteria routesByOperator(String name) {
        return DetachedCriteria.forClass(Route.class, "r")
                .createAlias("r.operators", "operators")
                .add(Property.forName("operators.name").eq(name));
    }
}
