/**
 * csc2024-hibernate-assignment
 *
 * Copyright (c) 2015 Newcastle University
 * Email: <h.firth@ncl.ac.uk/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ncl.cs.csc2024.operator;

import uk.ac.ncl.cs.csc2024.route.Route;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * Hibernate Operator Entity
 *
 * Task: Create fields, methods and annotations which implicitly define an appropriate database table schema for
 * Operator records.
 *
 * @author hugofirth
 */
@Entity
@NamedQueries({
        @NamedQuery(name = Operator.SELECT_ALL, query =
                "select o from Operator o order by o.name asc"),

        @NamedQuery(name = Operator.ROUTES_BY_DIAMOND, query =
                "select r from Route r left join r.operators operators where operators.name = 'Diamond Buses'"),

        @NamedQuery(name = Operator.SERVING_PARK_GATES, query =
                "select o from Operator o left join o.routes routes where "+
                        "routes in (select r from Route r where "+
                        "r.start.description = 'Park Gates' or r.destination.description = 'Park Gates')")
})
@Table(name = "operator")
public class Operator {
    public static final String SELECT_ALL =  "Operator.selectAll";
    public static final String ROUTES_BY_DIAMOND =  "Operator.routesByDiamond";
    public static final String SERVING_PARK_GATES =  "Operator.servingParkGates";

    @Id public String name;
    public String street;
    public String town;
    public String postcode;
    public String email;
    public String phone;

    @ManyToMany( cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "operators" )
    public Set<Route> routes;

    public String getName() { return this.name; }
    public String getStreet() { return this.street; }
    public String getTown() { return this.town; }
    public String getPostcode() { return this.postcode; }
    public String getEmail() { return this.email; }
    public String getPhone() { return this.phone; }
    public Set<Route> getRoutes() { return this.routes; }
}
