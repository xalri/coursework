/**
 * csc2024-hibernate-assignment
 *
 * Copyright (c) 2015 Newcastle University
 * Email: <h.firth@ncl.ac.uk/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ncl.cs.csc2024.route;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Query;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.Type;
import uk.ac.ncl.cs.csc2024.busstop.BusStop;
import uk.ac.ncl.cs.csc2024.operator.Operator;
import uk.ac.ncl.cs.csc2024.query.ExampleQuery;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hibernate.type.StandardBasicTypes.DOUBLE;
import static org.hibernate.type.StandardBasicTypes.INTEGER;

/**
 * Collection of Queries relating to Route entities
 *
 * Task: fill out method bodies to create "Queries" which satisfy the requirements laid out in the coursework spec.
 * Other than the `insert(...)` method, where you should return the session to which an Entity object has been persisted,
 * you should return an ExampleQuery object.
 *
 * The methods of the ExampleQuery objects should return a Query, a Named Query's identifier and a Criteria Query for
 * each task laid out in the coursework spec. You should return every query type for each task.
 *
 * An example of how this should look is provided in the `selectAll(...)` query.
 *
 * @author hugofirth
 */
public class RouteQueries {

    public static Session insert(final Map<String, String> row, final Session session) {
        Route route = new Route();
        route.number = row.get("number");
        route.frequency = Integer.parseInt(row.get("frequency"));
        route.start = (BusStop) session.get(BusStop.class, Long.parseLong(row.get("start")));
        route.destination = (BusStop) session.get(BusStop.class, Long.parseLong(row.get("destination")));
        route.operators = new HashSet<>();

        for(String value : row.get("operators").split("\\|")) {
           String name = value.trim();
           Operator o = (Operator) session.get(Operator.class, name);
           route.operators.add(o);
        }

        session.save(route);
        return session;
    }

    public static ExampleQuery selectAll() {
        return new ExampleQuery() {
            @Override
            public Query getQuery(Session session) {
                return session.createQuery("select r from Route r order by number asc");
            }

            @Override
            public String getNamedQueryName() {
                return Route.SELECT_ALL;
            }

            @Override
            public Criteria getCriteria(Session session) {
                Criteria criteria = session.createCriteria(Route.class, "r");
                criteria.addOrder(Order.asc("r.number"));
                return criteria;
            }
        };
    }


    public static ExampleQuery selectAllForRailwayStation() {
        return new ExampleQuery() {
            @Override
            public Query getQuery(Session session) {
                return session.createQuery("select r from Route r where "+
                        "r.start.description = 'Railway Station' or "+
                        "r.destination.description = 'Railway Station'");
            }

            @Override
            public String getNamedQueryName() {
               return Route.INCLUDING_STATION;
            }

            @Override
            public Criteria getCriteria(Session session) {
                return Route.routesContainingStop("Railway Station").getExecutableCriteria(session);
            }
        };
    }

    public static ExampleQuery cumulativeFrequencyByOkTravel() {
        return new ExampleQuery() {
            @Override
            public Query getQuery(Session session) {
                return session.createQuery("select sum(r.frequency / r.operators.size) "+
                        "from Route r left join r.operators operators "+
                        "where operators.name = 'OK Travel'");
            }

            @Override
            public String getNamedQueryName() {
                return Route.OK_FREQUENCY;
            }

            @Override
            public Criteria getCriteria(Session session) {
                // I'm probably missing something here, some way of getting this result without
                // using SQL projection... but regardless, this seems to work ^^
                return session.createCriteria(Route.class, "r")
                       .createAlias("r.operators", "operators")
                       .add(Property.forName("operators.name").eq("OK Travel"))
                       .setProjection(
                          Projections.sqlProjection(
                                  "sum(frequency / (select count(ops.route_number) from route_operators ops "+
                                  "where number=ops.route_number)) as total",
                                  new String[] { "total" },
                                  new Type[] { INTEGER }));
            }
        };
    }


}
