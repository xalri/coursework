# CSC2024 Database Technology Coursework 2

This piece of coursework is worth 10% of the total assessment for this module.
It is based upon the second part of this module. You are expected to make use of
MySQL, Java, Hibernate  the JPA specification to complete this coursework. Submit
your final project directory in a .zip folder to NESS.


## Aims:

To assess your ability to:
 - Define Entity beans according to the JPA spec (with @Annotations etc…) to
   implicitly re create a familiar schema.
 - Construct HQL (Hibernate Query Language) statements and Hibernate Criteria
   Queries to insert, select and aggregate these entities.

The database schema you are being asked to implement, and the queries you are
being asked to write, should be familiar to you from your last coursework
however there are some key differences which you should note before attempting
the coursework.


## The data model:

The transport department of a city council would like to implement a database
for bus users indicating where bus routes go, how frequently they run and how
to contact the relevant bus operator.
The council has identified some data items they would like to be recorded and
also have provided some sample data (this can be found at the very end of this
assignment).
For the bus operators, the council simply want to store their name (unique) and
some basic contact details.
For bus stops, they wish to store each stop’s unique reference number (or ID)
and a description of that stop’s location, e.g. “Railway Station.”
For the bus routes, they wish to store the route number (unique), the starting
and destination bus stops and the frequency – the number of buses per hour. Note
that the inclusion of both start and end bus stops makes the relationship between
stop and route a 2-many relationship.
They would also like to know which bus operators work each route. Some routes
are shared between multiple operators with each operator working an equal proportion
of the journeys on that route.


## Tasks:

i)  Annotate the Java classes. [20% of total marks]
i1) Add annotations to classes for BusStop, Operator  & Route, so that they
    become Entity classes that Hibernate knows how to persist in the database.
    You should make use of sensible types, defaults and annotations.
i2) Use relationship @Annotations to define the equivalent of the Operates and
    the Uses pivot tables from the previous assignment. Note: It is not possible
    to define additional fields in the pivot tables automatically created by
    Hibernate annotations. You should therefore not worry about providing a
    proportion field, as you did in the first assignment. In the case that a
    route has multiple operators, it is assumed that they split the frequency
    evenly. __ marks

ii) Complete the methods for inserting sample data into the database.
    [30% of total marks]
    You should complete the stub methods in the BusStopQueries, OperatorQueries,
    and RouteQueries to take a map of Strings and save it as the appropriate entity
    object. Notes:
    - The sample data is provided for you.
    - Your insert(...) will be called, providing a map of strings for each database row
      to be inserted. The map will take the form
      `{“id” ->  ”1015”, “description” -> “Quayside”}`.
    - Though all the values will be provided to you as strings, you should use parsing
      methods to convert them to the correct types before attempting to insert them
      into the database.
    - For routes which may have multiple operators, the string of operator names is
      separated by the | symbol. You should parse and split this appropriately to
      create a set before inserting.

iii) Implement HQL queries.  [50% of total marks]
     Complete the stub methods in the BusStopQueries, OperatorQueries, and
     RouteQueries classes to return an HQL Query, a Named Query and a Criteria
     Query to answer the queries below.
     Notes:
     - You will effectively be answering each query 3 times.
     - Named Queries should be defined in the relevant Entity class. Only the
       string identifier should be returned in the corresponding `___Queries` class.
     - In every case, a selectAll query has been provided in a completed state
       to show you what to do.
     Queries:
     - Write a query which would return the Route objects operated by Diamond
       Buses. OperatorQueries.selectAllRoutesByDiamondBuses()
     - Write a query which would return the BusStop object with the highest Id.
       BusStopQueries.selectMaxId()
     - Write a query which would return the Route objects which serve the Railway
       Station. RouteQueries.selectAllForRailwayStation()
     - Write a query which would return the integer value of Buses per hour
       operated by OK Travel. Note: Remember that workload is evenly distributed
       amongst operators. RouteQueries.cumulativeFrequencyByOkTravel()
     - Write a query which would return the Operator objects which serve Park
       Gates. OperatorQueries.selectAllForParkGates()


##Testing your work

You have been provided with extensive Unit tests to both test your queries and
help you with common errors. You may find these tests in the src/test/java
package and execute them as shown in practical.
If you look at the test package you will see several sub packages (busstop,
operator, route). These contain the tests for each Entity, however, you should
not run them directly. Instead, in the root of the package you will see several
test suite classes (InsertQueriesTestSuite, SelectQueriesTestSuite etc…). You
should run these (as you would a normal test: Right Click -> Run As …) as they
perform some necessary setup for each test.
As you are going through the coursework you will receive many errors for tests
where you have not yet implemented a query. You may ignore these, either manually
or by placing an @Ignore annotation above the @Test annotation of the relevant
test. Don’t forget to remove the @Ignore annotation later, otherwise you will
never know whether those tests will have succedeed!
Finally, you should attempt to complete the Entity classes and insert methods
early on in the coursework, as all of the later tests rely upon these.


This is the end of the assignment.

