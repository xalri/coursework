## i. Completing the entity relationship diagram

Changes from the initial diagram:

 - `Operates` relation labeled as many-to-many.
 - `Route` -> `Uses` marked as total, every route has exactly two stops.
 - `Email` attribute added to `Operator`
 - `ID` column added to operators: even with the uniqueness of the name
   guaranteed by the specification, a name could change in the future, so
   shouldn't be used as a primary key.
 - Primary keys have been indicated with underlines.

![Entity relationship diagram](entity_relationship.png)


\pagebreak

## ii. Implementing the database

```sql
CREATE DATABASE bus;
USE bus;

CREATE TABLE operators (
   /* ID field added as the primary key to make changing operator names easier */
   id integer PRIMARY KEY,
   /* Name must still be unique */
   name varchar(50) NOT NULL UNIQUE,
   street varchar(100) NOT NULL,
   town varchar(50) NOT NULL,
   postcode varchar(10) NOT NULL,
   email varchar(100) NOT NULL,
   phone varchar(16) NOT NULL
);

CREATE TABLE stops (
   id integer PRIMARY KEY,
   name varchar(50) NOT NULL
);

CREATE TABLE routes (
   /* Route IDs can include non-numeric characters, e.g. "16a" */
   id varchar(10) PRIMARY KEY,
   first_stop integer NOT NULL REFERENCES stops(id)
      ON UPDATE CASCADE,
   last_stop integer NOT NULL REFERENCES stops(id)
      ON UPDATE CASCADE,
   /* Frequency in number of buses per hour */
   frequency integer NOT NULL
);

CREATE TABLE route_operators (
   route_id varchar(10) NOT NULL REFERENCES routes(id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
   operator_id integer NOT NULL REFERENCES operators(id)
      ON UPDATE CASCADE,
   /* Proportion as a percentage, 000.01 to 100.00 */
   proportion DECIMAL(5, 2) NOT NULL,
   PRIMARY KEY (route_id, operator_id)
);
```
\pagebreak

## iii. Querying the database

a) What is the location of the bus stop with the highest stop ID?
```sql
SELECT name FROM stops WHERE id = (SELECT MAX(id) FROM stops);
```
```
+------------+
| name       |
+------------+
| Park Gates |
+------------+
```

b) List as briefly as you can, the name and phone number of the bus operators
serving the Shopping Centre.
```sql
SELECT DISTINCT name,phone
   FROM operators,route_operators
   WHERE operators.id = route_operators.operator_id AND
         route_operators.route_id IN (
            SELECT routes.id FROM routes,stops WHERE
               (routes.first_stop = stops.id OR routes.last_stop = stops.id)
               AND stops.name = "Shopping Centre"
         );
```
```
+---------------+---------------+
| name          | phone         |
+---------------+---------------+
| Diamond Buses | 0191 267 8937 |
| Lockey’s      | 0191 340 1934 |
+---------------+---------------+
```


c) How many bus journeys per hour are operated by Venture Travel?
```sql
SELECT SUM(frequency * (proportion/100)) AS journeys
   FROM routes,route_operators
   WHERE routes.id = route_operators.route_id AND
         route_operators.operator_id =
            (SELECT id FROM operators WHERE name = "Venture Travel");
```
```
+----------+
| journeys |
+----------+
| 6.000000 |
+----------+
```

d) How many bus stops serve as the starting point for a bus route but have no
routes which terminate there?
```sql
SELECT COUNT(id) FROM stops WHERE
   id IN (SELECT first_stop FROM routes)
   AND NOT id IN (SELECT last_stop FROM routes);
```
```
+-----------+
| COUNT(id) |
+-----------+
|         3 |
+-----------+
```


## iv. Adding contact details

![Updated entity relationship diagram](entity_relationship_2.png)


## v. Storing the full routes

 - Remove the `Start` and `Destination` attributes from `Route`.
 - Make the `Uses` relation many-to-many.
  - This would be implemented in the database as a new table, `route_stops`, with
    columns `route_id` referencing the key of the `routes` table, and `stop_id`
    referencing the `stops` table. The primary key would be these two columns
    composed.
  - The database would then need to be updated to populate this table.

\pagebreak

## Appendix A - Populating the database

Here is the SQL used to add the given sample data to the database.

```sql
USE bus;

INSERT INTO operators (id, name, street, town, postcode, email, phone) VALUES
   (0, "Venture Travel", "Venture House", "Consett", "DH8 8SV",
       "info@venturetravel.co.uk", "01207 222 145"),
   (1, "OK Travel", "Bondgate", "Durham", "DH2 2BC",
       "passengerservices@ok.com", "0191 301 3012"),
   (2, "Lockey’s", "The Garage", "Durham", "DH1 1AB",
       "contact@lockeysbuses.co.uk", "0191 340 1934"),
   (3, "Bond Brothers", "Coronation Terrace", "Durham", "DH2 3AG",
       "jeff.bond@bondbuses.com", "0191 333 1234"),
   (4, "Diamond Buses", "Diamond Buildings", "Newcastle", "NE2 5JH",
       "info@diamondbuses.co.uk", "0191 267 8937");

INSERT INTO stops (id, name) VALUES
   (1015, "Quayside"),
   (1023, "Ferry terminal"),
   (1500, "City Centre"),
   (6700, "Airpor"),
   (7628, "Shopping Centre"),
   (9015, "Railway Station"),
   (9016, "Railway Station"),
   (9022, "Park Gates");

INSERT INTO routes (id, first_stop, last_stop, frequency) VALUES
   ("1",   9015, 9022, 2),
   ("16",  7628, 1500, 6),
   ("16a", 7628, 9022, 2),
   ("21",  1023, 1015, 4),
   ("22",  1023, 1500, 1),
   ("30",  1015, 1500, 4),
   ("64",  9015, 7628, 6),
   ("88",  9016, 1015, 2),
   ("100", 6700, 1500, 4),
   ("111", 6700, 9016, 4);

INSERT INTO route_operators(route_id, operator_id, proportion) VALUES
   ("1",   0, 100),
   ("16",  4, 100),
   ("16a", 4, 100),
   ("21",  3, 100),
   ("22",  3, 100),
   ("30",  3, 100),
   ("64",  2, 100),
   ("88",  0, 100),
   ("100", 1, 100),
   ("111", 0, 50),
   ("111", 1, 50);
```
