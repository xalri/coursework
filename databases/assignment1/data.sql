USE bus;

INSERT INTO operators (id, name, street, town, postcode, email, phone) VALUES
   (0, "Venture Travel", "Venture House", "Consett", "DH8 8SV", "info@venturetravel.co.uk", "01207 222 145"),
   (1, "OK Travel", "Bondgate", "Durham", "DH2 2BC", "passengerservices@ok.com", "0191 301 3012"),
   (2, "Lockey’s", "The Garage", "Durham", "DH1 1AB", "contact@lockeysbuses.co.uk", "0191 340 1934"),
   (3, "Bond Brothers", "Coronation Terrace", "Durham", "DH2 3AG", "jeff.bond@bondbuses.com", "0191 333 1234"),
   (4, "Diamond Buses", "Diamond Buildings", "Newcastle", "NE2 5JH", "info@diamondbuses.co.uk", "0191 267 8937");

INSERT INTO stops (id, name) VALUES
   (1015, "Quayside"),
   (1023, "Ferry terminal"),
   (1500, "City Centre"),
   (6700, "Airpor"),
   (7628, "Shopping Centre"),
   (9015, "Railway Station"),
   (9016, "Railway Station"),
   (9022, "Park Gates");

INSERT INTO routes (id, first_stop, last_stop, frequency) VALUES
   ("1",   9015, 9022, 2),
   ("16",  7628, 1500, 6),
   ("16a", 7628, 9022, 2),
   ("21",  1023, 1015, 4),
   ("22",  1023, 1500, 1),
   ("30",  1015, 1500, 4),
   ("64",  9015, 7628, 6),
   ("88",  9016, 1015, 2),
   ("100", 6700, 1500, 4),
   ("111", 6700, 9016, 4);

INSERT INTO route_operators(route_id, operator_id, proportion) VALUES
   ("1",   0, 100),
   ("16",  4, 100),
   ("16a", 4, 100),
   ("21",  3, 100),
   ("22",  3, 100),
   ("30",  3, 100),
   ("64",  2, 100),
   ("88",  0, 100),
   ("100", 1, 100),
   ("111", 0, 50),
   ("111", 1, 50);
