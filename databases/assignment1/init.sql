CREATE DATABASE bus;
USE bus;

CREATE TABLE operators (
   /* ID field added as the primary key to make changing operator names easier */
   id integer PRIMARY KEY,
   /* Name must still be unique */
   name varchar(50) NOT NULL UNIQUE,
   street varchar(100) NOT NULL,
   town varchar(50) NOT NULL,
   postcode varchar(10) NOT NULL,
   email varchar(100) NOT NULL,
   phone varchar(16) NOT NULL
);

CREATE TABLE stops (
   id integer PRIMARY KEY,
   name varchar(50) NOT NULL
);

CREATE TABLE routes (
   /* Route IDs can include non-numeric characters, e.g. "16a" */
   id varchar(10) PRIMARY KEY,
   first_stop integer NOT NULL REFERENCES stops(id)
      ON UPDATE CASCADE,
   last_stop integer NOT NULL REFERENCES stops(id)
      ON UPDATE CASCADE,
   /* Frequency in number of busses per hour */
   frequency integer NOT NULL
);

CREATE TABLE route_operators (
   route_id varchar(10) NOT NULL REFERENCES routes(id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
   operator_id integer NOT NULL REFERENCES operators(id)
      ON UPDATE CASCADE,
   /* Proportion as a percentage, 000.01 to 100.00 */
   proportion DECIMAL(5, 2) NOT NULL,
   PRIMARY KEY (route_id, operator_id)
);

