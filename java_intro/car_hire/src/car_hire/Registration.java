package car_hire;

import java.util.Set;
import java.util.HashSet;

/**
 * A Registration number made of two components: a single letter, and
 * a 4 digit number.
 */
public final class Registration {
    private static final Set<Registration> used = new HashSet<>();

    private char a;
    private int b;

    /** Creates a registration with the given components. The components
     * must each be valid (a letter for a and a four digit number for b),
     * and together they must represent a unique registration, or an
     * IllegalArgumentException will be thrown. */
    public Registration(char a, int b) {
        a = Character.toLowerCase(a);
        if((int) a < 97 || (int) a > 97 + 26)
            throw new IllegalArgumentException("a must be a letter (a-z,A-Z)");
        if(b < 0 || b > 9999)
            throw new IllegalArgumentException("b must be a four digit number (0-9999)");

        this.a = a;
        this.b = b;
        // Lock the HashSet to avoid data races.
        synchronized (Registration.used) {
            if (Registration.used.contains(this))
                throw new IllegalArgumentException("Registration already exists.");
        }
    }

    /** Create a random unique registration for testing. */
    Registration(){
        if(Registration.used.size() >= 10000 * 26)
            throw new IllegalStateException("No more unique registrations can be created");
        this.a = (char)(Math.random() * 26 + 97);
        this.b = (int)(Math.random() * 9999);

        synchronized (Registration.used) {
            while (Registration.used.contains(this)) {
                this.a = (char)(Math.random() * 26 + 97);
                this.b = (int) (Math.random() * 9999);
            }
            Registration.used.add(this);
        }
    }

    /** Get the first component of the registration */
    public char getChar(){ return this.a; }

    /** Get the second component of the registration */
    public int getNum(){ return this.b; }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        Registration r = (Registration) o;
        return this.a == r.a && this.b == r.b;
    }

    @Override public int hashCode() {
        int result = (int) this.a;
        result = 31 * result + this.b;
        return result;
    }

    @Override public String toString() {
        return  this.a + String.format("%04d", this.b);
    }
}
