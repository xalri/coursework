package car_hire;

import com.sun.istack.internal.Nullable;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/** A driving licence */
public final class Licence {
    private static final Map<String,Licence> used = new HashMap<>();

    /** True if the licence is full - A licence must be a full licence for the
     * holder to hire a car. */
    public final boolean full;

    /** The car currently rented by the person who holds this licence.
     *  (package-private to allow access by HireManager) */
    @Nullable Car car;

    private final String[] name;
    // Dates are mutable, so must be private, getters return copies.
    private final Date dob;
    private final Date issued;
    private final int serialNo;

    private Licence(String name, Date dob, Date issued, boolean full, int serialNo){
        this.car = null;
        this.name = name.split(" ");
        this.dob = dob;
        this.issued = issued;
        this.full = full;
        this.serialNo = serialNo;
        if(this.name.length < 2)
            throw new IllegalArgumentException("name must have at least two parts");
    }

    /** Creates a licence with the given parameters - if the licence already
     *  exists, a reference to the existing licence will be returned.
     *  The name must have at least two parts, separated by a space. */
    // I decided to use a factory method rather than a factory class here
    // for simplicity. The disadvantage is that there is only one `used` set,
    // but I don't foresee a use for multiple sets of licences with internal
    // uniqueness.
    public static Licence makeLicence(String name, Date dob, Date issued,
                                      boolean full, int serialNo){
        Licence l = new Licence(name,dob,issued,full,serialNo);

        // Lock on Licence.used to avoid data races if the library's used in a
        // threaded application.
        synchronized (Licence.used) {
            String str = l.toString();
            // If the licence already exists, return the existing licence
            if (used.containsKey(str)) return used.get(str);
            // Otherwise, add the created licence to used and return it.
            used.put(str, l);
            return l;
        }
    }

    /** Get the date of birth of the holder of this licence. Returns a copy of
     * the internally stored date, so mutation will not effect the licence */
    public Date getDob(){ return (Date) this.dob.clone(); }

    /** Get the date of this licence being issued. Returns a copy of the
     * internally stored date, so mutation will not effect the licence */
    public Date getIssueDate(){ return (Date) this.issued.clone(); }

    /** Get the full name of the licence holder */
    public String getName(){ return String.join(" ",this.name); }

    /** Get the first and last initials of the licence holder, initials are
     * always uppercase, regardless of case in the name */
    public String getInitials(){
        return ("" + this.name[0].charAt(0) +
                this.name[this.name.length - 1].charAt(0))
                .toUpperCase();
    }

    /** Get the licence number, a string representation of the licence.
     *  Consists of the licence holder's initials, the year the licence
     *  was issued, and a serial number. e.g. JD-2003-72 */
    public String getNo(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.issued);
        String issueYear = "" + cal.get(Calendar.YEAR);
        return this.getInitials() + "-" + issueYear + "-" + this.serialNo;
    }

    @Override public String toString() {
        return this.getNo();
    }
}
