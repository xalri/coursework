package car_hire;

import java.util.function.Function;
import java.util.function.Predicate;

/** A representation of different sizes of cars. Contains the fuel capacity,
 *  a function to calculate the consumption for a given distance, and a
 *  function to check if someone is eligible to hire the car. */
public enum CarSize {
    large("Large", 60,
            distance ->
                distance > 50 ?
                    (int) (Math.ceil(50.d / 15.d + (distance - 50) / 20.d)) :
                    (int) (Math.ceil(distance / 15.d))
            ,
            licence ->
                licence.full &&
                DateUtils.checkAge(licence.getDob(), 25) &&
                DateUtils.checkAge(licence.getIssueDate(), 5)
            ),

    small("Small", 45,
            distance -> (int)(Math.ceil(distance / 25.d)),
            licence ->
                licence.full &&
                DateUtils.checkAge(licence.getDob(), 21) &&
                DateUtils.checkAge(licence.getIssueDate(), 1)
                );

    /** The name of this size of car */
    public final String name;
    /** The fuel capacity in liters */
    public final int capacity;
    /** The fuel consumed by this size of car for a given distance */
    public final Function<Integer,Integer> consumption;
    /** True if the licence holder is eligiable to hire a car of this size */
    public final Predicate<Licence> canHire;

    CarSize(String name, int capacity, Function<Integer,Integer> consumption,
            Predicate<Licence> canHire){
        this.name = name;
        this.capacity = capacity;
        this.consumption = consumption;
        this.canHire = canHire;
    }
}
