package car_hire;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/** Assorted functions to make working with dates easier */
public final class DateUtils {
    /** Create a date with the given year, month and day */
    public static Date date(int year, int month, int day){
        Calendar cal = new GregorianCalendar();
        cal.set(year, month, day);
        return cal.getTime();
    }

    /** Check that the given date is _older_ than x years. */
    public static boolean checkAge(Date date, int years){
        Calendar cal = new GregorianCalendar();
        Calendar now = new GregorianCalendar();
        cal.setTime(date);
        now.add(Calendar.YEAR, -years);
        return cal.before(now);
    }
}
