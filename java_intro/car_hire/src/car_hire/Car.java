package car_hire;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.Optional;

/** A representation of a car for hire. Contains an instance of the CarSize
 *  enum, to give the fuel tank size and for calculating fuel consumption */
public final class Car {

    /** The size of the car */
    public final CarSize size;
    /** The car's registration number */
    @NotNull public final Registration registration;

    /** The licence of the current user of the car, null if the car is not
     * being used. (access is package-private to allow access by HireManager) */
    @Nullable Licence user = null;

    private int fuel;

    /** Creates a car object with the given size and a random registration */
    public Car(CarSize size){
        this.size = size;
        this.fuel = size.capacity;
        this.registration = new Registration();
    }

    /** Creates a car object with the given size and registration components.
     * Uniqueness of the registration is guaranteed by the Registration class -
     * An IllegalArgumentException will be thrown if the registration has been
     * created before. (To ensure the guarantee of no two cars having the same
     * registration, cars can not be constructed by passing a Registration
     * object, as this could be the same object as used in another car)
     * @param registration_a The first part of the registration, a
     *                       single letter (a-z, A-Z)
     * @param registration_b The second part of the registration, a four
     *                       digit number.
     */
    public Car(CarSize size, char registration_a, int registration_b){
        this.size = size;
        this.fuel = size.capacity;
        this.registration = new Registration(registration_a, registration_b);
    }

    /** Get the driving licence of the current user of the car. */
    public Optional<Licence> getUser() { return Optional.ofNullable(this.user); }

    /** Get the maximum fuel capacity of the car */
    public int getCapacity() { return this.size.capacity; }

    /** Get the amount of fuel the car currently has */
    public int getFuel() { return this.fuel; }

    /** True if the fuel tank is full */
    public boolean isFull() { return this.getFuel() == this.getCapacity(); }

    /** True if the car is currently being rented */
    public boolean isRented() { return this.getUser().isPresent(); }

    /** Attempt to add fuel to the tank up to the given amount. Fails if the
     * car is not currently rented.
     * @return The amount of fuel added to the car.
     */
    public Optional<Integer> addFuel(int amount){
        if(!this.isRented()) return Optional.empty();
        int diff = Math.min(amount, this.getCapacity() - this.getFuel());
        this.fuel += diff;
        return Optional.of(diff);
    }

    /** Attempt to drive the car for the given distance, consuming the
     * appropriate amount of fuel. Fails if the car has no fuel or is not
     * currently rented.
     * @return If successful, the fuel consumed during the journey.
     */
    public Optional<Integer> drive(int distance){
        if(!this.isRented() || this.getFuel() < 1) return Optional.empty();
        int consumed = this.size.consumption.apply(distance);
        this.fuel -= consumed;
        return Optional.of(consumed);
    }
}
