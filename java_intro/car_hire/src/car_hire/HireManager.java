package car_hire;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/** Manages a collection of cars, including issuing them to clients. */
public class HireManager {
    private Set<Car> cars = new HashSet<>();

    /** Create a HireManager with 10 large and 20 small cars, each car will
     * have a random unique registration for testing. */
    public HireManager(){
        for(int i = 0; i < 10; i++) this.cars.add(new Car(CarSize.large));
        for(int i = 0; i < 20; i++) this.cars.add(new Car(CarSize.small));
    }

    /** Create a HireManager with the given set of cars */
    public HireManager(HashSet<Car> cars){
        this.cars = cars;
    }

    /** Get a collection of cars filtered by the given predicate, see
     * the implementation of HireManager.getAvailable for an example. */
    public Collection<Car> getFiltered(Predicate<Car> f){
        return this.cars.stream()
                .filter(f)
                .collect(Collectors.toList());
    }

    /** Get a collection of the available cars of the given type */
    public Collection<Car> getAvailable(CarSize size){
        return this.getFiltered(c -> (!c.getUser().isPresent() && c.size == size));
    }

    /** Count the available cars of the given type */
    public int countAvailable(CarSize type){
        return this.getAvailable(type).size();
    }

    /** Get a collection of all cars which are currently rented */
    public Collection<Car> getRented(){
        return this.getFiltered(Car::isRented);
    }

    /** Attempt to get the car currently hired by the holder of the given
     *  licence. Returns Optional.empty() if they are not currently hiring
     *  a car. */
    public Optional<Car> getCar(Licence user){
        for(Car c : this.cars){
            if(c.user == user) return Optional.of(c);
        }
        return Optional.empty();
    }

    /** Attempt to issue a car of the given type to the holder of the licence.
     * Fails if the the licence holder is not eligible to hire a car of the
     * given type, or there are no cars with a full fuel tank available. */
    public IssueResult issueCar(Licence licence, CarSize type){
        if(!type.canHire.test(licence)) return IssueResult.NOT_ELIGIBLE;
        if(this.getCar(licence).isPresent()) return IssueResult.ALREADY_RENTING;

        Collection<Car> available = this.getAvailable(type);
        if(available.isEmpty()) return IssueResult.NONE_AVAILABLE;

        for(Car c : available){
            if(c.isFull()){
                licence.car = c;
                c.user = licence;
                return IssueResult.SUCCESS;
            }
        }

        return IssueResult.NONE_AVAILABLE;
    }

    /** Terminate the rental of the holder of the given licence. If no cars
     *  are currently rented by them, there will be no effect. If the car's
     *  fuel tank is not full, the fuel needed to fill it will be added.
     *  @return The amount of fuel required to fill the tank.
     */
    public int terminateRental(Licence user){
        Optional<Car> _car = this.getCar(user);
        if(!_car.isPresent()) return 0;
        Car car = _car.get();
        car.user = null;
        user.car = null;
        int toFill = car.getCapacity() - car.getFuel();
        // The specification doesn't say that the needed fuel should be added,
        // but not adding it would prevent the car from being hired again.
        car.addFuel(toFill);
        return toFill;
    }

    /** The result of an attempt to issue a car */
    public enum IssueResult{
        SUCCESS, ALREADY_RENTING, NOT_ELIGIBLE, NONE_AVAILABLE
    }
}
