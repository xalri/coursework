package car_hire;

import org.junit.Test;

import static org.junit.Assert.*;

public class CarTest {

    private static final Licence licence = Licence.makeLicence("John Smith",
            DateUtils.date(1987,4,21), DateUtils.date(2009,6,1),true, 42);

    @Test public void isFull() throws Exception {
        Car car = new Car(CarSize.large);
        assertTrue(car.isFull());
        car.user = CarTest.licence;
        car.drive(1);
        assertFalse(car.isFull());
    }

    @Test public void isRented() throws Exception {
        Car car = new Car(CarSize.large);
        assertFalse(car.isRented());
        car.user = CarTest.licence;
        assertTrue(car.isRented());
    }

    @Test public void addFuel() throws Exception {
        Car car = new Car(CarSize.small);
        car.user = CarTest.licence;
        int fuel = car.drive(1125).get();
        car.addFuel(fuel + 10);
        assertTrue(car.getCapacity() == car.getFuel());
    }

    @Test public void drive() throws Exception {
        Car car = new Car(CarSize.large);
        assertFalse(car.drive(1).isPresent());
        car.user = CarTest.licence;
        assertTrue(car.drive(1).isPresent());

        car.drive(10000);
        assertFalse(car.drive(1).isPresent());
    }

}