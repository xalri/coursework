package car_hire;

import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.*;

public class HireManagerTest {

    private static final Licence licence = Licence.makeLicence("John Smith",
            DateUtils.date(1987,4,21), DateUtils.date(2009,6,1),true, 42);

    @Test public void countAvailable() throws Exception {
        HireManager mgr = new HireManager();
        assertEquals(10, mgr.countAvailable(CarSize.large));
        assertEquals(20, mgr.countAvailable(CarSize.small));

        mgr.issueCar(licence, CarSize.large);
        assertEquals(9, mgr.countAvailable(CarSize.large));
    }

    @Test public void getRented() throws Exception {
        HireManager mgr = new HireManager();
        mgr.issueCar(licence, CarSize.large);
        Collection<Car> rented = mgr.getRented();
        assertEquals(rented.size(), 1);
        assertEquals(licence, rented.iterator().next().getUser().get());
    }

    @Test public void getCar() throws Exception {
        HireManager mgr = new HireManager();
        mgr.issueCar(licence, CarSize.large);
        assertEquals(licence, mgr.getCar(licence).get().getUser().get());
    }

    @Test public void issueCar() throws Exception {
        HireManager mgr = new HireManager();
        HireManager.IssueResult result = mgr.issueCar(licence, CarSize.large);
        assertEquals(HireManager.IssueResult.SUCCESS, result);
    }

    @Test public void terminateRental() throws Exception {
        HireManager mgr = new HireManager();
        mgr.issueCar(licence, CarSize.small);
        mgr.getCar(licence).get().drive(30);
        assertTrue(mgr.terminateRental(licence) > 0);
    }

}