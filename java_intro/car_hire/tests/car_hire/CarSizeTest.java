package car_hire;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.junit.Assert.*;

public class CarSizeTest {

    @Test public void consumptionLarge(){
        Function<Integer,Integer> fn = CarSize.large.consumption;
        assertTrue(fn.apply(0) == 0);
        assertTrue(fn.apply(1) == 1);
        assertTrue(fn.apply(15) == 1);
        assertTrue(fn.apply(16) == 2);

        assertTrue(fn.apply(50) == 4);
        assertTrue(fn.apply(63) == 4);
        assertTrue(fn.apply(64) == 5);
    }

    @Test public void consumptionSmall(){
        Function<Integer,Integer> fn = CarSize.small.consumption;
        assertTrue(fn.apply(0) == 0);
        assertTrue(fn.apply(1) == 1);
        assertTrue(fn.apply(25) == 1);
        assertTrue(fn.apply(26) == 2);
    }

    @Test public void eligibleLarge() {
        eligible(CarSize.large.canHire, 5);
    }

    @Test public void eligibleSmall() {
        eligible(CarSize.small.canHire, 1);
    }

    private void eligible(Predicate<Licence> fn, int years) {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.MONTH, -(12*years)+1); Date issuedFail = cal.getTime();
        cal.add(Calendar.MONTH, -2); Date issuedPass = cal.getTime();
        cal.add(Calendar.YEAR, -20); Date dobPass = cal.getTime();
        cal.add(Calendar.MONTH, 2); Date dobFail = cal.getTime();

        Licence licence = Licence.makeLicence("John Smith", dobPass, issuedPass, true, 0);
        assertTrue(fn.test(licence));
        licence = Licence.makeLicence("John Smith", dobPass, issuedPass, false, 1);
        assertFalse(fn.test(licence));
        licence = Licence.makeLicence("John Smith", dobPass, issuedFail, true, 2);
        assertFalse(fn.test(licence));
        licence = Licence.makeLicence("John Smith", dobFail, issuedPass, true, 3);
        assertFalse(fn.test(licence));
    }
}