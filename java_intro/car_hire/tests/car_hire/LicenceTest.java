package car_hire;

import org.junit.Test;
import static org.junit.Assert.*;

public class LicenceTest {
    @Test public void name() throws Exception {
        Licence l = Licence.makeLicence("John Smith", DateUtils.date(1987,4,21),
                DateUtils.date(2009,6,1),true, 42);
        assertEquals(l.getName(), "John Smith");
    }

    @Test public void string() throws Exception {
        Licence l = Licence.makeLicence("john smith", DateUtils.date(1987,4,21),
                DateUtils.date(2009,6,1),true, 32);
        assertEquals(l.toString(), "JS-2009-32");
    }

    @Test public void uniqueness2() throws Exception {
        Licence l1 = Licence.makeLicence("jane smith", DateUtils.date(1986, 8, 3),
                DateUtils.date(2009, 1, 15), true, 4);
        Licence l2 = Licence.makeLicence("jane smith", DateUtils.date(1986, 8, 3),
                DateUtils.date(2009, 1, 15), true, 4);
        assertTrue(l1 == l2);
    }
}