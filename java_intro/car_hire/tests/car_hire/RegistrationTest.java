package car_hire;

import org.junit.Test;
import java.util.function.Supplier;

import static org.junit.Assert.*;

public class RegistrationTest {
    @Test public void zeroPadding() throws Exception {
        Registration r = new Registration('V',0);
        assertEquals(r.toString(), "v0000");
    }

    @Test public void validation(){
        assertTrue(isThrown(() -> new Registration('@',0)));
        assertTrue(isThrown(() -> new Registration('a',-1)));
        assertTrue(isThrown(() -> new Registration('a',10000)));

        assertFalse(isThrown(() -> new Registration('a',0)));
        assertFalse(isThrown(() -> new Registration('Z',0)));
        assertFalse(isThrown(() -> new Registration('Z',9999)));
    }

    public boolean isThrown(Supplier<?> f){
        boolean thrown = false;
        try{ f.get(); }catch(Exception e){thrown = true;}
        return thrown;
    }

}