import java.util.Collection;
import java.util.Arrays;

/**
 * Contains a method for outputting the content of a Hotel in
 * a human readable format.
 */
public class HotelReport {
    Hotel h;
    public HotelReport(Hotel h){ this.h = h; }

    /**
     * Prints the content of a Hotel object in a human readable format.
     */
    public void report(){
        System.out.println(h.getName());
        System.out.println();
        printRooms(h.getRooms());
        System.out.println();
        System.out.println("Max Occupancy: "+h.getMaxOccupancy());
        System.out.println("Vacancies: "+h.getVacancies());
    }

    /**
     * Prints a collection of rooms as a table
     * @param rooms The table to print.
     */
    private void printRooms(Collection<Room> rooms){
        String[][] table = new String[rooms.size() + 1][3];
        table[0] = new String[] {"Room No.","Vacant?","Beds"};
        int i = 1;
        for(Room r : rooms){
            table[i] = new String[] {
                    String.valueOf(r.getRoomNo()),
                    r.isVacant()? "Yes" : "No",
                    getBedSizes(r.getBeds())
            };
            i++;
        }
        printTable(table);
    }

    /**
     * Constructs a human-readable string showing the number of beds and the
     * corresponding type for a collection of beds.
     * @param beds The beds to return the string for
     * @return A string in the format 1 x BedType1 bed, 2 x BedType2 beds, ...
     */
    private String getBedSizes(Collection<Bed> beds){
        String s = "";
        int[] counter = new int[Bed.Type.values().length];
        Arrays.fill(counter,0);

        // Count the number of beds of each type
        for(Bed b : beds)
            counter[b.type.ordinal()]++;

        // For each bed type, if there is at least of bed of that type, add it to the string.
        for(int i = 0; i < counter.length; i++)
            if(counter[i] != 0)
                s += counter[i] + " x " + Bed.Type.values()[i].name+
                        (counter[i] > 1 ? " beds, " : " bed, ");

        return s;
    }

    /**
     * Prints a formatted table with separators.
     * (re-used from CSC1021 Project 1)
     * @param table A 2D array of strings representing the table.
     *              Each inner array must be the same length.
     */
    private void printTable(String[][] table){
        int columns = table[0].length;

        // Calculate the maximum width of each column
        int[] lengths = new int[columns];
        for(String[] r : table)
            for(int i = 0; i < columns; i++)
                if(r[i].length() > lengths[i]) lengths[i] = r[i].length();

        // Find the total width of the table and construct a separator
        int totalLength = 0;
        for(int i : lengths) totalLength += i + 3;
        String seperator = new String(new char[totalLength]).replace('\0','-');

        // Iterate through the cells, constructing a format string for each
        // cell then outputting with System.out.printf.
        for(int i = 0; i < table.length; i++){
            for(int j = 0; j < columns; j++){
                String format = "%1$-" + lengths[j] + "s";
                format += (j+1 == columns ? "" : " | ");
                System.out.printf(format,table[i][j]);
            }
            System.out.print("\n");
            // Add a separator after the first row
            if(i == 0) System.out.println(seperator);
        }
    }
}
