import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

/**
 * Represents some basic information about a hotel: the hotel's name, and it's current rooms.
 * contains methods to calculate the maximum occupancy of the hotel, check if there are any
 * vacancies, and if there are calculate how many.
 */
public class Hotel {
    private Map<Integer, Room> rooms = new HashMap<>();
    private String name;

    /**
     * The main Hotel constructor, creates a hotel with the given name and rooms.
     * @param n The initial name of the hotel.
     * @param r The rooms to be added to the hotel.
     */
    public Hotel(String n, Room... r){
        this.name = n;
        for(Room i : r) addRoom(i);
    }

    /**
     * Adds a new room to the hotel, checking that there is no existing room with
     * the same room number.
     * @param r The room to be added.
     */
    public void addRoom(Room r){
        if(r == null) throw new IllegalArgumentException("Room can't be null.");
        if(rooms.get(r.getRoomNo()) != null)
            throw new IllegalArgumentException("A room with room number "+
                    r.getRoomNo()+ " already exists.");
        rooms.put(r.getRoomNo(),r);
    }

    /**
     * Calculates the maximum occupancy of the hotel by summing
     * the max occupancy of the rooms.
     */
    public int getMaxOccupancy(){
        int i = 0;
        for(Room r : rooms.values()) i += r.getMaxOccupancy();
        return i;
    }

    /**
     * Checks if the hotel has any rooms which aren't currently occupied
     * @return true if there are any empty rooms, false otherwise.
     */
    public boolean hasVacancies(){
        for(Room r : rooms.values())
            if(r.isVacant()) return true;
        return false;
    }

    /**
     * Calculates the number of empty rooms in the hotel.
     * @return The number of vacant rooms.
     */
    public int getVacancies(){
        int i = 0;
        for(Room r : rooms.values())
            if(r.isVacant()) i++;
        return i;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public Collection<Room> getRooms(){
        return this.rooms.values();
    }
}
