import java.util.Scanner;

/**
 * A simple command line interface for creating a Hotel.
 * Takes user input, then outputs the Hotel using HotelReport
 */
public class HotelConfigure {

    public static void main(String[] args){
        Hotel h = constructHotel(new Scanner(System.in));
        System.out.println();
        new HotelReport(h).report();
    }

    /**
     * Creates a hotel from user input.
     * @param s The scanner from which to take input.
     * @return The constructed hotel
     */
    public static Hotel constructHotel(Scanner s){
        System.out.print("Enter the name of the hotel: ");
        Hotel h = new Hotel(s.nextLine());

        System.out.print("Enter the number of rooms in the hotel: ");
        int n = s.nextInt();
        while(n > 0){
            addRoom(h, s);
            n--;
        }
        return h;
    }

    private static void addRoom(Hotel h, Scanner s){
        System.out.print("Enter the room number (must be unique): ");
        int roomNo = s.nextInt();
        Room r = new Room(roomNo);
        System.out.print("Is the room occupied? [y/N]: ");
        String tmp = s.next();
        if(tmp.toLowerCase().contains("y")) r.setVacant(false);
        System.out.print("Enter the number of beds in room "+roomNo+": ");
        int n = s.nextInt();
        printBedTypes();
        while(n > 0){
            System.out.print("Enter the bed type number: ");
            r.addBed(new Bed(Bed.Type.values()[s.nextInt()]));
            n--;
        }
        h.addRoom(r);
    }

    private static void printBedTypes(){
        System.out.println("Bed types:");
        for(int i = 0; i < Bed.Type.values().length; i++){
            System.out.println(i + ": " + Bed.Type.values()[i].name);
        }
    }
}
