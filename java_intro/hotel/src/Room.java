import java.util.Collection;
import java.util.List;
import java.util.ArrayList;

/**
 * Represents a hotel room.
 * Tracks the beds the room contains, and whether or not it's occupied.
 * Contains a method to calculate the maximum occupancy.
 */
public class Room{
    private final int roomNo;
    private boolean vacant = true;
    private List<Bed> beds = new ArrayList<>();

    /**
     * Create the room given it's number and 0 or more beds.
     * @param no The room number.
     * @param bed The beds to be added to the room.
     */
    public Room(int no, Bed.Type... bed){
        roomNo = no;
        for(Bed.Type b : bed) addBed(new Bed(b));
    }

    /**
     * Creates a room given it's room number, it's vacency status, and optionally
     * beds to be added to the room.
     * @param no The room number.
     * @param vacant True if the room is vacant, false if it's occupied.
     * @param bed The beds to be added to the room.
     */
    public Room(int no, boolean vacant, Bed.Type... bed){
        this(no, bed);
        this.vacant = vacant;
    }

    /**
     * Adds a bed to the room, verifying it's not null.
     * @param b The bed to be added.
     */
    public void addBed(Bed b){
        if(b == null) throw new IllegalArgumentException("Bed can't be null.");
        beds.add(b);
    }

    /**
     * Calculate the maxmum number of people who can stay in this hotel room
     * @return The room's maximum occupancy.
     */
    public int getMaxOccupancy(){
        int i = 0;
        for(Bed b : beds) i += b.isDouble() ? 2 : 1;
        return i;
    }

    public Collection<Bed> getBeds(){
        return beds;
    }
    public int getRoomNo() {
        return roomNo;
    }
    public boolean isVacant() {
        return vacant;
    }
    public void setVacant(boolean vacant) {
        this.vacant = vacant;
    }
}
