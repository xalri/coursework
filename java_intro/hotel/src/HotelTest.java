public class HotelTest {

    /**
     * A simple test for the Hotel class.
     * @param args command line arguments
     */
    public static void main(String[] args){
        Hotel h = new Hotel("My Amazing Hotel",
                new Room(1, false, Bed.Type.DOUBLE, Bed.Type.SINGLE),
                new Room(2, true, Bed.Type.KING, Bed.Type.SMALL_DOUBLE),
                new Room(4, false, Bed.Type.DOUBLE, Bed.Type.DOUBLE),
                new Room(7, false, Bed.Type.SINGLE, Bed.Type.SINGLE),
                new Room(5, true, Bed.Type.QUEEN),
                new Room(6, false, Bed.Type.SINGLE),
                new Room(10, true, Bed.Type.SMALL_DOUBLE)
        );

        new HotelReport(h).report();
        System.out.println();

        if(h.getMaxOccupancy() != 18){
            System.out.println("Test 1 failed: Max occupancy ("+h.getMaxOccupancy()+
                    ") should be " + 18);
            System.exit(1);
        }
        if(h.getVacancies() != 3){
            System.out.println("Test 2 failed: Vacancies ("+h.getVacancies()+
                    ") should be " + 3);
            System.exit(1);
        }
    }
}
