/**
 * Represents a bed, stores the size of the bed in the format
 * most common in the UK.
 */
public class Bed {
    public enum Type {
        //Single beds:
        SINGLE("Standard Single"),
        SMALL_SINGLE("Small Single"),
        //Double beds:
        SMALL_DOUBLE("Small Double"),
        DOUBLE("Standard Double"),
        KING("King"),
        QUEEN("Queen"),
        UNKNOWN("Unknown");

        final String name;
        Type(String name){
            this.name = name;
        }
    }

    public final Type type;

    public Bed(Type type){
        this.type = type;
    }

    public Bed copyBed(){
        return new Bed(type);
    }

    public Boolean isDouble(){
        return type.ordinal() >= Type.SMALL_DOUBLE.ordinal();
    }

    public String toString(){
        return type.name;
    }
}

