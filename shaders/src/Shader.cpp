#include "Shader.h"
#include "Mesh.h"

Shader::Shader(string vFile, string fFile, string gFile, string tcsFile, string tesFile)	{
	linkSuccess = true;
	loadSuccess = true;

	program	= glCreateProgram();
	objects[SHADER_VERTEX] = GenerateShader(vFile, GL_VERTEX_SHADER);
	objects[SHADER_FRAGMENT] = GenerateShader(fFile, GL_FRAGMENT_SHADER);
    objects[SHADER_GEOMETRY] = GenerateShader(gFile, GL_GEOMETRY_SHADER);
    objects[SHADER_TCS]	= GenerateShader(tcsFile, GL_TESS_CONTROL_SHADER);
    objects[SHADER_TES]	= GenerateShader(tesFile, GL_TESS_EVALUATION_SHADER);

	for (unsigned int object : objects) {
		if (object) glAttachShader(program, object);
	}

    Vertex::BindAtribs(program);
	linkSuccess = LinkProgram();
}

Shader::~Shader()	{
	if (program) {
		for (unsigned int object : objects) {
			if (object) {
				glDetachShader(program, object);
				glDeleteShader(object);
			}
		}
		glDeleteProgram(program);
	}
}

/*
Basic text file loading. Should be pretty efficient as the text
buffer will only be made once, and never resized.
*/
bool Shader::LoadShaderFile(string from, string &into)	{
	ifstream	file;

	file.open(from);
	if(!file){
		cout << "File does not exist!" << endl;
		return false;
	}

	file.seekg(0, std::ios::end);
	into.resize(1 + (unsigned int)file.tellg());
	file.seekg(0, std::ios::beg);
	file.read(&into[0], into.size());

	into[into.size() - 1] = '\n';

	file.close();
	return true;
}

/*
The shader files you create are just text - they must be compiled into
machine code compatible with your graphics card, just as with your C++ code.

All shader objects are of a specific 'type' (vertex/fragment/geometry etc),
so when you create a shader object using glCreateShader, you must set this.

One you have loaded a shader file, you can tell OpenGL about it using the
glShaderSource function - you can send multiple text files at once using this
function, so you could create your own #include system by parsing your text
files. Once you've sent the text to OpenGL, you can tell it to turn it into
a shader object using glCompileShader. If your driver fails to compile the
code, you can query information on why its failed using the 'info log', which
will return a string telling you about what warnings and errors it found.
*/
GLuint	Shader::GenerateShader(string from, GLenum type)	{
    if(from.empty()) return 0;

	cout << "Compiling Shader " << from << endl;

	string load;
	if(!LoadShaderFile(from,load)) {
		cout << "Compiling failed! File not found!" << endl;
		loadSuccess = false;
		return 0;
	}

	GLuint shader = glCreateShader(type);

	const char *chars = load.c_str();
	glShaderSource(shader, 1, &chars, NULL);
	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)	{
		cout << "Compiling failed! Error log as follows:" << endl;
		char error[2048];
		glGetInfoLogARB(shader, sizeof(error), NULL, error);
		cout << error << endl;
		loadSuccess = false;
		return false;
	}
	cout << "Compiling success!" << endl;
	return shader;
}

/*
In order to create a finished working shader program, you must 'link' all of
your shader objects together - this makes sure the outputs of one stage connect
to the inputs of the next stage. In case this linking stage fails, it is possible
to get an 'info log' from your OpenGL driver, which should (hopefully) inform you
of why.
*/
bool Shader::LinkProgram()	{
	if (!loadSuccess) {
		cout << "Can't link program, one or more shader objects have failed!" << endl;
		return false;
	}
	glLinkProgram(program);

	GLint status;
	glGetProgramiv(program, GL_LINK_STATUS, &status);

	if (status == GL_FALSE)	{
		cout << "Linking failed! Error log as follows:" << endl;
		char error[2048];
		glGetProgramInfoLog(program, sizeof(error), NULL, error);
		cout << error << endl;
		return false;
	}
	return true;
}
