#include "Demo.h"
#include "SFML/Graphics/Image.hpp"
#include <random>

const float Z_NEAR = 1.0;
const float Z_FAR = 1000.0;
const float FOV = 60.0;

Demo::Demo(sf::Window &parent):
    OGLRenderer(parent),
    buffer(parent.getSize().x, parent.getSize().y),
    light_shader("shaders/light_vert.glsl", "shaders/light_frag.glsl", "shaders/light_geom.glsl")
{
    //glEnable(GL_DEBUG_OUTPUT);
    //glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    //glDebugMessageCallback(Debug, nullptr);

    OnResize(window.getSize().x, window.getSize().y);
    viewMatrix = Matrix4::BuildViewMatrix(Vector3(0, 0, 0), Vector3(0, 0, -1));

    shaders = std::vector<shared_ptr<Shader>>{
        std::make_shared<Shader>("shaders/fade/vert.glsl", "shaders/fade/frag.glsl"),
        std::make_shared<Shader>("shaders/shrink/vert.glsl", "shaders/shrink/frag.glsl"),
        std::make_shared<Shader>("shaders/multiply/vert.glsl", "shaders/multiply/frag.glsl"),
        std::make_shared<Shader>("shaders/split/vert.glsl", "shaders/split/frag.glsl", "shaders/split/geom.glsl"),
        std::make_shared<Shader>("shaders/disolve/vert.glsl", "shaders/disolve/frag.glsl"),
    };
    cycle_ms = std::vector<double>{ 3000.0, 3000.0, 8000.0, 5000.0, 6000.0 };
    current_shader = 0;

    auto cube = std::make_shared<RenderObject>(
        Mesh::FromFile("media/cube.asciimesh"),
        shaders[0]);

    cube->modelMatrix = Matrix4::Translation(Vector3(0, 0, -4));
    objects.push_back(cube);

    noise = LoadTexture("media/perlin.png");
    tx_diffuse = LoadTexture("media/crate.png");
    tx_norm = LoadTexture("media/crate_norm.png");

    glGenVertexArrays(1, &light_vao);
}

void Demo::NextShader() {
    current_shader += 1;
    if (current_shader == shaders.size()) current_shader = 0;
    objects[0]->shader = shaders[current_shader];
    time_ms = 0;
}

void Demo::RenderScene() {
    // Geometry pass
    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    buffer.BindWrite();
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, noise);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tx_diffuse);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, tx_norm);

    float time = time_ms / cycle_ms[current_shader];
    if (time > 1.0) { time = 1 - (time - 1); }

    for (auto &o : objects) {
        auto uniforms = Uniforms {
            time, 0, 1, 2, viewMatrix, projMatrix
        };
        o->Draw(uniforms);
    }

    // Lighting pass
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    window.setActive(true);
    auto program = light_shader.GetShaderProgram();
    glUseProgram(program);
    buffer.BindRead(program);
    if (current_shader != 2) time = 0;
    glUniform1f(glGetUniformLocation(program, "time"), time);
    glBindVertexArray(light_vao);
    glDrawArrays(GL_POINTS, 0, 1);
    glBindVertexArray(0);
}

void Demo::Update(float msec) {
    auto first = time_ms < cycle_ms[current_shader];
    time_ms += msec;
    auto& o = objects[0];

    if (first && time_ms > cycle_ms[current_shader]) {
        NextShader();
        time_ms = (float) cycle_ms[current_shader];
    };

    if (time_ms > cycle_ms[current_shader] * 2) NextShader();

    //o->modelMatrix = o->modelMatrix * Matrix4::Rotation(0.02f * msec, Vector3(0, 0.5, 1));
    o->modelMatrix = o->modelMatrix * Matrix4::Rotation(0.02f * msec, Vector3(0.5, 0.5, 0));

    for (auto &renderObject : objects) {
        renderObject->Update(msec);
    }
}

void Demo::OnResize(unsigned w, unsigned h) {
    // Update projection matrix & resize viewport
    auto aspect = (float) w / (float) h;
    projMatrix = Matrix4::Perspective(Z_NEAR, Z_FAR, aspect, FOV);
    glViewport(0, 0, w, h);
    buffer = GBuffer(w, h);
}

GLuint LoadTexture(const string &filename) {
    // Load the image
    sf::Image img_data;
    if (!img_data.loadFromFile(filename)) {
        cout << "Texture file " << filename << " failed to load!" << std::endl;
        return 0;
    }

    return GLTexture(GL_RGBA, img_data.getSize().x, img_data.getSize().y, img_data.getPixelsPtr());
}

GLuint GLTexture(GLenum format, unsigned w, unsigned h, const void* data) {
    GLuint texName;
    glGenTextures(1, &texName);
    glBindTexture(GL_TEXTURE_2D, texName);
    cout << "Uploading texture " << texName << endl;
    glTexImage2D(GL_TEXTURE_2D, 0, format, w, h, 0, format, GL_UNSIGNED_BYTE, data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    return texName;
}

void Debug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
           const GLchar *message, const void *userParam) {
    std::cout << message << std::endl;
}
