#pragma once

#include "OGLRenderer.h"
#include "RenderObject.h"
#include "GBuffer.h"

#include <vector>
#include <memory>

using std::vector;
using std::shared_ptr;

class Demo : public OGLRenderer {
public:
    vector<shared_ptr<RenderObject>> objects;
    GLuint noise;
    GLuint tx_diffuse;
    GLuint tx_norm;
    GBuffer buffer;
    Shader light_shader;
    GLuint light_vao;
    float time_ms;

    std::vector<shared_ptr<Shader>> shaders;
    std::vector<double> cycle_ms;
    unsigned current_shader;

    explicit Demo(sf::Window &parent);

    void RenderScene() override;

    void Update(float msec);

    void OnResize(unsigned w, unsigned h);

    void NextShader();
};


GLuint LoadTexture(const string &filename);

GLuint GLTexture(GLenum format, unsigned w, unsigned h, const void* data);

GLuint RGBNoise(unsigned w, unsigned h);

void Debug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
           const GLchar *message, const void *userParam);
