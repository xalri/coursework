#include "Demo.h"
#include <chrono>
#include <memory>

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::microseconds us;
using std::chrono::duration_cast;

int main() {
    auto settings = sf::ContextSettings();
    settings.depthBits = 24;
    settings.stencilBits = 8;
    settings.antialiasingLevel = 4;
    settings.majorVersion = 4;
    settings.minorVersion = 3;
    settings.attributeFlags = sf::ContextSettings::Core | sf::ContextSettings::Debug;

    sf::Window w(
            sf::VideoMode(800, 600),
            "Shader Demo",
            sf::Style::Default,
            settings);
    w.setVerticalSyncEnabled(true);

    Demo demo(w);

    bool running = true;
    auto last_tick = Clock::now();
    while (running) {
        auto ev = sf::Event();
        while(w.pollEvent(ev)) {
            if (ev.type == sf::Event::Closed) running = false;
            if (ev.type == sf::Event::Resized) demo.OnResize(ev.size.width, ev.size.height);
            if (ev.type == sf::Event::KeyPressed) {
                if (ev.key.code == sf::Keyboard::Space) demo.NextShader();
            }
        }

        auto elapsed = Clock::now() - last_tick;
        auto ms = (float) duration_cast<us>(elapsed).count() / 1000.0f ;
        last_tick = Clock::now();

        demo.Update(ms);
        demo.ClearBuffers();
        demo.RenderScene();
        demo.SwapBuffers();
    }
}
