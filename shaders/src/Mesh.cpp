#include "Mesh.h"

Mesh::Mesh() {
    glGenVertexArrays(1, &arrayObject);
}

Mesh::~Mesh() {
    glDeleteVertexArrays(1, &arrayObject);
    glDeleteBuffers(1, &vertexBuffer);
}

void Mesh::Draw() {
    glBindVertexArray(arrayObject);
    glDrawArrays(type, 0, (GLsizei)(vertices.size()));
    glBindVertexArray(0);
}

void Mesh::BufferData() {
    glBindVertexArray(arrayObject);

    glGenBuffers(1, &vertexBuffer);
    std::cout << "Loading vertex data to VBO " << vertexBuffer
              << " ," << vertices.size() * sizeof(Vertex) << std::endl;
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);
    Vertex::SetAttribPointers();

    glBindVertexArray(0);
}

void Vertex::SetAttribPointers() {
    auto stride = sizeof(Vertex);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (void*)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (void*)(3 * sizeof(GLfloat)));
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, stride, (void*)(5 * sizeof(GLfloat)));
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, stride, (void*)(9 * sizeof(GLfloat)));
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, stride, (void*)(12 * sizeof(GLfloat)));
    glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, stride, (void*)(15 * sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);
    glEnableVertexAttribArray(4);
    glEnableVertexAttribArray(5);
}

void Vertex::BindAtribs(GLuint program)	{
    glBindAttribLocation(program, 0, "position");
    glBindAttribLocation(program, 1, "uv");
    glBindAttribLocation(program, 2, "colour");
    glBindAttribLocation(program, 3, "normal");
    glBindAttribLocation(program, 4, "tangent");
    glBindAttribLocation(program, 5, "bitangent");
}

std::shared_ptr<Mesh> Mesh::FromFile(const string &filename) {
    ifstream f(filename);

    if (!f) {
        std::cout << "Mesh file not found: " << filename << std::endl;
        return nullptr;
    }

    auto m = std::make_shared<Mesh>();
    unsigned int n = 0;
    f >> n;

    int hasTex = 0;
    int hasColour = 0;

    f >> hasTex;
    f >> hasColour;

    m->vertices = std::vector<Vertex>(n);

    for (unsigned int i = 0; i < n; ++i) {
        f >> m->vertices[i].position.x;
        f >> m->vertices[i].position.y;
        f >> m->vertices[i].position.z;
    }

    if (hasColour) {
        for (unsigned int i = 0; i < n; ++i) {
            unsigned char r, g, b, a;

            f >> r;
            f >> g;
            f >> b;
            f >> a;
            //OpenGL can use floats for colours directly - this will take up 4x as
            //much space, but could avoid any byte / float conversions happening
            //behind the scenes in our shader executions
            m->vertices[i].colour = Vector4(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
        }
    }

    if (hasTex) {
        for (unsigned int i = 0; i < n; ++i) {
            f >> m->vertices[i].uv.x;
            f >> m->vertices[i].uv.y;
        }
    }

    m->GenerateNormals();
    m->BufferData();
    return m;
}

void Mesh::GenerateNormals() {
    for(auto i = 0; i < vertices.size(); i += 3) {
        auto& v0 = vertices[i];
        auto& v1 = vertices[i+1];
        auto& v2 = vertices[i+2];

        auto d1 = v1.position-v0.position;
        auto d2 = v2.position-v0.position;

        auto dt1 = v1.uv-v0.uv;
        auto dt2 = v2.uv-v0.uv;

        Vector3 normal = Vector3::Cross(d1, d2);
        normal.Normalise();

        // Calculate tangent & bitangent for normal mapping
        // www.terathon.com/code/tangent.html
        auto r = 1.0f / (dt1.x * dt2.y - dt1.y * dt2.x);
        Vector3 tangent = (d1 * dt2.y - d2 * dt1.y) * r;
        Vector3 bitangent = (d2 * dt1.x - d1 * dt2.x) * r;

        v0.normal = normal; v1.normal = normal; v2.normal = normal;
        v0.tangent = tangent; v1.tangent = tangent; v2.tangent = tangent;
        v0.bitangent = bitangent; v1.bitangent = bitangent; v2.bitangent = bitangent;
    }
}
