#include "GBuffer.h"

GBuffer::GBuffer(unsigned w, unsigned h) {
   glGenFramebuffers(1, &fbo);
   glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);

   CreateTx(tx_diffuse, 0, w, h);
   CreateTx(tx_normal, 1, w, h);
   CreateTx(tx_pos, 2, w, h);
   CreateTx(tx_uv, 3, w, h);

   glGenTextures(1, &tx_depth);
   glBindTexture(GL_TEXTURE_2D, tx_depth);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, w, h, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
   glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tx_depth, 0);

   GLenum DrawBuffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
   glDrawBuffers(4, DrawBuffers);

   glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

GBuffer::~GBuffer() {
   // TODO
}

void GBuffer::CreateTx(GLuint &tx, unsigned i, unsigned w, unsigned h) {
   glGenTextures(1, &tx);
   glBindTexture(GL_TEXTURE_2D, tx);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_FLOAT, nullptr);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
   glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, tx, 0);
}

void GBuffer::BindRead(GLuint program) {
   glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

   glActiveTexture(GL_TEXTURE0);
   glBindTexture(GL_TEXTURE_2D, tx_diffuse);
   glUniform1i(glGetUniformLocation(program, "tx_diffuse"), 0);
   glActiveTexture(GL_TEXTURE1);
   glBindTexture(GL_TEXTURE_2D, tx_normal);
   glUniform1i(glGetUniformLocation(program, "tx_normal"), 1);
   glActiveTexture(GL_TEXTURE2);
   glBindTexture(GL_TEXTURE_2D, tx_pos);
   glUniform1i(glGetUniformLocation(program, "tx_pos"), 2);
   glActiveTexture(GL_TEXTURE3);
   glBindTexture(GL_TEXTURE_2D, tx_uv);
   glUniform1i(glGetUniformLocation(program, "tx_uv"), 3);
}

void GBuffer::BindWrite() {
   glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);
}
