#pragma once

#include "GL/glew.h"

class GBuffer {
public:
    GBuffer(unsigned w, unsigned h);
    ~GBuffer();

    void BindRead(GLuint program);
    void BindWrite();

private:
    GLuint fbo;
    GLuint tx_pos;
    GLuint tx_diffuse;
    GLuint tx_normal;
    GLuint tx_uv;
    GLuint tx_depth;

    void CreateTx(GLuint &tx, unsigned int i, unsigned int w, unsigned int h);
};
