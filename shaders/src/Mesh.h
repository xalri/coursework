/******************************************************************************
Class:Mesh
Implements:
Author:Rich Davison	<richard.davison4@newcastle.ac.uk>
Description:Wrapper around OpenGL primitives, geometry and related 
OGL functions.

There's a couple of extra functions in here that you didn't get in the tutorial
series, to draw debug normals and tangents. 


-_-_-_-_-_-_-_,------,   
_-_-_-_-_-_-_-|   /\_/\   NYANYANYAN
-_-_-_-_-_-_-~|__( ^ .^) /
_-_-_-_-_-_-_-""  ""   

*//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "OGLRenderer.h"
#include <vector>
#include <string>
#include <fstream>
#include <memory>

using std::ifstream;
using std::string;

struct Vertex {
    Vector3 position;
    Vector2 uv;
    Vector4 colour;
    Vector3 normal;
    Vector3 tangent;
    Vector3 bitangent;

    static void SetAttribPointers();
    static void BindAtribs(GLuint program);
};

class Mesh {
public:
    GLuint type = GL_TRIANGLES;

    Mesh();
    ~Mesh();
    virtual void Draw();
    void GenerateNormals();

    static std::shared_ptr<Mesh> FromFile(const string &filename);

protected:
    GLuint arrayObject;
    GLuint vertexBuffer;
    std::vector<Vertex> vertices;

    //Buffers all VBO data into graphics memory. Required before drawing!
    void BufferData();
};

