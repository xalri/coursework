#include "RenderObject.h"
#include <utility>

RenderObject::RenderObject(shared_ptr<Mesh> m, shared_ptr<Shader> s, GLuint t):
        mesh(std::move(m)), shader(std::move(s)), texture(t) {}

void RenderObject::Update(float msec) {
    worldTransform = (parent) ? parent->worldTransform * modelMatrix : modelMatrix;
    for (auto &i : children) i->Update(msec);
}

void RenderObject::Draw(Uniforms uniforms) const {
    if(shader && mesh) {
        uniforms.modelMatrix = worldTransform;
        auto program = shader->GetShaderProgram();
        glUseProgram(program);
        uniforms.Apply(program);
        mesh->Draw();
    }
    for (auto& o : children) o->Draw(uniforms);
}

void Uniforms::Apply(GLuint program) const {
    glUniform1i(glGetUniformLocation(program, "noise"), noise);
    glUniform1f(glGetUniformLocation(program, "time"), time);
    glUniform1i(glGetUniformLocation(program, "tx_diffuse"), tx_diffuse);
    glUniform1i(glGetUniformLocation(program, "tx_norm"), tx_norm);
    glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, false, (float *) &modelMatrix);
    glUniformMatrix4fv(glGetUniformLocation(program, "viewMatrix"), 1, false, (float *) &viewMatrix);
    glUniformMatrix4fv(glGetUniformLocation(program, "projMatrix"), 1, false, (float *) &projMatrix);
}
