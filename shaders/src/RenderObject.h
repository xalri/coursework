#pragma once

#include "Matrix4.h"
#include "OGLRenderer.h"
#include "Mesh.h"
#include "Shader.h"
#include <memory>

using std::shared_ptr;

struct Uniforms {
    float time;
    GLuint noise;
    GLuint tx_diffuse;
    GLuint tx_norm;
    Matrix4 viewMatrix;
    Matrix4 projMatrix;
    Matrix4 modelMatrix;

    void Apply(GLuint program) const;
};

class RenderObject {
public:
    shared_ptr<Mesh> mesh;
    shared_ptr<Shader> shader;

    GLuint texture;
    Matrix4 modelMatrix;
    Matrix4 worldTransform;

    shared_ptr<RenderObject> parent;
    vector<shared_ptr<RenderObject>> children;

    RenderObject(shared_ptr<Mesh> m, shared_ptr<Shader> s, GLuint t = 0);
    ~RenderObject() = default;

    virtual void Draw(Uniforms) const;
    virtual void Update(float msec);
};

