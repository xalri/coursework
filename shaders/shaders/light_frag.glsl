#version 330 core

uniform sampler2D tx_diffuse;
uniform sampler2D tx_normal;
uniform sampler2D tx_pos;
uniform sampler2D tx_uv;
uniform float time;

in vec2 uv;
out vec4 fragColour;

void main(void) {
   vec2 uv = uv;

   if (time != 0) {
      uv -= 0.5;
      uv = uv * (1 + 100 * pow(time, 5));
      if (mod(floor(uv.y + 0.5), 2) == 1) uv.x += 10 * pow(time, 0.8);
      uv += 0.5;
   }

	vec4 diffuse = texture(tx_diffuse, uv);
	vec4 normal = texture(tx_normal, uv);
	vec4 pos = texture(tx_pos, uv);
	vec4 tex = texture(tx_uv, uv);

   if (time != 0) {
      diffuse += vec4(abs(uv / 10), 0.0, 0.0);
      pos += vec4(abs(uv / 10), 0.0, 0.0);
   }

   const vec3 light_dir = normalize(vec3(-1, 1, 0.5));
   const vec3 light_pos = normalize(vec3(-3, 3, 0));
   const vec3 ambient = vec3(0.1, 0.1, 0.1);
   float light = max(dot(normal.xyz, light_dir), 0.0);

   vec3 camera_dir = normalize(-pos.xyz);
   vec3 half_dir = (light_dir - camera_dir) / 2;
   float specular = pow(max(dot(normal.xyz, half_dir), 0.0), 9.0);
   float atten = pow(max(1 - distance(light_pos, pos.xyz) / 15, 0), 2);

	vec3 col = ambient + atten * (vec3(diffuse) * light + specular * vec3(1.0, 1.0, 1.0));

	float alpha = diffuse[3];
	fragColour = vec4(col, alpha);
}