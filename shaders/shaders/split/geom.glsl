#version 330 core

uniform float time;

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in Vertex {
   vec2 uv;
   vec4 colour;
   vec3 normal;
	vec3 tangent;
	vec3 bitangent;
} IN[];

out Vertex {
	vec2 uv;
	vec4 colour;
	vec3 normal;
	vec3 tangent;
	vec3 bitangent;
} OUT;

// Creates a rotation matrix
// http://www.neilmendoza.com/glsl-rotation-about-an-arbitrary-axis/
mat4 rotationMatrix(vec3 axis, float angle) {
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

void main() {
   vec3 normal = IN[0].normal;
   vec4 center = gl_in[0].gl_Position;
   for(int i = 1; i < gl_in.length(); ++i) {
      center += gl_in[i].gl_Position;
   }
   center /= gl_in.length();
   mat4 rot = rotationMatrix(normal, pow(time, 2) * 2);

   for(int i = 0; i < gl_in.length(); ++i) {
      vec4 position = gl_in[i].gl_Position;
      position -= center;

      position = rot * position * (1 - pow(time, 0.7)) + center;
      gl_Position = position;

      OUT.uv = IN[i].uv;
      OUT.colour = IN[i].colour;
      OUT.normal = IN[i].normal;
      OUT.tangent = IN[i].tangent;
      OUT.bitangent = IN[i].bitangent;

      EmitVertex();
   }

   EndPrimitive();
}