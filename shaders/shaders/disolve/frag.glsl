#version 330 core

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform float time;
uniform sampler2D noise;
uniform sampler2D tx_diffuse;
uniform sampler2D tx_norm;

in Vertex	{
	vec2 uv;
	vec4 colour;
	vec3 normal;
	vec3 tangent;
	vec3 bitangent;
} IN;

out vec4 out_difuse;
out vec4 out_normal;
out vec4 out_pos;
out vec4 out_uv;

// Bump mapping
// https://stackoverflow.com/a/5284527
void bump(out vec4 bump, in vec2 uv, in sampler2D tx, in float t) {
   const ivec3 off = ivec3(-1,0,1);
   vec2 size = vec2(t, 0.0);

   float s11 = texture(tx, uv).x;
   float s01 = textureOffset(tx, uv, off.xy).x;
   float s21 = textureOffset(tx, uv, off.zy).x;
   float s10 = textureOffset(tx, uv, off.yx).x;
   float s12 = textureOffset(tx, uv, off.yz).x;
   vec3 va = normalize(vec3(size.xy,s21-s01));
   vec3 vb = normalize(vec3(size.yx,s12-s10));
   bump = vec4( cross(va,vb), s11 );
}

void blend_normals(out vec3 n, in vec3 n1, in vec3 n2) {
   //n = normalize(vec3(n1.xy*n2.z + n2.xy*n1.z, n1.z*n2.z));
   n = normalize(vec3(n1.xy + n2.xy, n1.z*n2.z));
}

void main(void)	{
	float life = clamp(1 - time * 0.7, 0, 1);
	life = pow(life, 2);

   mat3 tangentToWorld = mat3(
      IN.tangent.x, IN.bitangent.x, IN.normal.x,
      IN.tangent.y, IN.bitangent.y, IN.normal.y,
      IN.tangent.z, IN.bitangent.z, IN.normal.z);

	vec4 diffuse = texture(tx_diffuse, IN.uv);
	vec3 tx_normal = normalize(texture(tx_norm, IN.uv).xyz * 2 - 1).xyz;
	tx_normal.y = -tx_normal.y;

	vec4 normal_off; bump(normal_off, IN.uv, noise, -pow(life, 8));
	vec3 n1; blend_normals(n1, tx_normal, normal_off.xyz);
	if(life < texture(noise, IN.uv).r) diffuse.a = 0;

	//out_difuse = vec4(IN.tangent, 1.0);
	out_difuse = diffuse;
	out_normal = vec4(normalize(n1 * tangentToWorld), 1.0);
	out_pos = gl_FragCoord;
	out_uv = vec4(vec3(IN.uv, 0.0), 1.0);
}

