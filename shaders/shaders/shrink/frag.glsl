#version 330 core

uniform sampler2D tx_diffuse;
uniform sampler2D tx_norm;

in Vertex	{
	vec2 uv;
	vec4 colour;
	vec3 normal;
	vec3 tangent;
	vec3 bitangent;
} IN;

out vec4 out_difuse;
out vec4 out_normal;
out vec4 out_pos;
out vec4 out_uv;

void main(void)	{
   mat3 tangentToWorld = mat3(
      IN.tangent.x, IN.bitangent.x, IN.normal.x,
      IN.tangent.y, IN.bitangent.y, IN.normal.y,
      IN.tangent.z, IN.bitangent.z, IN.normal.z);

	vec4 diffuse = texture(tx_diffuse, IN.uv);
	vec3 tx_normal = normalize(texture(tx_norm, IN.uv).xyz * 2 - 1).xyz;
	tx_normal.y = -tx_normal.y;

	out_difuse = diffuse;
	out_normal = vec4(normalize(tx_normal * tangentToWorld), 1.0);
	out_pos = gl_FragCoord;
	out_uv = vec4(vec3(IN.uv, 0.0), 1.0);
}

