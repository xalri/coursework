#version 330 core

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform float time;

in vec3 position;
in vec2 uv;
in vec4 colour;
in vec3 normal;
in	vec3 tangent;
in vec3 bitangent;

out Vertex	{
	vec2 uv;
	vec4 colour;
	vec3 normal;
	vec3 tangent;
	vec3 bitangent;
} OUT;

void main(void)	{
   mat4 mv = viewMatrix * modelMatrix;
   mat3 normalMatrix = transpose(inverse(mat3(mv)));
   mat4 mvp = projMatrix * mv;

	vec3 pos = position;

	pos *= 1 - pow(time, 3);

	OUT.uv = uv;
	OUT.colour = colour;
	OUT.normal = normalize(normalMatrix * normal);
	OUT.tangent = normalize(normalMatrix * tangent);
	OUT.bitangent = normalize(normalMatrix * bitangent);
	gl_Position = mvp * vec4(pos, 1.0);
}