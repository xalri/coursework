---
title: "GLSL Shader Demo"
author: "Lewis Hallam"
geometry: margin=2.2cm
output: pdf_document
---

### The Shaders

The demo loops through five effects created using GLSL shaders:

 1. The cube fades out.
 2. The cube shrinks to a point.
 3. An effect in the lighting shader 'multiplies' the cube, zooming out
    to show hundreds of 'duplicates' (drawn in the lighting shader by
    multipliying the uv coords then wrapping). Some movement is added
    and colours change.
 4. A geometry shader splits the triangles making up the box apart,
    rotating them and fading them out.
 5. A combination of bump mapping and other little bits of geometry make
    the surface of the box disolve.

The shaders alternate between running forwards and in reverse for continuity;
since there are an odd number of shaders after one cycle the shaders which
ran in reverse the first time run normally.

Deferred shading is used -- in the geometry pass, various shaders are used
to draw the colour, normals, uv, etc, to texture buffers, then the lighting
shader reads from the buffers draws to the screen with blinn-phong shading.
The cube has a hand-drawn normal map, the code for calculating vertex
tangents and bitangents is based on that from <www.terathon.com/code/tangent.html>.

### Notes on compilation

As an upgrade from the last project submitted for this module, this one
can be compiled on MSVC! Though running it gives an access violation exception..
Presumably _undefined behaviour_ was invoked somewhere, though gcc doesn't
mind.

The sumbitted executable & dlls were cross-compiled from linux using mingw-w64:

    i686-w64-mingw32-cmake ..; make shader_demo

### Screenshots

![](./screenshot1.png){width=350px}

![](./screenshot2.png){width=350px}

![](./screenshot3.png){width=350px}

![](./screenshot4.png){width=350px}
