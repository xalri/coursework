/*
Class:OGLRenderer
Author:Rich Davison	<richard.davison4@newcastle.ac.uk>
Description:Abstract base class for the graphics tutorials. Creates an OpenGL 
3.2 CORE PROFILE rendering context. Each lesson will create a renderer that 
inherits from this class - so all context creation is handled automatically,
but students still get to see HOW such a context is created.

-_-_-_-_-_-_-_,------,   
_-_-_-_-_-_-_-|   /\_/\   NYANYANYAN
-_-_-_-_-_-_-~|__( ^ .^) /
_-_-_-_-_-_-_-""  ""   

*/

#include "OGLRenderer.h"

using std::string;
using std::cout;
using std::endl;

OGLRenderer::OGLRenderer(sf::Window &window) : window(window) {
    // SFML handles the platform-specific window & render context setup, so there's
    // not much we need to do here.
    // We initialise GLEW, tell opengl to draw to the window's render context, and
    // set the initial viewport based on the window dimensions.
    if (glewInit()) std::cout << "Failed to initialise GLEW" << std::endl;
    window.setActive(true);
    glClearColor(0.2f, 0.2f, 0.2f, 1.0f); //When we clear the screen, we want it to be dark grey
}

/*
Swaps the buffers, ready for the next frame's rendering. Should be called
every frame, at the end of RenderScene(), or wherever appropriate for
your application.
*/
void OGLRenderer::SwapBuffers() {
    window.display();
}

void OGLRenderer::ClearBuffers() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
