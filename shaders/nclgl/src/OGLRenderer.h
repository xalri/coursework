#pragma once
/*
Class:OGLRenderer
Author:Rich Davison	<richard.davison4@newcastle.ac.uk>
Description:Abstract base class for the graphics tutorials. Creates an OpenGL 
3.2 CORE PROFILE rendering context. Each lesson will create a renderer that 
inherits from this class - so all context creation is handled automatically,
but students still get to see HOW such a context is created.

-_-_-_-_-_-_-_,------,   
_-_-_-_-_-_-_-|   /\_/\   NYANYANYAN
-_-_-_-_-_-_-~|__( ^ .^) /
_-_-_-_-_-_-_-""  ""   

*/

#include <string>
#include <fstream>
#include <vector>

#include "SFML/Window.hpp"
#include <GL/glew.h>

#include "Vector4.h"
#include "Vector3.h"
#include "Vector2.h"
#include "Quaternion.h"
#include "Matrix4.h"

using std::vector;

class OGLRenderer {
public:
    sf::Window &window;
    Matrix4 projMatrix;     // Projection matrix
    Matrix4 modelMatrix;    // Model matrix. NOT MODELVIEW
    Matrix4 viewMatrix;     // View matrix
    Matrix4 textureMatrix;  // Texture matrix

    explicit OGLRenderer(sf::Window &parent);

    virtual void RenderScene() = 0;
    void SwapBuffers();
    void ClearBuffers();
};