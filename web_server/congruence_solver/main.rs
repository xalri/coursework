#![feature(plugin)]
#![feature(proc_macro)]
#![feature(custom_derive)]
#![plugin(rocket_codegen)]

extern crate num;
extern crate rocket;
extern crate maud;

use num::*;
use maud::html;
use maud::DOCTYPE;
use rocket::request::Form;
use rocket::response::content;
use std::str::FromStr;

#[derive(FromForm)]
struct Params { a: String, b: String, n: String, }

#[get("/")]
fn index() -> content::Html<String> {
   let form = html! {
      (DOCTYPE)
      head{
         link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css";
      }
      body{
         p {
            "Solve a linear congruence in the form ax + b = 0 (mod N):"
         }
         form method="POST" {
            div class="form-group" {
               label for="text" "a: ";
               input type="text" name="a";
            }
            div class="form-group" {
               label for="text" "b: ";
               input type="text" name="b";
            }
            div class="form-group" {
               label for="text" "n: ";
               input type="text" name="n";
            }
            div class="form-group" {
               input.btn type = "submit" value = "Solve";
            }
         }
      }
   };

   content::Html(form.into_string())
}

#[post("/", data = "<input>")]
fn calculate(input: Form<Params>) -> String {
   _calculate(input).unwrap_or("Failed to parse input".to_string())
}

fn _calculate(input: Form<Params>) -> Result<String, num::bigint::ParseBigIntError> {
   let a = BigInt::from_str(&input.get().a)?;
   let b = BigInt::from_str(&input.get().b)?;
   let n = BigInt::from_str(&input.get().n)?;
   Ok(match solve(&a, &b, &n) {
      Some(result) => result.to_str_radix(10),
      None => "No solution".to_string(),
   })
}

fn main() {
   rocket::ignite().mount("/", routes![index, calculate]).launch();
}

fn solve(a: &BigInt, b: &BigInt, n: &BigInt) -> Option<BigInt> {
   let d = &a.gcd(&n);
   if *d == 1.into() { return Some(((n - b) * modinverse(a, n).unwrap()).mod_floor(&n)); }
   let c = (-b).mod_floor(&n);
   if !c.is_multiple_of(&d) { return None; }
   return solve(&(a / d), &(-c / d).mod_floor(&n), &(n / d));
}

// Based on https://github.com/simon-andrews/rust-modinverse (public domain)

pub fn egcd<T: Clone + Integer>(a: &T, b: &T) -> (T, T, T) {
    assert!(a < b);
    if *a == T::zero() {
        return (b.clone(), T::zero(), T::one());
    }
    else {
        let (g, x, y) = egcd(&(b.clone() % a.clone()), a);
        return (g, y - (b.clone() / a.clone()) * x.clone(), x);
    }
}

pub fn modinverse<T: Clone + Integer>(a: &T, m: &T) -> Option<T> {
    let (g, x, _) = egcd(a, m);
    if g != T::one() {
        return None;
    }
    else {
        return Some(x % m.clone());
    }
}