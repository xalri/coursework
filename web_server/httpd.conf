ServerRoot "/usr/local/apache2"

# Modules
LoadModule mime_module modules/mod_mime.so
LoadModule dir_module modules/mod_dir.so
LoadModule log_config_module modules/mod_log_config.so
LoadModule alias_module modules/mod_alias.so
LoadModule rewrite_module modules/mod_rewrite.so
LoadModule unixd_module modules/mod_unixd.so
LoadModule ssl_module modules/mod_ssl.so
LoadModule cgi_module modules/mod_cgi.so
LoadModule php7_module modules/libphp7.so
LoadModule rpaf_module modules/mod_rpaf.so
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_http_module modules/mod_proxy_http.so
LoadModule proxy_balancer_module modules/mod_proxy_balancer.so
LoadModule lbmethod_byrequests_module modules/mod_lbmethod_byrequests.so
LoadModule autoindex_module modules/mod_autoindex.so
LoadModule authn_core_module modules/mod_authn_core.so
LoadModule authn_file_module modules/mod_authn_file.so
LoadModule authz_core_module modules/mod_authz_core.so
LoadModule authz_user_module modules/mod_authz_user.so
LoadModule auth_basic_module modules/mod_auth_basic.so
LoadModule status_module modules/mod_status.so
LoadModule slotmem_shm_module modules/mod_slotmem_shm.so

# Basic settings
Listen 80
User daemon
Group daemon
DocumentRoot "/usr/local/apache2/htdocs"
DirectoryIndex index.php
ServerName https://localhost

# Logging
ErrorLog "logs/error_log"
LogLevel warn
LogFormat "%h %l %u %t \"%r\" %>s %b" common
CustomLog "logs/access_log" common

# Allow scripts in /cgi-bin/
ScriptAlias /cgi-bin/ "/usr/local/apache2/cgi-bin/"

# Sets REMOTE_ADDR to the value of the X-Real-IP HTTP header, required for
# IP based access control when running behind a reverse proxy
RPAF_Enable On
# The IP range used by my docker setup -- nginx will be 172.17.0.1
RPAF_ProxyIPs 172.17.0.0/24
RPAF_SetHostName On
RPAF_SetHTTPS On
RPAF_SetPort On
RPAF_ForbidIfNotProxy Off

# Custom error pages
ErrorDocument 400 /error/HTTP400.html
ErrorDocument 401 /error/HTTP401.html
ErrorDocument 403 /error/HTTP403.html
ErrorDocument 404 /error/HTTP404.html
ErrorDocument 500 /error/HTTP500.html
ErrorDocument 501 /error/HTTP501.html
ErrorDocument 502 /error/HTTP502.html
ErrorDocument 503 /error/HTTP503.html

# SSL Config
Listen 443
<VirtualHost *:443>
ServerName wp.xa1.uk
SSLEngine on
SSLCipherSuite RC4-SHA:AES128-SHA:HIGH:!aNULL:!MD5
SSLHonorCipherOrder on
SSLCertificateFile "/etc/ssl/certs/wp.pem"
SSLCertificateKeyFile "/etc/ssl/keys/wp.pem"
</VirtualHost>

<Proxy balancer://cluster>
   BalancerMember http://localhost:8080 timeout=15 retry=300
   BalancerMember http://cs-csc3422-001.ncl.ac.uk status=H
   ProxySet lbmethod=byrequests
</Proxy>

<Location /solver>
   ProxyPass balancer://cluster
   ProxyPassReverse balancer://cluster
</Location>

# Deny access to the root of the file system...
<Directory />
    AllowOverride none
    Require all denied
</Directory>

# Allow access to htdocs
<Directory "/usr/local/apache2/htdocs">
    Options FollowSymLinks
    AllowOverride All
    Require all granted

    <FilesMatch .php$>
        SetHandler application/x-httpd-php
    </FilesMatch>

</Directory>

<Directory "/usr/local/apache2/cgi-bin">
    Require all granted
</Directory>

<Files ".ht*">
    Require all denied
</Files>
