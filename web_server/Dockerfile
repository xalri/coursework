FROM centos:latest

# Update the repos and install dependencies
RUN yum -y update
RUN yum -y install \
   wget \
   gcc \
   make \
   unzip \
   zlib \
   openssl \
   apr-devel \
   apr-util-devel \
   openssl-devel \
   libxml2-devel \
   httpd-devel \
   curl-devel \
   openssl \
   mysql \
   gd \
   gd-devel

# Download & compile apache httpd, installs to /usr/local/apache2.
# Note that if VERSION is not a current release the mirror must be changed
# to the archive site.
RUN VERSION='2.4.29' && \
    wget http://mirror.vorboss.net/apache//httpd/httpd-${VERSION}.tar.gz && \
    tar -zxvf httpd-${VERSION}.tar.gz && \
    rm httpd-${VERSION}.tar.gz && \
    cd httpd-${VERSION} && \
    ./configure \
       --enable-cgi \
       --enable-so \
       --enable-rewrite && \
    make && \
    make install

# Download & compile php 7.1 with mysql support
RUN VERSION='7.1.10' && \
    wget http://uk1.php.net/get/php-${VERSION}.tar.gz/from/this/mirror && \
    tar -zxvf mirror && \
    rm mirror && \
    cd php-${VERSION} && \
    ./configure \
       --with-apxs2=/usr/local/apache2/bin/apxs \
       --with-zlib \
       --with-mysqli \
       --with-gd \
       --with-curl && \
    make && \
    make install

# Build the mod_rpaf module for setting REMOTE_ADDR when behind a reverse proxy
RUN wget https://github.com/gnif/mod_rpaf/raw/stable/mod_rpaf.c && \
   apxs -i -c mod_rpaf.c
RUN mv /.libs/mod_rpaf.so /usr/local/apache2/modules/

# Download wordpress..
RUN wget https://wordpress.org/latest.zip && \
   unzip latest.zip && \
   rm latest.zip && \
   cd wordpress && \
   mv ./* /usr/local/apache2/htdocs

# The server's hostname.
ENV HOSTNAME "localhost"

# Generate a self-signed PKCS#10 cert for basic HTTPS
# TODO: allow external certs?
RUN mkdir -p /etc/ssl/keys && \
   openssl req -x509 -newkey rsa:4096 -days 365 \
      -keyout /etc/ssl/keys/wp.pem \
      -out /etc/ssl/certs/wp.pem \
      -nodes \
      -subj "/C=UK/CN=${HOSTNAME}" && \
   chown daemon:daemon /etc/ssl/keys/wp.pem && \
   chmod -R 400 /etc/ssl/keys

# Add configuration files, themes, scripts, etc.
ADD htdocs /usr/local/apache2/htdocs
ADD httpd.conf /usr/local/apache2/conf/
ADD .htpasswd /usr/local/apache2/
ADD cgi-bin /usr/local/apache2/cgi-bin
ADD wp.sql /
ADD init.sh /
RUN chmod +x /init.sh
RUN chown -R daemon:daemon /usr/local/apache2/htdocs

# Very secure default database credentials >_>
ENV DB_NAME "wp"
ENV DB_USER "wp"
ENV DB_PASSWORD "hunter2"
ENV DB_HOST "mysql"

ADD congruence_solver/target/x86_64-unknown-linux-musl/release/congruence_solver /
ENV ROCKET_ENV "prod"
ENV ROCKET_PORT "8080"

EXPOSE 80
EXPOSE 443

CMD /init.sh
