This page describes how the requirements in the specification have been implemented.

LAMP Stack {#lamp-stack}
----------

The website runs in docker, the build scripts for the image are available on gitlab (https://gitlab.com/xalri/csc3422). The repository contains a `Dockerfile`, a script which contains all the commands which build up the image from a base CentOS7 image, it’s commented to explain what each section does.

Dependencies are installed `yum` then Apache httpd 2.4 and PHP7 are installed from source. MySQL runs in a seperate contianer and is linked when the container starts:

       docker run --link mysql:mysql -e DB_USER=wp, DB_PASS=etc...

Wordpress {#wordpress}
---------

Wordpress is installed to the `htdocs` directory, which the apache configuration uses as the document root.

To re-create this wordpress setup in the image we need to include the database, so we use the `mysqldump` command to store the configuration in an SQL file.

Since wordpress uses absolute URLs, we need to modify the database to work in different environments: The `save_db.sh` script in the repository connects to a running container and saves the current database to an sql file, replacing all instances of the hostname with a placeholder, `__hostname`. Then, when the container is first started, the init script replaces \_\_hostname with the actual hostname, passed to the container as an environmental variable. This gives us an sql file with the correct hostname for the current deployment (localhost when developing, `xa1.uk` when running on this server), which we run on the linked database, re-creating the wordpress setup, including configuration, posts, this page, etc.

So, to persist changes in wordpress in the docker image, we run the container locally, make the changes (adding pages, posts, etc..) then run `./save_db.sh` and recreate the image.

Wordpress is configured to use a child-theme of `storefront`, which currently makes no changes to it’s parent. The `woocommerce` plugin provides a simple e-commerce store, on which we sell text files containing small prime numbers.

Web service {#web-service}
-----------

The server doesn’t include a setup for Jetty.

In an attempt to still satisfy the related configuration requirements (load balancing and reverse proxying), we instead include a small web service written in rust. The source is included in the `congruence_solver` directory of the repository. The compiled executable is added to the docker image and started by the init script (init.sh). It listens to GET and POST requests to the path `/`, GET requests return a simple form with inputs for a linear congruence equation, for POST request the service computes and returns the solution.

Server configuration {#server-configuration}
--------------------

This section describes how the required features of the server configuration are satisfied.

### URL rewrites: {#url-rewrites}

We add simple rewriting rules to the `.htaccess` file in the document root. The rules check for any requested URL for which there isn’t an existing file or directory, and passes the request to `index.php`. Wordpress uses it’s own URL rewriting which is configurable via it’s settings page to provide nice URLS for posts and pages.

### HTTPS: {#https}

When building the image we generate a self-signed SSL certificate. Apache is configured to server the website over HTTPS using this certificate. For this live instance of the website, the container is behind an NGINX reverse proxy. NGINX is configured to use a certificate signed by Let’s Encrypt, which is trusted by current web browsers. So NGINX terminates the TLS connection and forwards the HTTP request to the container.

### Authentication: {#authentication}

The `/downloads` directory is accessible to the user `root` with the password `CSC3422-pass`, this is configured in the directories `.htaccess` file.

### Access control: {#access-control}

Since the website runs beind a reverse proxy this step is a little more involved. In the Dockerfile we compile and install a new apache module, `mod_rpaf`, which rewrites `REMOTE_ADDR` based on HTTP headers – NGINX is configured to add the `X-Real-IP` header, so the apache server now knows the real IP of the request.

We use this in the root `.htaccess` file, adding a rule which returns a different header image when the connection comes from an IP block owned by the university (128.240.0.0/16). When the website is accessed from outside the university an image of the quayside is displayed. Otherwise the header is an image of the Urban Sciences Building. Both images were created by me and are licenced CC0.

### Error pages {#error-pages}

Simple error pages from https://github.com/AndiDittrich/HttpErrorPages have been added.

### CGI {#cgi}

A few lines added to the apache configuration add an alias for the `cgi-bin` directory, allow all requests to that directory, and enable the CGI scripts. The scripts themselves are made exectable and the shebang is added to run the scripts with bash.

### Reverse Proxy {#reverse-proxy}

Requests to `/solver` are forwarded to port 8080, on which a simple web service is running. The ‘Solver’ page of the website contains an iframe containing the service.

### Load balancing {#load-balancing}

Apache is configured to fall back to http://cs-csc3422-001.ncl.ac.uk/ if it can’t connect to the web service on /solver.
