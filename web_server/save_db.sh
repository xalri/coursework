#!/bin/bash

rm wp.sql
echo 'Backing up database..'
docker exec -it mysql \
   mysqldump -u wp -phunter2 \
   --add-drop-table --quick --extended-insert \
   wp > wp.sql

echo 'Replacing https://localhost with __hosname..'
sed -i 's|https://localhost|__hostname|g' wp.sql
sed -i '1d' wp.sql
