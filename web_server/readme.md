A web server configuration with an eclectic mix of features.

The server is built as a docker image from CentOS7.  Apache httpd 2.4 and PHP 7
are installed from source.  A self signed PKCS#10 cert is generated when the
image is built, and used to provide HTTPS.

Wordpress plays host to an online store where one can purchase text files
containing the first `n` prime numbers.

Of course, they're also available through the `/downloads/` section of the
website, but fear not for our buisness, a prospective customer is unlikely to
guess that `root`'s password is `CSC3422-pass`.

To provide extra value to our customers the website includes a solver for
linear congruencies. Requests to `solver/` are forwarded to an embedded jetty
web server where a form is provided for solving `ax + b = 0 (mod m)` given
arbitrarily large values of `a`, `b`, and `m`.

Viewing the website front page from the university's IP block will display a
picutre of the Urban Sciences Building in the header.

Et cetera and so on and so forth.
