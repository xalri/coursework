<?php
/** The base configuration for WordPress */

define('DB_NAME', getenv('DB_NAME'));
define('DB_USER', getenv('DB_USER'));
define('DB_PASSWORD', getenv('DB_PASSWORD'));
define('DB_HOST', getenv('DB_HOST'));
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

$table_prefix  = 'wp_';
define('WP_DEBUG', false);

define('FORCE_SSL_ADMIN', true);
$_SERVER['HTTP_X_FORWARDED_PROTO'] = 'https';
$_SERVER['HTTPS'] = 'on';
$_SERVER['SERVER_PORT'] = 443;

define('AUTH_KEY',         '|-f6cZ~l0}gpp)NHg8/Aqc;o]Pu3&M;]wTI/Z|O4n afDa_qyqV~:9PEprTk@.N9');
define('SECURE_AUTH_KEY',  '.=~Q-]Sjf,tlN@pVlr<t^,HXCuFyg?Str+[Y+{?o=)gBe%l++9 FUEh J$QU4DX?');
define('LOGGED_IN_KEY',    'e7JM+Jj=5t5djbgl/6HF|;_cZ.[NBQSMuGBxex7l0.z]-vT--Ffx3nPd:1^BP+J/');
define('NONCE_KEY',        '9Q}hA-v6$b4kAtG8VwdggKcT-2-r:$d/r|Y9_K! 4|k5vM%d!<zHh;|Ba&t6Y$p;');
define('AUTH_SALT',        'JPhp~j+m3AE=5BisYiajI9hs{JQ~|UjY<+[]|5s@1Oyb@},9yeN-^eM@:2EPzGW3');
define('SECURE_AUTH_SALT', 'pI>z8C-U{6+2XU|tfj+p5.HopZH$T9Hlh2z<X|U9NN->7L0|qA`M0Fc7^IPbF[XA');
define('LOGGED_IN_SALT',   ']F@j*G2.<$lnXfy-I_j(]2/t.#tJ+z!vgZQ|CW;Az~D-I4w^-iKJAJQy+c?J5zui');
define('NONCE_SALT',       'Cc/nEJ4pC&3}=?7Ba>mD6 r.<D|A}Hgs&;VzUQf;aq+T%l;c[oS#Yvk/ucy$++sg');

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
