#!/bin/bash

export ADDRESS=https://$HOSTNAME

if [ ! -f /init ]; then
   echo "Replacing __hostname with ${ADDRESS} in apache config"
   sed -i "s|__hostname|${ADDRESS}|g" /usr/local/apache2/conf/httpd.conf

   echo "Replacing __hostname with ${ADDRESS} in wp.sql"
   sed -i "s|__hostname|${ADDRESS}|g" /wp.sql

   echo 'Restoring database backup'
   mysql -h $DB_HOST -u $DB_USER -p$DB_PASSWORD $DB_NAME < /wp.sql
   echo 'Finished initialisation'
   touch /init
fi

echo 'Starting apache'
/usr/local/apache2/bin/apachectl start

echo 'Starting app'
/congruence_solver &

echo 'Following access log:'
touch /usr/local/apache2/logs/access_log
tail -f /usr/local/apache2/logs/access_log

