--- 
title: "Security analysis of a scheme for searchable encryption" 
author: "Lewis Hallam (150184050)"
bibliography: references.bib
geometry: margin=2.8cm
fontsize: 11pt
output: pdf_document
header-includes:
 - \linespread{1.1}
...


Section 8 of US9553867 describes a system where users access encrypted data 
stored on an untrusted server via a proxy which handles the encryption. 
Keys are stored in a keystore connected to the proxy.
The proxy has access to the encrypted data via the server and the keys via the 
keystore, so must be trusted. The data is searchable with pre-defined keywords 
and can be sorted by the first character of each field without the need to fetch
and decrypt all the data.

The threat this system attempts to guard against is an adversary with 
full access to the server.


## Searchable encryption with random tags

The proxy encrypts each document uploaded by a client with a standard symmetric encryption
algorithm. To each instance of a keyword in the plaintext it appends to
the same location in the ciphertext a (presumably unique) random string of
data of a known size, which we call the tag. The tag is associated with
the keyword in an index kept by the proxy and the encrypted data is sent
to the server. To find instances of a keyword the proxy queries the server
with every tag associated with that keyword in it's index.

The first observation is that the proxy storing an index obviates the need
for searchable encryption, since for each keyword the proxy could store direct
references to the data rather than the tags. This requires no more trust
than the proxy already has since in the proposed scheme it can obviously
recover these references; It would prevent any leakage of data to the server;
and would significantly increase performance since searching for the tags
would be unnecessary.

That aside, assuming the underlying encryption scheme satisfies ciphertext
indistinguishability from random noise the random tag appended to each
keyword is indistinct, so knowledge of the ciphertext is insufficient to
break the scheme. The scheme also satisfies forward privacy: the server
doesn't know if a newly added document matches any previous queries. Our
threat model includes an adversary with access to the server, so as well
as the encrypted data they have access to the queries made by the proxy.
The tags associated with a particular keyword will necessarily be queried
together for the proxy to get full results for the client's query. This
leaks:

 - The documents particular keywords exist in;
 - The frequency of the keywords in each document;
 - The positions in the documents of the keywords;
 - An upper bound on the length of each keyword, by observing gaps
   between the locations matched across many queries.

@CCS15 define a leakage hierarchy for searchable
encryption. The highest leakage, and that which applies to the proposed
scheme, is L4:

> "The server learns the pattern of locations in the text where
each word occurs and its total number of occurrences in the document
collection"

It is trivially observed that if the adversary knows the plaintext for
some file then a query for a keyword which exists in that file reveals the
keyword itself as well as all other instances of the keyword in the set
of documents. Beyond this, Cash describes various statistical attacks from
which the plaintext of other documents can be partially reconstructed.

The proxy could send fake queries to mitigate the leak at the cost of
reduced performance. @SP00 propose periodically re-encrypting
the documents with a new key as a defence against statistical attacks. In
the proposed scheme the random tags would also be re-generated.

The proposed searchable encryption scheme is only secure
under an adversarial model in which an attacker cannot observe
queries, and provides no advantage over the proxy storing an index which
references documents directly.

## Order preserving encryption

The patent proposes not encrypting the first character of each data field to
allow the server to sort the data by this character. This is less useful than
full order preserving encryption as it doesn't support range queries, and
if the result of some query must be sorted then the proxy still needs to do
most of the work after decrypting the result.

For a data set where each character occurs at least once as the first character,
an actual order preserving encryption scheme would offer no benefit over not
encrypting the character at all, as an adversary could simply sort the data to
recover the unencrypted value.

# References
