All the steps described in the coursework specification were completed.

## Sending

On sending a new message, we flip the first bit of the sequence number, set
the seq field of the message to that sequence number, take a copy of the
message, then pass it to `AMSendReceiveI`. We copy the saved version of the
message back into the new buffer returned by the call to `send`, calculate and
store the time at which we want to send the next message, then set the timer
to fire periodically.

When the timer fires, we check if we've received an acknowledegment for the last
message we sent, if not, we resend the message. If so, we send a new message
and the process above repeats.

## Receiving

On receiving a message we switch on it's type -- if it's an acknowledgement we
check the sequence number, if it's the same as that which we last sent, we
set the timer to fire at the previously calculated time.
If the message is some data, we send an acknowledgement (hardcoded to send to
the echo node, I couldn't find a way to get the ID of the sender of the message),
then display the counter in the message's payload on the device's LEDs.

## Reliability

If data messages are lost, there will be no acknowledgement, so the periodic
timer will fire to resend the message every 200ms (now changed to 800ms -- congestion?).
Since we calculate the time
to send the next message when we send the previous, the pattern of the LEDs
should even out, e.g. if one message is 300ms late, the next will be 300ms early.
The situation is the same if acknowledegement message are lost -- the message is
resent automatically.

# Sample data

Messages with `63` in the fith word are those we send, `03` are those we receive.
There seems to be a bug in the output in which the destination is always 0 --
in actuality the destinations should be the inverse of the above.

## Messages when working normally

Here most messages are send, received and acknowledged normally -- I had to increase
the resend timer to ~800ms to get this, presumably due to congestion, the rack's
quite full at the moment ^^

```
// Sending a message
00 00 00 00 62 08 00 06 00 55 00 00 00 62 00 04
// Got the message
00 00 00 00 03 08 00 06 00 55 00 00 00 62 00 04
// Sending an acknowledement for the above message
00 00 00 00 62 08 00 06 00 CC 00 00 00 62 00 00
// Received the acknowledgement
00 00 00 00 03 08 00 06 00 CC 00 00 00 62 00 00

// .. and so on:
00 00 00 00 62 08 00 06 00 55 00 01 00 62 00 05
00 00 00 00 03 08 00 06 00 55 00 01 00 62 00 05
00 00 00 00 62 08 00 06 00 CC 00 01 00 62 00 00
00 00 00 00 03 08 00 06 00 CC 00 01 00 62 00 00

00 00 00 00 62 08 00 06 00 55 00 00 00 62 00 06
00 00 00 00 03 08 00 06 00 55 00 00 00 62 00 06
// This ack seems to have been lost.
00 00 00 00 62 08 00 06 00 CC 00 00 00 62 00 00
00 00 00 00 62 08 00 06 00 55 00 00 00 62 00 06
00 00 00 00 03 08 00 06 00 55 00 00 00 62 00 06
00 00 00 00 62 08 00 06 00 CC 00 00 00 62 00 00
00 00 00 00 03 08 00 06 00 CC 00 00 00 62 00 00

00 00 00 00 62 08 00 06 00 55 00 01 00 62 00 07
00 00 00 00 03 08 00 06 00 55 00 01 00 62 00 07
00 00 00 00 62 08 00 06 00 CC 00 01 00 62 00 00
00 00 00 00 03 08 00 06 00 CC 00 01 00 62 00 00

00 00 00 00 62 08 00 06 00 55 00 00 00 62 00 08
00 00 00 00 03 08 00 06 00 55 00 00 00 62 00 08
00 00 00 00 62 08 00 06 00 CC 00 00 00 62 00 00
00 00 00 00 03 08 00 06 00 CC 00 00 00 62 00 00
```

## Messages with packet loss

Messages with counter `09` and `0A` are resent.

```
00 00 00 00 62 08 00 63 00 55 00 01 00 62 00 07
00 00 00 00 03 08 00 63 00 55 00 01 00 62 00 07
00 00 00 00 62 08 00 63 00 CC 00 01 00 62 00 00
00 00 00 00 03 08 00 63 00 CC 00 01 00 62 00 00
00 00 00 00 62 08 00 63 00 55 00 00 00 62 00 08
00 00 00 00 03 08 00 63 00 55 00 00 00 62 00 08
00 00 00 00 62 08 00 63 00 CC 00 00 00 62 00 00
00 00 00 00 03 08 00 63 00 CC 00 00 00 62 00 00
// Sending a message
00 00 00 00 62 08 00 63 00 55 00 01 00 62 00 09
// No ACK in 800ms, resending the message
00 00 00 00 62 08 00 63 00 55 00 01 00 62 00 09
// Recieved the message
00 00 00 00 03 08 00 63 00 55 00 01 00 62 00 09
00 00 00 00 62 08 00 63 00 CC 00 01 00 62 00 00
00 00 00 00 03 08 00 63 00 CC 00 01 00 62 00 00
00 00 00 00 62 08 00 63 00 55 00 00 00 62 00 0A
00 00 00 00 62 08 00 63 00 55 00 00 00 62 00 0A
00 00 00 00 03 08 00 63 00 55 00 00 00 62 00 0A
00 00 00 00 62 08 00 63 00 CC 00 00 00 62 00 00
00 00 00 00 03 08 00 63 00 55 00 00 00 62 00 0A
00 00 00 00 62 08 00 63 00 CC 00 00 00 62 00 00
00 00 00 00 03 08 00 63 00 CC 00 00 00 62 00 00
00 00 00 00 62 08 00 63 00 55 00 01 00 62 00 0B
00 00 00 00 03 08 00 63 00 55 00 01 00 62 00 0B
00 00 00 00 62 08 00 63 00 CC 00 01 00 62 00 00
00 00 00 00 03 08 00 63 00 CC 00 01 00 62 00 00
```
