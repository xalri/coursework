 #include <Timer.h>
 #include "BlinkToRadio.h"

module BlinkToRadioC {
  uses {
    interface Boot;
    interface SplitControl as RadioControl;

    interface Leds;
    interface Timer<TMilli> as Timer0;

    interface Packet;
    interface AMPacket;
    interface AMSendReceiveI;
  }
}

implementation {
  enum {
     RESEND_MS = 800
  };

  event void Boot.booted() {
    call RadioControl.start();
  };

  event void RadioControl.startDone(error_t error) {
    if (error == SUCCESS) {
      call Timer0.startOneShot(TIMER_PERIOD_MILLI);
    }
  };

  event void RadioControl.stopDone(error_t error){};

  // ======================================================
  // ---------------------  Sender  -----------------------
  // ======================================================

  // Here we send and resend messages -- messages are sent with some regularity,
  // each time the timer fires we will either send a new message, or, if our
  // previously sent message hasn't yet been acknowledged, resend that.
  // The message payload is a counter.

  // A counter which is incremented with each message we send.
  uint16_t counter = 0;
  // An empty message buffer.
  message_t send_msg_buf;
  // A pointer to the next message buffer to use when sending a message.
  message_t* send_msg = &send_msg_buf;
  // The sequence number of the previously sent message.
  uint16_t send_seq = 0;
  // Set to true after sending a message, then reset to false as soon as
  // we get an acknowledgement.
  bool resend = FALSE;
  // The time at which we last sent a message
  uint32_t last_send = 0;

  // Attempt to send a message. Takes a pointer to a pointer to a buffer, and
  // sets the pointer to a copy of the sent message in a new buffer.
  // Returns true if the message was sent successfully, or false if it hasn't
  // been sent (if the radio send queue is full).
  bool send(message_t** msg) {
    // Take a copy of the message we want to send
    message_t prev = **msg;
    // Attempt to send the message, which returns a pointer to a new buffer.
    message_t* result = call AMSendReceiveI.send(*msg);
    // If the returned pointer is the same as that we sent, the message failed
    // to send.
    bool success = (result != *msg);
    // Set the msg pointer to point to the new buffer
    *msg = result;
    // Copy the sent message into that buffer.
    **msg = prev;
    return success;
  }

  // The event called when the timer fires -- either a periodic timer for resending
  // a message or a oneshot timer to increment the counter and send a new message.
  event void Timer0.fired() {
    BlinkToRadioMsg* btrpkt;

    if (resend) {
       // On sending a message we copy the sent message into the new buffer,
       // so to re-send we just call send again.
       // We don't need check the return value as the message will be sent again
       // automatically if the message isn't sent, as we won't receive an
       // acknowledgement.
       send(&send_msg);
       return;
    }

    call AMPacket.setType(send_msg, AM_BLINKTORADIO);
    call AMPacket.setDestination(send_msg, DEST_ECHO);
    call AMPacket.setSource(send_msg, TOS_NODE_ID);
    call Packet.setPayloadLength(send_msg, sizeof(BlinkToRadioMsg));

    btrpkt = (BlinkToRadioMsg*)(call Packet.getPayload(send_msg, sizeof (BlinkToRadioMsg)));
    counter++;
    send_seq ^= 1;

    btrpkt->type = TYPE_DATA;
    btrpkt->seq = send_seq;
    btrpkt->nodeid = TOS_NODE_ID;
    btrpkt->counter = counter;

    send(&send_msg);
    resend = TRUE;

    // Set last_send such that when we get an acknowledgement the timer will
    // be set to fire at the time it would have had we not changed it for
    // resending.
    last_send = call Timer0.getNow();
    // Restart the timer with a period for resending.
    call Timer0.stop();
    call Timer0.startPeriodic(RESEND_MS);
  }

  // ======================================================
  // --------------------  Receiver  ----------------------
  // ======================================================

  // Here messages are received. On receiving some data, we display the data
  // on the node's LEDs, then send an acknowledgement of receipt. On receiving
  // an acknowledgement, we tell the sender that it can send the next message.

  // An empty message buffer.
  message_t ack_msg_buf;
  // A pointer to the next message buffer to use when sending an acknowledement.
  message_t* ack_msg = &ack_msg_buf;

  event message_t* AMSendReceiveI.receive(message_t* msg) {
    uint8_t len = call Packet.payloadLength(msg);
    BlinkToRadioMsg* btrpkt = (BlinkToRadioMsg*)(call Packet.getPayload(msg, len));

    if (btrpkt->type == TYPE_DATA) {
      uint16_t seq = btrpkt->seq;
      if (btrpkt->nodeid != TOS_NODE_ID) {
        return msg;
      }
      call Leds.set(btrpkt->counter);

      call AMPacket.setType(ack_msg, AM_BLINKTORADIO);
      call AMPacket.setDestination(ack_msg, DEST_ECHO);
      call AMPacket.setSource(ack_msg, TOS_NODE_ID);
      call Packet.setPayloadLength(ack_msg, sizeof(BlinkToRadioMsg));

      btrpkt = (BlinkToRadioMsg*)(call Packet.getPayload(ack_msg, sizeof (BlinkToRadioMsg)));
      btrpkt->type = TYPE_ACK;
      btrpkt->seq = seq;
      btrpkt->nodeid = TOS_NODE_ID;
      btrpkt->counter = 0;

      send(&ack_msg);
    } else if (btrpkt->type == TYPE_ACK) {
      // If we're waiting on an ACK, check the seq, if equal to our last send
      // we're ready to send a new message -- set resend to false and restart
      // the timer to fire at the time set the last time we sent.
      if (resend && btrpkt->seq == send_seq) {
         resend = FALSE;
         call Timer0.startOneShotAt(last_send, TIMER_PERIOD_MILLI);
      }
    }

    return msg; // no need to make msg point to new buffer as msg is no longer needed
  }
}


