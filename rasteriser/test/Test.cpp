#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "rasteriser/SoftwareRasteriser.h"
#include "rasteriser/Colour.h"

TEST_CASE("Triangle area is correct") {
    auto a = Vector4(2.0, 3.0, 0.0, 0.0);
    auto b = Vector4(5.0, -1.0, 0.0, 0.0);
    auto c = Vector4(-2.0, 0.0, 0.0, 0.0);

    REQUIRE(TriArea2D(a, b, c) == -12.5);
    REQUIRE(TriArea2D(c, b, a) == 12.5);
};

TEST_CASE("lerp") {
    REQUIRE(Colour::Lerp(0.5, 0, 10) == 5);
    REQUIRE(Colour::Lerp(0.0, 0, 10) == 0);
    REQUIRE(Colour::Lerp(1.0, 0, 10) == 10);
}

// TODO: test gamma functions etc.
