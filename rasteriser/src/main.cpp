#include "rasteriser/SoftwareRasteriser.h"

#include <chrono>
#include <thread>
#include <random>
#include <memory>

#include "world.h"

auto const RESOLUTION_X = 1280;
auto const RESOLUTION_Y = 720;
auto const FOV = 75.f;
auto const Z_NEAR = 0.01f;
auto const Z_FAR = 1000.f;
auto const TARGET_FPS = 60;
auto const PER_FRAME_MS = 1000 / TARGET_FPS;

class State {
private:
    std::vector<std::unique_ptr<Entity>> entities;
    SoftwareRasteriser &r;
    Vector3 cameraPos;
    Vector3 cameraLook;

public:
    explicit State(SoftwareRasteriser &rasteriser) : r(rasteriser), cameraLook(0, 0, 1) {
        auto aspect = RESOLUTION_X / (float) RESOLUTION_Y;
        rasteriser.SetProjectionMatrix(Matrix4::Perspective(Z_NEAR, Z_FAR, aspect, FOV));
        rasteriser.SetViewMatrix(Matrix4::BuildViewMatrix(cameraPos, cameraLook));
        rasteriser.SetColourInterpolator(ColourInterpolator::LINEAR);

        auto brick = Texture::TextureFromTGA("media/brick.tga");

        // Point field
        auto points = std::make_unique<PointField>(100, 250.0, 200.0, 100.0);
        points->transform = Matrix4::Translation(Vector3(0, 0, 100));
        entities.push_back(std::move(points));

        // A wireframe cube
        auto wireframe = std::make_unique<TriMesh>(
                Wireframe(LoadMeshFile("media/cube_new.asciimesh")));
        wireframe->type = PrimitiveType::LINE_LIST;
        wireframe->transform = Matrix4::Translation(Vector3(0, 3, 8));
        entities.push_back(std::move(wireframe));

        // A rotating cube
        auto mesh = std::make_unique<TriMesh>("media/cube_new.asciimesh");
        mesh->transform = Matrix4::Translation(Vector3(1.8, 0, 4));
        auto spinner = std::make_unique<Dynamic>(std::move(mesh));
        spinner->angularMomentum.y = 1.0;
        spinner->angularMomentum.z = 0.5;
        entities.push_back(std::move(spinner));

        // A textured cube
        auto mesh2 = std::make_unique<TriMesh>("media/cube_new.asciimesh");
        mesh2->transform = Matrix4::Translation(Vector3(-1.8, 0, 4));
        mesh2->texture = brick;
        auto spinner2 = std::make_unique<Dynamic>(std::move(mesh2));
        spinner2->angularMomentum.y = -0.5;
        spinner2->angularMomentum.x = -1.0;
        entities.push_back(std::move(spinner2));

        // Transparent triangles
        auto triangles = std::vector<Vertex>{
                Vertex(Vector4(0, 0, 2, 1), Colour(255, 255, 255, 255)),
                Vertex(Vector4(2, 1, 2, 1), Colour(255, 255, 255, 0)),
                Vertex(Vector4(2, -1, 2, 1), Colour(255, 255, 255, 0)),
                Vertex(Vector4(0, 0, 2, 1), Colour(255, 255, 255, 255)),
                Vertex(Vector4(-2, -1, 2, 1), Colour(255, 255, 255, 0)),
                Vertex(Vector4(-2, 1, 2, 1), Colour(255, 255, 255, 0))
        };
        auto mesh3 = std::make_unique<TriMesh>(triangles);
        entities.push_back(std::move(mesh3));
    }

    State(const State&) = delete;
    State& operator=(const State&) = delete;
    ~State() = default;

    void update() {
        // Camera movement
        if (Keyboard::KeyDown(KEY_W)) {
            cameraPos.y += 0.02; cameraLook.y += 0.02;
            r.SetViewMatrix(Matrix4::BuildViewMatrix(cameraPos, cameraLook));
        }
        if (Keyboard::KeyDown(KEY_S)) {
            cameraPos.y -= 0.02; cameraLook.y -= 0.02;
            r.SetViewMatrix(Matrix4::BuildViewMatrix(cameraPos, cameraLook));
        }
        if (Keyboard::KeyDown(KEY_A)) {
            cameraPos.x += 0.02; cameraLook.x += 0.02;
            r.SetViewMatrix(Matrix4::BuildViewMatrix(cameraPos, cameraLook));
        }
        if (Keyboard::KeyDown(KEY_D)) {
            cameraPos.x -= 0.02; cameraLook.x -= 0.02;
            r.SetViewMatrix(Matrix4::BuildViewMatrix(cameraPos, cameraLook));
        }

        // Switch between linear and gamma-correct colour interpolation
        if (Keyboard::KeyDown(KEY_G) && !Keyboard::KeyHeld(KEY_G)) {
            if (r.GetColourInterpolator() == LINEAR) r.SetColourInterpolator(GAMMA);
            else r.SetColourInterpolator(LINEAR);
        }

        for (auto& e : entities) e->update();
    }

    void draw() {
        for (auto& e : entities) e->draw(r, Matrix4());
    }
};

int main() {
    SoftwareRasteriser r(RESOLUTION_X, RESOLUTION_Y);
    State state(r);

    while (r.UpdateWindow()) {
        r.ClearToColour(Colour(10, 10, 10, 255));
        auto updateStart = std::chrono::system_clock::now();

        state.update();
        state.draw();
        r.SwapBuffers();

        // Limit to TARGET_FPS..
        auto updateEnd = std::chrono::system_clock::now();
        auto updateTime = updateEnd - updateStart;
        auto sleep = std::chrono::milliseconds(PER_FRAME_MS) - updateTime;
        if (sleep > std::chrono::milliseconds()) {
            std::this_thread::sleep_for(sleep);
        }
    }

    return 0;
}
