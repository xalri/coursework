#include "math/Vector3.h"

Vector4 Vector3::ToVector4(float w) {
    return Vector4(x, y, z, w);
}

float Vector3::GetMaxElement() {
    return std::max(x, std::max(y, z));
}

float Vector3::GetMinElement() {
    return std::min(x, std::min(y, z));
}

Vector2 Vector3::ToVector2() {
    return Vector2(x, y);
}
