#include "math/Vector4.h"

Vector2 Vector4::ToVector2() {
    return Vector2(x, y);
}

Vector3 Vector4::ToVector3() {
    return Vector3(x, y, z);
}

Vector4 Vector4::DivideByW() {
    return Vector4(x / w, y / w, z / w, 1.0);
}

void Vector4::SelfDivisionByW() {
    float recip = 1.0f / w;

    x *= recip;
    y *= recip;
    z *= recip;
    w = 1.0f;
}

float Vector4::GetMaxElement() {
    return std::max(x, std::max(std::max(y, z), w));
}

float Vector4::GetMinElement() {
    return std::min(x, std::min(std::min(y, z), w));
}
