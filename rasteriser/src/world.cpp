#include <random>
#include "world.h"
#include "rasteriser/Vertex.h"

std::vector<Vertex> Wireframe(const std::vector<Vertex> &data) {
    auto out = std::vector<Vertex>();
    for(int i = 0; i < data.size(); i += 3) {
        out.push_back(data[i]);
        out.push_back(data[i+1]);
        out.push_back(data[i+1]);
        out.push_back(data[i+2]);
        out.push_back(data[i+2]);
        out.push_back(data[i]);
    }
    return out;
}

TriMesh::TriMesh(std::vector<Vertex> data): data(data) {}

TriMesh::TriMesh(std::string filename): data(LoadMeshFile(filename)) { }

void TriMesh::update() { }

void TriMesh::draw(SoftwareRasteriser &r, Matrix4 t) {
    r.Draw(data, type, transform * t, texture);
}

Dynamic::Dynamic(std::unique_ptr<Entity> e): inner(std::move(e)) {}

void Dynamic::update() { inner->update(); }

void Dynamic::draw(SoftwareRasteriser &r, Matrix4 t) {
    transform = transform
        * Matrix4::Rotation(angularMomentum.x, Vector3(1, 0, 0))
        * Matrix4::Rotation(angularMomentum.y, Vector3(0, 1, 0))
        * Matrix4::Rotation(angularMomentum.z, Vector3(0, 0, 1))
        * Matrix4::Translation(linearMomentum);
    inner->draw(r, t * transform);
}

PointField::PointField(unsigned n, float w, float h, float d): data(n) {
    std::default_random_engine random;
    std::uniform_real_distribution<double> xDist(-w/2, w/2);
    std::uniform_real_distribution<double> yDist(-h/2, h/2);
    std::uniform_real_distribution<double> zDist(-d/2, d/2);

    for(unsigned i = 0; i < n; i++) {
        data[i] = Vertex(Vector4(xDist(random), yDist(random), zDist(random), 1.0f));
    }
}

void PointField::update() { }

void PointField::draw(SoftwareRasteriser &r, Matrix4 t) {
    r.Draw(data, PrimitiveType::POINT_LIST, transform * t, nullptr);
}
