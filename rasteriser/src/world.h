#pragma once

#include <vector>
#include <memory>
#include "math/Matrix4.h"
#include "rasteriser/Vertex.h"
#include "rasteriser/Texture.h"
#include "rasteriser/SoftwareRasteriser.h"

std::vector<Vertex> Wireframe(const std::vector<Vertex> &data);

class Entity {
public:
    virtual void update() = 0;
    virtual void draw(SoftwareRasteriser &r, Matrix4 transform) = 0;
};

struct TriMesh: Entity {
    TriMesh(std::vector<Vertex> data);
    explicit TriMesh(std::string filename);

    virtual void update();
    virtual void draw(SoftwareRasteriser &r, Matrix4 transform);

    std::vector<Vertex> data;
    Matrix4 transform;
    PrimitiveType type = PrimitiveType::TRIANGLE_LIST;
    Texture *texture = nullptr;
};

struct Dynamic: Entity {
    Dynamic(std::unique_ptr<Entity> e);

    virtual void update();
    virtual void draw(SoftwareRasteriser &r, Matrix4 transform);

    std::unique_ptr<Entity> inner;
    Vector3 angularMomentum;
    Vector3 linearMomentum;
    Matrix4 transform;
};

struct PointField: Entity {
    PointField(unsigned n, float w, float h, float d);
    PointField() = default;

    virtual void update();
    virtual void draw(SoftwareRasteriser &r, Matrix4 transform);

    std::vector<Vertex> data;
    Matrix4 transform;
};
