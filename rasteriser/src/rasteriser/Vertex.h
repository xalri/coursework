/******************************************************************************
Class:Mesh
Implements:
Author:Rich Davison	<richard.davison4@newcastle.ac.uk>
Description: Class to represent the geometric data that makes up the meshes
we render on screen.

-_-_-_-_-_-_-_,------,   
_-_-_-_-_-_-_-|   /\_/\   NYANYANYAN
-_-_-_-_-_-_-~|__( ^ .^) /
_-_-_-_-_-_-_-""  ""   

*//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "rasteriser/Colour.h"
#include "math/Vector4.h"
#include "math/Vector3.h"
#include "math/Vector2.h"
#include "math/Common.h"

#include <string>
#include <fstream>
#include <vector>

using std::ifstream;
using std::string;

enum PrimitiveType {
    POINT_LIST,
    LINE_LIST,
    TRIANGLE_LIST,
    TRIANGLE_STRIP,
    TRIANGLE_FAN,
};

struct Vertex {
    Vector4 pos;
    Colour col;
    Vector2 tex;

    Vertex(Vector4 pos = Vector4(0, 0, 0, 1), Colour col = Colour::White, Vector2 tex = Vector2(0, 0));
    Vertex(const Vertex &o) = default;
};

std::vector<Vertex> LoadMeshFile(const string &filename);
