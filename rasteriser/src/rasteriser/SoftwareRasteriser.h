/******************************************************************************
Class:SoftwareRasteriser
Implements:Window
Author:Rich Davison	<richard.davison4@newcastle.ac.uk>
Description: Class to encapsulate the various rasterisation techniques looked
at in the course material.

This is the class you'll be modifying the most!

-_-_-_-_-_-_-_,------,   
_-_-_-_-_-_-_-|   /\_/\   NYANYANYAN
-_-_-_-_-_-_-~|__( ^ .^) /
_-_-_-_-_-_-_-""  ""   

*//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "math/Matrix4.h"
#include "rasteriser/Vertex.h"
#include "rasteriser/Texture.h"
#include "math/Common.h"
#include "window/Window.h"
#include <functional>

class RenderObject;
class Texture;

enum ColourInterpolator { LINEAR, GAMMA };

class SoftwareRasteriser : public Window {
public:
    SoftwareRasteriser(uint width, uint height);

    ~SoftwareRasteriser(void);

    static void Bresenham(
            const Vector4 &v0,
            const Vector4 &v1,
            std::function<void(int, int, float)> draw);

    static float ClipEdge(Vector4 &v0, Vector4 &v1, unsigned axis);

    static unsigned Outcode(Vector4 &v0);

    void DrawObject(RenderObject *o);

    void Draw(const std::vector<Vertex>& v, PrimitiveType t, const Matrix4 &transform, Texture *texture);

    void Draw(const Vertex* v, unsigned n, PrimitiveType t, const Matrix4 &transform, Texture *texture);

    void DrawPoint(const Vertex &v, Matrix4 &transform);

    void DrawLine(Vertex v0, Vertex v1, const Matrix4 &transform, Texture *texture);

    void DrawTri(Vertex v0, Vertex v1, Vertex v2, const Matrix4 &transform, Texture *texture);

    void ClearToColour(Colour c);

    void SwapBuffers();

    void SetViewMatrix(const Matrix4 &m) {
        viewMatrix = m;
        viewProjMatrix = projectionMatrix * viewMatrix;
    }

    void SetProjectionMatrix(const Matrix4 &m) {
        projectionMatrix = m;
        viewProjMatrix = projectionMatrix * viewMatrix;
    }

    void SetBlendMode(const BlendMode c) { blendMode = c; }
    BlendMode GetBlendMode() { return blendMode; }

    void SetColourInterpolator(const ColourInterpolator c) { colourInterpolator = c; }
    ColourInterpolator GetColourInterpolator() { return colourInterpolator; }

protected:
    int currentDrawBuffer;
    Colour *buffers[2];
    unsigned short *depthBuffer;

    Matrix4 viewMatrix;
    Matrix4 projectionMatrix;
    Matrix4 textureMatrix;
    ColourInterpolator colourInterpolator = ColourInterpolator::LINEAR;
    BlendMode blendMode = BlendMode::NORMAL;
    Matrix4 viewProjMatrix;
    Matrix4 portMatrix;
    Colour *GetCurrentBuffer();

    virtual void Resize();

    inline void DrawPixel(uint x, uint y, const Colour &col) {
        if(y >= screenHeight || x >= screenWidth) return;
        unsigned index = (y * screenWidth) + x;

        Colour &d = buffers[currentDrawBuffer][index];

        Colour c = col;
        switch (blendMode) {
            case BlendMode::ADD: c = d + col; break;
            case BlendMode::MULTIPLY: c = d * col; break;
        }

        unsigned char cMult = col.a;
        unsigned char dMult = (255 - col.a);

        d.r = ((c.r * cMult) + (d.r * dMult)) / 255;
        d.g = ((c.g * cMult) + (d.g * dMult)) / 255;
        d.b = ((c.b * cMult) + (d.b * dMult)) / 255;
        d.a = ((col.a * cMult) + (d.a * dMult)) / 255;
    }

    inline bool DepthTest(uint x, uint y, float z) {
        if(y >= screenHeight || x >= screenWidth) return false;
        auto index = (y * screenWidth) + x;
        if (z > depthBuffer[index]) return false;
        depthBuffer[index] = z;
        return true;
    }
};
