#include "rasteriser/Vertex.h"
#include <fstream>
#include <string>

Vertex::Vertex(Vector4 pos, Colour col, Vector2 tex): pos(pos), col(col), tex(tex) { }

std::vector<Vertex> LoadMeshFile(const std::string &filename) {
	std::ifstream f(filename);
	if(!f) throw std::invalid_argument("File not found");

    unsigned n, hasTex, hasColour;
    f >> n >> hasTex >> hasColour;

    auto data = std::vector<Vertex>(n);

	for (unsigned i = 0; i < n; ++i) {
        int x, y, z;
        f >> x >> y >> z;
        data[i].pos = Vector4(x, y, z, 1.0f);
	}

	if (hasColour) {
		for (unsigned i = 0; i < n; ++i) {
			unsigned char r, g, b, a;
            f >> r >> g >> b >> a;
            data[i].col = Colour(r, g, b, a);
		}
	}

	if (hasTex) {
		for (unsigned i = 0; i < n; ++i) {
            int u, v;
            f >> u >> v;
            data[i].tex = Vector2(u, v);
		}
	}

	return data;
}
