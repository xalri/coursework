#include "rasteriser/SoftwareRasteriser.h"
#include <math.h>
#include <functional>

/*
While less 'neat' than just doing a 'new', like in the tutorials, it's usually
possible to render a bit quicker to use direct pointers to the drawing area
that the OS gives you. For a bit of a speedup, you can uncomment the define below
to switch to using this method.

For those of you new to the preprocessor, here's a quick explanation:

Preprocessor definitions like #define allow parts of a file to be selectively enabled
or disabled at compile time. This is useful for hiding parts of the codebase on a
per-platform basis: if you have support for linux and windows in your codebase, obviously
the linux platform won't have the windows platform headers available, so compilation will
fail. So instead you can hide away all the platform specific stuff:

#if PLATFORM_WINDOWS
 DoSomeWindowsStuff();
#elif PLATFORM_LINUX
 DoSomeLinuxStuff();
 #else
 #error Unsupported Platform Specified!
 #endif

 As in our usage, it also allows you to selectively compile in some different functionality
 without any 'run time' cost - if it's not enabled by the preprocessor, it won't make it to
 the compiler, so no assembly will be generated.

Also, I've implemented the Resize method for you, in a manner that'll behave itself
no matter which method you use. I kinda forgot to do that, so there was a chance you'd
get exceptions if you resized to a bigger screen area. Sorry about that.
*/
#define USE_OS_BUFFERS

SoftwareRasteriser::SoftwareRasteriser(uint width, uint height) : Window(width, height) {
    currentDrawBuffer = 0;
    Resize();
}

SoftwareRasteriser::~SoftwareRasteriser(void) {
    // TODO: is parent destructor called automatically?
#ifndef USE_OS_BUFFERS
    for (auto &buffer : buffers) {
        delete[] buffer;
    }
#endif
    delete[] depthBuffer;
}

void SoftwareRasteriser::Resize() {
    Window::Resize(); //make sure our base class gets to do anything it needs to

#ifndef USE_OS_BUFFERS
    for (auto &buffer : buffers) {
        delete[] buffer;
        buffer = new Colour[screenWidth * screenHeight];
    }
#else
    for (int i = 0; i < 2; ++i) {
        buffers[i] = (Colour*)bufferData[i];
    }
#endif

    delete[] depthBuffer;
    depthBuffer = new unsigned short[screenWidth * screenHeight];

    float zScale = (pow(2.0f, 16) - 1) * 0.5f;

    Vector3 halfScreen = Vector3((screenWidth - 1) * 0.5f, (screenHeight - 1) * 0.5f, zScale);

    portMatrix = Matrix4::Translation(halfScreen) * Matrix4::Scale(halfScreen);
}

Colour *SoftwareRasteriser::GetCurrentBuffer() {
    return buffers[currentDrawBuffer];
}

void SoftwareRasteriser::ClearToColour(Colour c) {
    Colour *buffer = GetCurrentBuffer();

    for (uint y = 0; y < screenHeight; ++y) {
        for (uint x = 0; x < screenWidth; ++x) {
            buffer[(y * screenWidth) + x].c = c.c;
            depthBuffer[(y * screenWidth) + x] = ~0;
        }
    }
}

void SoftwareRasteriser::SwapBuffers() {
    PresentBuffer(buffers[currentDrawBuffer]);
    currentDrawBuffer = !currentDrawBuffer;
}

void SoftwareRasteriser::Draw(const std::vector<Vertex>& v, PrimitiveType t, const Matrix4 &transform, Texture *texture) {
    Draw(v.data(), v.size(), t, transform, texture);
}

void SoftwareRasteriser::Draw(const Vertex* data, unsigned n, PrimitiveType t, const Matrix4 &transform, Texture *texture) {
    auto mvp = viewProjMatrix * transform;

    switch (t) {
        case POINT_LIST: {
            for (uint i = 0; i < n; ++i) {
                DrawPoint(data[i], mvp);
            }
            break;
        }
        case LINE_LIST: {
            for (uint i = 0; i < n; i += 2) DrawLine(data[i], data[i+1], mvp, texture);
            break;
        }
        case TRIANGLE_LIST: {
            for (uint i = 0; i < n; i += 3) {
                DrawTri(data[i], data[i+1], data[i+2], mvp, texture);
            }
            break;
        }
        case TRIANGLE_STRIP: {
            for (uint i = 2; i < n; i += 1) {
                DrawTri(data[i-2], data[i-1], data[i], mvp, texture);
            }
            break;
        }
        case TRIANGLE_FAN: {
            for (uint i = 2; i < n; i += 1) {
                DrawTri(data[0], data[i-1], data[i], mvp, texture);
            }
            break;
        }
    }
}

void SoftwareRasteriser::DrawPoint(const Vertex &v, Matrix4 &transform) {
    auto screenPos = portMatrix * (transform * v.pos).DivideByW();
    if (!DepthTest(screenPos.x, screenPos.y, screenPos.z)) return;
    DrawPixel(screenPos.x, screenPos.y, v.col);
}

void SoftwareRasteriser::DrawLine(Vertex v0, Vertex v1, const Matrix4 &transform, Texture *texture) {
    v0.pos = transform * v0.pos;
    v1.pos = transform * v1.pos;

    // TODO: fix clipping
    /*
    for(auto bit = 0; bit < 6; bit++) {
        auto a0 = Outcode(v0);
        auto a1 = Outcode(v1);
        if(a0 == 0 && a1 == 0) break;

        auto mask = 0b1 << bit;
        if ((a0 & mask) & (a1 & mask)) {
            std::cout << "failed on " << mask << std::endl;
            return;
        }
        if ((a0 & mask) == (a1 & mask)) continue;

        auto ratio = ClipEdge(v0, v1, mask);
        if (a0 | mask) {
            //TODO: put this interpolation in Vertex class?
            tex0 = Vector3::Lerp(tex0, tex1, ratio);
            col0 = Colour::Lerp(ratio, col0, col1);
            v0 = Vector4::Lerp(v0, v1, ratio);
        } else {
            tex1 = Vector3::Lerp(tex0, tex1, ratio);
            col1 = Colour::Lerp(ratio, col0, col1);
            v1 = Vector4::Lerp(v0, v1, ratio);
        }
    }
     */

    v0.pos = portMatrix * v0.pos.DivideByW();
    v1.pos = portMatrix * v1.pos.DivideByW();

    Bresenham(v0.pos, v1.pos, [this, v0, v1](int x, int y, float t) {
        Colour col = v0.col;
        switch (colourInterpolator) {
            case LINEAR: col = Colour::Lerp(t, v0.col, v1.col); break;
            case GAMMA: col = Colour::GammaInterp(t, v0.col, v1.col); break;
        }
        auto depth = v0.pos.z * t + v1.pos.z * (1.0f - t);
        if (DepthTest(x, y, depth)) DrawPixel(x, y, col);
    });
}

void SoftwareRasteriser::DrawTri(Vertex v0, Vertex v1, Vertex v2, const Matrix4 &transform, Texture *texture ) {
    v0.pos = transform * v0.pos;
    v1.pos = transform * v1.pos;
    v2.pos = transform * v2.pos;

    auto t0 = Vector3(v0.tex.x, v0.tex.y, 1.0) / v0.pos.w;
    auto t1 = Vector3(v1.tex.x, v1.tex.y, 1.0) / v1.pos.w;
    auto t2 = Vector3(v2.tex.x, v2.tex.y, 1.0) / v2.pos.w;

    // TODO: clipping

    v0.pos = portMatrix * v0.pos.DivideByW();
    v1.pos = portMatrix * v1.pos.DivideByW();
    v2.pos = portMatrix * v2.pos.DivideByW();

    auto minX = (int) std::min(std::min(v0.pos.x, v1.pos.x), v2.pos.x);
    auto minY = (int) std::min(std::min(v0.pos.y, v1.pos.y), v2.pos.y);
    auto maxX = (int) std::max(std::max(v0.pos.x, v1.pos.x), v2.pos.x);
    auto maxY = (int) std::max(std::max(v0.pos.y, v1.pos.y), v2.pos.y);

    minX = clamp(minX, 0, (int) screenWidth);
    minY = clamp(minY, 0, (int) screenHeight);
    maxX = clamp(maxX, 0, (int) screenWidth);
    maxY = clamp(maxY, 0, (int) screenHeight);

    auto area = TriArea2D(v0.pos, v1.pos, v2.pos);
    if (area < 0.0) return;

    for (auto x = minX; x < maxX; x++) {
        for (auto y = minY; y < maxY; y++) {
            auto v = Vector4(x, y, 0.0, 0.0);
            auto areaAB = TriArea2D(v0.pos, v1.pos, v) / area;
            auto areaBC = TriArea2D(v1.pos, v2.pos, v) / area;
            auto areaCA = TriArea2D(v2.pos, v0.pos, v) / area;
            if (areaAB < 0.0 || areaBC < 0.0 || areaCA < 0.0) continue;

            auto depth = areaBC * v0.pos.z + areaCA * v1.pos.z + areaAB * v2.pos.z;
            if (!DepthTest(x, y, depth)) continue;

            if (texture != nullptr) {
                auto subTex = t0 * areaBC + t1 * areaCA + t2 * areaAB;
                subTex.x /= subTex.z;
                subTex.y /= subTex.z;
                DrawPixel(x, y, texture->NearestTexSample(subTex, 0));
            } else {
                Colour col;
                switch (colourInterpolator) {
                    case LINEAR: col = Colour::Lerp(areaBC, areaCA, areaAB, v0.col, v1.col, v2.col); break;
                    case GAMMA: col = Colour::GammaInterp(areaBC, areaCA, areaAB, v0.col, v1.col, v2.col); break;
                }

                DrawPixel(x, y, col);
            }
        }
    }
}

void SoftwareRasteriser::Bresenham(
    const Vector4 &v0,
    const Vector4 &v1,
    std::function<void(int, int, float)> draw
) {
    auto dir = v1 - v0;
    auto xDir = (dir.x < 0.0f) ? -1 : 1;
    auto yDir = (dir.y < 0.0f) ? -1 : 1;

    auto x = (int) v0.x;
    auto y = (int) v0.y;

    int *target = nullptr, *scan = nullptr;
    auto targetVal = 0, scanVal = 0;
    auto slope = 0.0;
    auto range = 0;

    if (abs(dir.y) > abs(dir.x)) {
        slope = dir.x / dir.y;
        range = (int) abs(dir.y);
        target = &x;
        scan = &y;
        scanVal = yDir;
        targetVal = xDir;
    } else {
        slope = dir.y / dir.x;
        range = (int) abs(dir.x);
        target = &y;
        scan = &x;
        scanVal = xDir;
        targetVal = yDir;
    }

    auto absSlope = abs(slope);
    auto error = 0.0f;

    for (auto i = 0; i < range; ++i) {
        draw(x, y, (float) i / (float) range);
        error += absSlope;
        if (error > 0.5f) {
            error -= 1.0f;
            (*target) += targetVal;
        }
        (*scan) += scanVal;
    }
}

float SoftwareRasteriser::ClipEdge(Vector4 &v0, Vector4 &v1, unsigned axis) {
    float ratio;
    switch (axis) {
        case 0b000001: ratio = (-v0.w - v0.x) / ((v1.x - v0.x) + v1.w - v0.w); break;
        case 0b000010: ratio = ( v0.w - v0.x) / ((v1.x - v0.x) - v1.w + v0.w); break;
        case 0b000100: ratio = (-v0.w - v0.y) / ((v1.y - v0.y) + v1.w - v0.w); break;
        case 0b001000: ratio = ( v0.w - v0.y) / ((v1.y - v0.y) - v1.w + v0.w); break;
        case 0b010000: ratio = (-v0.w - v0.z) / ((v1.z - v0.z) + v1.w - v0.w); break;
        case 0b100000: ratio = ( v0.w - v0.z) / ((v1.z - v0.z) - v1.w + v0.w); break;
    }
    return std::min(ratio, 1.0f);
}

unsigned SoftwareRasteriser::Outcode(Vector4 &v) {
    auto a = 0;
    if (v.x < -v.w) a |= 0b000001;
    if (v.x >  v.w) a |= 0b000010;
    if (v.y < -v.w) a |= 0b000100;
    if (v.y >  v.w) a |= 0b001000;
    /*
    if (v.z < -v.w) a |= 0b010000;
    if (v.z >  v.w) a |= 0b100000;
     */
    return a;
}
