/******************************************************************************
Class:Colour
Implements:
Author:Rich Davison	<richard.davison4@newcastle.ac.uk>
Description:Simple class to hold colours made up of 4 bytes.

You might be wondering why in the class, the bytes aren't arranged in rgb order

This is due to how Win32 internally stores its bitmaps - in bgra. In order to 
speed up mem copies to the window buffer, we keep our colours in the same order

-_-_-_-_-_-_-_,------,   
_-_-_-_-_-_-_-|   /\_/\   NYANYANYAN
-_-_-_-_-_-_-~|__( ^ .^) /
_-_-_-_-_-_-_-""  ""   

*//////////////////////////////////////////////////////////////////////////////


#pragma once

#include <cmath>

enum BlendMode {
    NORMAL, ADD, MULTIPLY, /* ... */
};

struct Colour {
    union {
        struct {
            unsigned char b;
            unsigned char g;
            unsigned char r;
            unsigned char a;
        };
        unsigned int c;
    };

    explicit Colour(unsigned char r = 0, unsigned char g = 0, unsigned char b = 0, unsigned char a = 255) {
        this->r = r;
        this->g = g;
        this->b = b;
        this->a = a;
    }

    static unsigned char GammaInterp(float t, unsigned char a, unsigned char b) {
        auto aLin = std::pow(a / 255.0, 2.2);
        auto bLin = std::pow(b / 255.0, 2.2);
        auto v = aLin + t * (bLin - aLin);
        return (unsigned char) (std::pow(v, 1.0 / 2.2) * 255.0);
    }

    static unsigned char GammaInterp(float ta, float tb, float tc, unsigned char a, unsigned char b, unsigned char c) {
        auto aLin = std::pow(a / 255.0, 2.2);
        auto bLin = std::pow(b / 255.0, 2.2);
        auto cLin = std::pow(c / 255.0, 2.2);
        auto v = aLin * ta + bLin * tb + cLin * tc;
        return (unsigned char) (std::pow(v, 1.0 / 2.2) * 255.0);
    }

    static Colour GammaInterp(float t, const Colour &a, const Colour &b) {
        return Colour(
                GammaInterp(t, a.r, b.r),
                GammaInterp(t, a.g, b.g),
                GammaInterp(t, a.b, b.b),
                Lerp(t, a.a, b.a));
    }

    static Colour GammaInterp(float ta, float tb, float tc, const Colour &a, const Colour &b, const Colour &c) {
        return Colour(
                GammaInterp(ta, tb, tc, a.r, b.r, c.r),
                GammaInterp(ta, tb, tc, a.g, b.g, c.g),
                GammaInterp(ta, tb, tc, a.b, b.b, c.b),
                Lerp(ta, tb, tc, a.a, b.a, c.a));
    }

    static unsigned char Lerp(float t, unsigned char a, unsigned char b) {
        return (unsigned char) ((float)a * t + (float)b * (1.0f - t));
    }

    static unsigned char Lerp(float ta, float tb, float tc, unsigned char a, unsigned char b, unsigned char c) {
        return (unsigned char) (a * ta + b * tb + c * tc);
    }

    static Colour Lerp(float t, const Colour &a, const Colour &b) {
        return Colour(Lerp(t, a.r, b.r), Lerp(t, a.g, b.g), Lerp(t, a.b, b.b), Lerp(t, a.a, b.a));
    }

    static Colour Lerp(float ta, float tb, float tc, const Colour &a, const Colour &b, const Colour &c) {
        return Colour(
                Lerp(ta, tb, tc, a.r, b.r, c.r),
                Lerp(ta, tb, tc, a.g, b.g, c.g),
                Lerp(ta, tb, tc, a.b, b.b, c.b),
                Lerp(ta, tb, tc, a.a, b.a, c.a));
    }

    inline Colour operator*(const float factor) const {
        return Colour((unsigned char) (float) (r * factor),
                      (unsigned char) (float) (g * factor),
                      (unsigned char) (float) (b * factor),
                      (unsigned char) (float) (a * factor));
    }

    inline Colour operator*(const Colour &o) const {
        return Colour(r * o.r, g * o.g, b * o.b, a * o.a);
    }

    inline Colour operator+(const Colour &add) const {
        return Colour(r + add.r, g + add.g, b + add.b, a + add.a);
    }

    inline void operator+=(const Colour &add) {
        r += add.r;
        g += add.g;
        b += add.b;
        a += add.a;
    }

    inline Colour operator-(const Colour &sub) const {
        return Colour(r - sub.r, g - sub.g, b - sub.b, a - sub.a);
    }

    static Colour White;
};

