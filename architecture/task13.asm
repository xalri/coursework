    jmp start

; Displays a null terminated string at [AL]
display:
    org 10
    mov BL, C0
    disp_loop:
        mov CL, [AL]
        cmp CL, 00
        jz disp_end
        mov [BL], CL
        inc AL
        inc BL
        jmp disp_loop
    disp_end:
        ret

start:
    mov BL, 40
loop:
    in 00
    mov [BL], AL
    inc BL
    cmp AL, 0D
    jnz loop

    mov AL, 40
    call 10
    end
