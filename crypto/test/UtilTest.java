package uk.xa1.crypto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UtilTest {
	@Test void isAlphabetic() {
		Assertions.assertTrue(Util.isAlphabetic('a'));
		Assertions.assertTrue(Util.isAlphabetic('z'));
		Assertions.assertTrue(Util.isAlphabetic('A'));
		Assertions.assertTrue(Util.isAlphabetic('Z'));

		Assertions.assertFalse(Util.isAlphabetic('`'));
		Assertions.assertFalse(Util.isAlphabetic('{'));
		Assertions.assertFalse(Util.isAlphabetic('@'));
		Assertions.assertFalse(Util.isAlphabetic('['));
	}

	@Test void wrap() {
		Assertions.assertEquals(Util.wrap('a', 1), 'b');
		Assertions.assertEquals(Util.wrap('A', 1), 'B');
		Assertions.assertEquals(Util.wrap('z', 1), 'a');
		Assertions.assertEquals(Util.wrap('Z', 1), 'A');
	}
}