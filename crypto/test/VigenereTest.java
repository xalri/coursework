package uk.xa1.crypto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.*;

class VigenereTest {
	@Test void vigenereDistribution() {
		try {
			File file = new File("data/ex2_crypto");
			CharacterFreq.Profile p = CharacterFreq.Profile.analyseFile(file);
			//Util.displayMap(p.charRelative);
		} catch (Exception ignore) {}
	}

	@Test void encode() {
		InputStream stream = new ByteArrayInputStream("newcastleuniversity".getBytes());
		Assertions.assertEquals(
				"aghpcdgnphptigcfkel",
				Util.readToString(Vigenere.Stream.encode(stream, "ncl")));
	}

	@Test void decode() {
		InputStream stream = new ByteArrayInputStream("aghpcdgnphptigcfkel".getBytes());
		Assertions.assertEquals(
				  "newcastleuniversity",
				  Util.readToString(Vigenere.Stream.decode(stream, "ncl")));
	}

	/** Encode some plain text with a vigenere cipher and check that the
	 *  profiler finds the correct password */
	@Test void analyse() {
		try {
			String password = "passibleblueringman";
			Vigenere.Stream encode = Vigenere.Stream.encode(
					  new FileInputStream(new File("data/ex1_plain")),
					  password);

			Vigenere.Solver profile = Vigenere.Solver.fromStream(encode);
			Assertions.assertEquals(password, profile.solve());
		} catch (Exception e) { e.printStackTrace(); }
	}
}