package uk.xa1.crypto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Arrays;
import java.util.Map;

class CharacterFreqTest {
	private static File testFile = new File("data/long");
	private static String testString = "A luminous mental state is one that you have and " +
			  "know that you have.  It could be an emotion, a belief " +
			  "or alief, a disposition, a quale, a memory - anything " +
			  "that might happen or be stored in your brain.  What's " +
			  "going on in your head?  What you come up with when you " +
			  "ponder that question - assuming, nontrivially, that you " +
			  "are accurate - is what's luminous to you.";

	/** Test that character counting is correct */
	@Test void count() {
		Map<Character, Integer> freq = CharacterFreq.Profile.analyse(testString).charFreq;

		Assertions.assertEquals(30, (int) freq.get('a'));
		Assertions.assertEquals(22, (int) freq.get('e'));
		Assertions.assertEquals(16, (int) freq.get('h'));
		Assertions.assertEquals(5, (int) freq.get('p'));
		Assertions.assertEquals(6, (int) freq.get('w'));
	}

	/** Tests that relative character frequencies sum to 1.0 */
	@Test void relativeFreq() {
		Map<?, Double> relative = CharacterFreq.Profile.analyse(testString).charRelative;
		Double sum = relative.values().stream().mapToDouble(i->i).sum();
		Assertions.assertEquals(1.0, sum, 0.00001);
	}

	/** Run known english text through the matcher, test that the
	 * mapping is a -> a etc. */
	@Test void matchEnglish() {
		try {
			CharacterFreq.Profile profile = CharacterFreq.Profile.analyseFile(testFile);
			Substitution.Solver solver = new Substitution.Solver();
			solver.applyProfile(profile);

			// Use the general substitution solver
			Map<Character, Character> match = solver.match();
			int count = 0;
			for(char c = 'a'; c <= 'z'; c++) if (match.get(c) == c) count++;

			// Assert that at least 20 characters are correct
			Assertions.assertTrue(count > 20);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	/** Test the weighted maximal bipartite matching algorithm */
	@Test void matching() {
		double[][] w = new double[][] {
				{2, 10, 8, 3},
				{5, 1, 4, 9},
				{3, 4, 5, 6},
				{8, 1, 9, 7}
		};
		Assertions.assertTrue(Arrays.equals(new Substitution.Hungarian(w).solve(), new int[]{1, 3, 2, 0}));
	}

	/** Test the unweighted maximal bipartite matching algorithm */
	@Test void unweightedMatching() {
		boolean[][] m = new boolean[][]{
				{ true,  false, false, false, true,  true  },
				{ true,  true,  false, true,  false, false },
				{ false, false, false, true,  false, true  },
				{ true,  false, false, false, false, true  },
				{ true,  true,  true,  false, false, false },
				{ false, false, true,  false, false, false },
		};
		int[] match = Substitution.Hungarian.unweighted(6, (x, y) -> m[x][y]);
		Assertions.assertTrue(Arrays.equals(match, new int[]{4, 3, 5, 0, 1, 2}));
	}
}
