package uk.xa1.crypto;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/** En/decoding and breaking of vigenre ciphertext. */
class Vigenere {

	/** Finds the password and decrypts a vigenere encoded file */
	static class Solve {
		public static void main(String[] args) {
			if (args.length < 1) System.err.println("Missing argument");
			try {
				File file = new File(args[0]);

				Vigenere.Solver solver = Vigenere.Solver.fromStream(new BufferedInputStream(
						  new FileInputStream(file)));
				String password = solver.solve();
				System.out.println("password: " + password);

				System.out.println("decoded: ");
				Vigenere.Stream decode = Vigenere.Stream.decode(
						  new FileInputStream(file),
						  password);

				Util.readToEnd(decode, System.out::print);
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}

	/** Encrypts a file with a vigenere cipher */
	static class Encrypt {
		public static void main(String[] args) {
			if (args.length < 2) System.err.println("Missing argument");
			try {
				Util.readToEnd(Stream.encode(new FileInputStream(new File(args[0])), args[1]),
						  System.out::print);
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}

	/** Decrypts a file encoded with a vigenere cipher */
	static class Decrypt {
		public static void main(String[] args) {
			if (args.length < 3) System.err.println("Missing argument");
			try {
				Util.readToEnd(Stream.decode(new FileInputStream(new File(args[0])), args[1]),
						  System.out::print);
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}

	/** Wraps a stream to encode/decode data using a vigenere cipher */
	static class Stream extends InputStream {
		InputStream source;
		int[] pw;
		int offset = 0;

		private Stream() {}

		/** Create a new encoder, fails if the password contains
		 * non-alphabetic characters */
		static Stream encode(InputStream source, String password) {
			Stream s = new Stream();
			s.source = source;
			s.pw = passwordOffset(password);
			return s;
		}

		/** Create a new decoder, fails if the password contains
		 * non-alphabetic characters */
		static Stream decode(InputStream source, String password) {
			Stream s = encode(source, password);
			for(int i = 0; i < s.pw.length; i++) {
				s.pw[i] = (26 - s.pw[i]) % 26;
			}
			return s;
		}

		@Override public int read() throws IOException {
			int b = source.read();
			if(b == -1) return -1;
			char c = (char) b;

			if (Util.isAlphabetic(c)) {
				c = Util.wrap(c, pw[offset]);
				offset += 1;
				if(offset == pw.length) offset = 0;
			}
			return (int) c;
		}
	}

	/** Takes vigenere encoded ciphertext and attempts to find the password */
	static class Solver {
		List<Split> splits = IntStream
				  .range(1, 100)
				  .boxed()
				  .map(Split::new)
				  .collect(Collectors.toList());

		/** Analyse data from a stream */
		static Solver fromStream(InputStream stream) throws IOException {
			Solver s = new Solver();
			Util.readToEnd(stream, s::consume);
			s.splits.forEach(Solver.Split::normalize);
			return s;
		}

		/** Consume the next character */
		void consume(char c) {
			splits.forEach(s -> s.consume(c));
		}

		/** Find the password */
		String solve() {
			return splits.stream()
				  .filter(x -> x.indexOfCoincidence() > 0.062)
				  .findFirst()
				  .map(Split::solve)
				  .orElse(null);
		}

		/** Frequency profiles for every n characters */
		static class Split {
			int n;
			int offset = 0;
			List<CharacterFreq.Profile> data = new ArrayList<>();

			Split(int n) {
				for(int i = 0; i < n; i++) data.add(new CharacterFreq.Profile());
				this.n = n;
			}

			/** Consume the next character */
			void consume(char c) {
				if(data.get(offset).consume(c) && ++offset == n) offset = 0;
			}

			void normalize() {
				data.forEach(CharacterFreq.Profile::normalize);
			}

			/** Get the average index of coincidence */
			double indexOfCoincidence() {
				return data.stream()
						.map(CharacterFreq.Profile::indexOfCoincidence)
						.mapToDouble(x -> x)
						.average()
						.getAsDouble();
			}

			/** Get the password assuming it's length is n */
			String solve() {
				StringBuilder sb = new StringBuilder();
				for(CharacterFreq.Profile p : data) {
					Substitution.Solver solver = new Substitution.Solver();
					solver.applyProfile(p);
					int i = (26 - solver.matchShift()) % 26;
					sb.append((char)(i + 97));
				}
				return sb.toString();
			}
		}
	}

	private static int[] passwordOffset(String s) {
		int[] pw = new int[s.length()];
		int i = 0;
		for (char c : s.toCharArray()) {
			if (!Util.isAlphabetic(c)) throw new RuntimeException("Non-alphabetic password");
			c = Character.toUpperCase(c);
			pw[i++] = c - 65;
		}
		return pw;
	}
}
