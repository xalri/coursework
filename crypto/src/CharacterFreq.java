package uk.xa1.crypto;

import java.io.*;
import java.util.*;

/** Things relating to character frequency analysis */
class CharacterFreq {
	/** Takes a file path as a parameter and displays the relative character
	 * frequencies for that file */
	static class Analyse {
		public static void main(String[] args) {
			try {
				Profile p = Profile.analyse(new FileInputStream(new File(args[0])));
				Util.displayMap(p.charRelative);
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}

	/** A character frequency profile for some text. */
	static class Profile {
		Map<Character, Integer> charFreq = new HashMap<>();
		Map<String, Integer> bigramFreq = new HashMap<>();
		Map<Character, Double> charRelative = new HashMap<>();
		Map<String, Double> bigramRelative = new HashMap<>();
		private Character prev = null;

		Profile() {
			for (int i = 97; i < 123; i++) charFreq.put((char) i, 0);
		}

		/** Analyse data from a stream */
		static Profile analyse(InputStream stream) throws IOException {
			Profile data = new Profile();
			Util.readToEnd(stream, data::consume);
			data.normalize();
			return data;
		}

		/** Consume the next character, returns false if it wasn't used. */
		boolean consume(char c) {
			if(!Util.isAlphabetic(c)) {
				if(c != '\'') prev = null;
				return false;
			}
			c = Character.toLowerCase(c);

			this.charFreq.put(c, this.charFreq.get(c) + 1);
			if(prev != null) {
				String s = "" + prev + c;
				this.bigramFreq.put(s, this.bigramFreq.getOrDefault(s, 0) + 1);
			}
			prev = c;
			return true;
		}

		/** Analyse the character frequency of a string */
		static Profile analyse(String in) {
			try {
				return analyse(new ByteArrayInputStream(in.getBytes()));
			} catch(IOException e) { return null; }
		}

		/** Analyse the character frequency of a text file */
		static Profile analyseFile(File path) throws IOException {
			return analyse(new BufferedInputStream(new FileInputStream(path)));
		}

		/** Calculate the index of coincidence for the profile. */
		double indexOfCoincidence() {
			return charRelative.values().stream().mapToDouble(x -> x * x).sum();
		}

		/** Populate relative frequency maps. */
		void normalize() {
			charRelative = Util.relativeFreq(charFreq);
			bigramRelative = Util.relativeFreq(bigramFreq);
		}
	}

	/** Character frequency data; useful for cryptanalysis of simple ciphers. */
	static class Data {
		/** English character frequency data, generated using the Open
		 * American National Corpus */
		final static Data ENGLISH = new Data();

		/** Get the average relative frequency of an alphabetic character in this data set */
		Double charAvr(char c) { return charAvr.get(c); }

		/** Get the variance in relative frequency of an alphabetic character in this data set */
		Double charVar(char c) { return charVar.get(c); }

		/** Get the average relative frequency of a bigram in this data set */
		Double bigramAvr(String c) { return bigramAvr.get(c); }

		/** Get the variance in relative frequency of a bigram in this data set */
		Double bigramVar(String c) { return bigramVar.get(c); }

		/** A list of all bigrams in descending frequency order */
		List<Util.Pair<Double, String>> sortedBigrams() { return bigramSorted; }

		/** Calculate character distributions from a corpus of `.txt` files. */
		static Data fromCorpus(File directory) throws IOException {
			List<Profile> profiles = new ArrayList<>();

			Util.walkDir(directory, file -> {
				if (!file.isFile() || !file.getName().endsWith(".txt")) return;
				try {
					profiles.add(Profile.analyseFile(file));
				} catch(Exception e) {
					System.out.println("Error reading file "+file+": " + e);
					e.printStackTrace();
				}
			});

			return Data.fromProfiles(profiles);
		}

		/** Calculate character distributions for a given set of frequency profiles */
		static Data fromProfiles(Collection<Profile> data) {
			Data d = new Data();

			System.out.println(data.size() + " entries");
			Map<Character, Double> charRelativeSum = new HashMap<>();
			Map<Character, Double> charRelativeSqSum = new HashMap<>();
			Map<String, Double> bigramRelativeSum = new HashMap<>();
			Map<String, Double> bigramRelativeSqSum = new HashMap<>();
			Map<String, Integer> bigramCount = new HashMap<>();

			for(char c = 'a'; c <= 'z'; c++) {
				charRelativeSum.put(c, 0.0);
				charRelativeSqSum.put(c, 0.0);
			}

			for(Profile p : data) {
				p.normalize();
				for(Map.Entry<Character, Double> e : p.charRelative.entrySet()){
					charRelativeSum.put(e.getKey(), charRelativeSum.get(e.getKey()) + e.getValue());
					charRelativeSqSum.put(e.getKey(), charRelativeSqSum.get(e.getKey()) + (e.getValue() * e.getValue()));
				}
				for(Map.Entry<String, Double> e : p.bigramRelative.entrySet()){
					if(!bigramCount.containsKey(e.getKey())) {
						bigramRelativeSum.put(e.getKey(), 0.0);
						bigramRelativeSqSum.put(e.getKey(), 0.0);
						bigramCount.put(e.getKey(), 0);
					}
					bigramCount.put(e.getKey(), bigramCount.get(e.getKey()) + 1);
					bigramRelativeSum.put(e.getKey(), bigramRelativeSum.get(e.getKey()) + e.getValue());
					bigramRelativeSqSum.put(e.getKey(), bigramRelativeSqSum.get(e.getKey()) + (e.getValue() * e.getValue()));
				}
			}
			System.out.println(bigramRelativeSum.size() + " bigrams");

			for(Map.Entry<Character, Double> e : charRelativeSum.entrySet()) {
				char c = e.getKey();
				double avr = e.getValue() / data.size();
				double sqarv = charRelativeSqSum.get(c) / data.size();
				double var = sqarv - (avr * avr);
				d.charVar.put(c, var);
				d.charAvr.put(c, avr);
			}

			for(Map.Entry<String, Double> e : bigramRelativeSum.entrySet()) {
				String bigram = e.getKey();
				double avr = e.getValue() / data.size();
				double sqAvr = bigramRelativeSqSum.get(bigram) / data.size();
				double var = sqAvr - (avr * avr);
				d.bigramAvr.put(bigram, avr);
				d.bigramVar.put(bigram, var);
				d.bigramSorted.add(new Util.Pair<>(avr, bigram));
			}
			d.bigramSorted.sort(Comparator.reverseOrder());

			return d;
		}

		static {
			// Data generated by `Analyse` using the Open American National Corpus
			// http://www.anc.org/data/oanc/

			addChar('a', 0.08140447068190719, 4.227803623126341E-5);
			addChar('b', 0.015494630011772712, 1.1006349735789004E-5);
			addChar('c', 0.0296582307224584, 7.588086025964104E-5);
			addChar('d', 0.03630461070409571, 2.7681177572486115E-5);
			addChar('e', 0.1189111166605066, 6.862206574499534E-5);
			addChar('f', 0.019926371740705592, 1.7257451701462002E-5);
			addChar('g', 0.02043928281570668, 1.3882973613395851E-5);
			addChar('h', 0.05324552463194776, 1.9642194212869885E-4);
			addChar('i', 0.07351226178333606, 6.241380341290206E-5);
			addChar('j', 0.0021258865195926077, 1.912081033425628E-6);
			addChar('k', 0.009076379776926758, 2.209853425150014E-5);
			addChar('l', 0.04151885986253877, 3.275728726494622E-5);
			addChar('m', 0.02522040844146509, 2.0460009304353868E-5);
			addChar('n', 0.06982191343088298, 4.3437634644677425E-5);
			addChar('o', 0.07685089363425308, 5.414536859378247E-5);
			addChar('p', 0.020127738243309237, 3.562132911188884E-5);
			addChar('q', 9.794524270199491E-4, 8.315575657698997E-7);
			addChar('r', 0.057783257330703164, 8.760889903714972E-5);
			addChar('s', 0.06515113925118023, 8.828812336194019E-5);
			addChar('t', 0.09459306691649395, 8.431773073910506E-5);
			addChar('u', 0.032634239031579776, 9.23561465651548E-5);
			addChar('v', 0.010436842913054594, 6.570539494950219E-6);
			addChar('w', 0.019574537703037714, 3.8633850428501E-5);
			addChar('x', 0.0020466317248502624, 2.1460276879192743E-6);
			addChar('y', 0.022176086087114983, 6.338932207996205E-5);
			addChar('z', 9.861669535611627E-4, 9.445148589280615E-7);

			addBigram("th", 0.035290237253791816, 6.745967788416391E-5);
			addBigram("he", 0.027023649848563137, 3.9518431766025815E-5);
			addBigram("in", 0.024191032832378404, 1.921991292776139E-5);
			addBigram("an", 0.01944269005852811, 1.80174673002708E-5);
			addBigram("er", 0.018574151938097975, 1.5050573002513073E-5);
			addBigram("re", 0.018490577736845263, 1.4831827768869326E-5);
			addBigram("at", 0.01598671755007423, 1.4213133657017861E-5);
			addBigram("on", 0.015830815196997708, 1.8439908990177105E-5);
			addBigram("nd", 0.013111591553387474, 1.8525286598211258E-5);
			addBigram("es", 0.013111324128812177, 1.525524352273376E-5);
			addBigram("en", 0.013011409357035179, 1.2826805320388651E-5);
			addBigram("ha", 0.012569321619657401, 3.167339905170606E-5);
			addBigram("or", 0.012156836320171352, 1.1036560529466478E-5);
			addBigram("ou", 0.012121585625198066, 4.5491246442903453E-5);
			addBigram("it", 0.012076031418267495, 1.3553220200988978E-5);
			addBigram("to", 0.011105374563087038, 8.507442205211947E-6);
			addBigram("ti", 0.010958941856573432, 2.412068233671852E-5);
			addBigram("nt", 0.010953234649196569, 9.420817061580084E-6);
			addBigram("te", 0.010764832406362907, 1.4226914806000167E-5);
			addBigram("st", 0.01065393915150048, 9.025487125247273E-6);
			addBigram("al", 0.010451430600447306, 8.471558003144783E-6);
			addBigram("ng", 0.010408065488710433, 9.15575185536573E-6);
			addBigram("ar", 0.009977602233375463, 9.571310733143094E-6);
			addBigram("is", 0.009594453973208975, 1.3251892168815563E-5);
			addBigram("ed", 0.009420578205256078, 1.4345682531006077E-5);
			addBigram("ea", 0.00895201392889447, 1.8916982568330755E-5);
			addBigram("ve", 0.00888108799390093, 8.120257827313966E-6);
			addBigram("se", 0.008736126800317214, 7.623664773419571E-6);
			addBigram("as", 0.008237494925866522, 7.374900399310762E-6);
			addBigram("of", 0.008220991472091845, 5.933432464193036E-6);
			addBigram("le", 0.008008728899669311, 6.887567851698311E-6);
			addBigram("me", 0.007963325359694464, 6.147953929509691E-6);
			addBigram("ll", 0.00755157034501028, 9.746262577192389E-6);
			addBigram("hi", 0.0072648614513279065, 8.46487184847197E-6);
			addBigram("ne", 0.007132469510920772, 6.5903832936715174E-6);
			addBigram("li", 0.006997252466717768, 6.795132209775481E-6);
			addBigram("co", 0.006773267958477643, 7.6184636050038404E-6);
			addBigram("ri", 0.006637703248456715, 6.433975691422564E-6);
			addBigram("de", 0.006389935289850489, 8.475475216185131E-6);
			addBigram("ro", 0.0063683329122162765, 7.072474454545973E-6);
			addBigram("ic", 0.006205969427542054, 8.926064246669971E-6);
			addBigram("no", 0.005967492413315731, 1.1715920297352043E-5);
			addBigram("io", 0.00583175817001268, 1.300132550038317E-5);
			addBigram("ca", 0.0058193467917709045, 5.595982179457668E-6);
			addBigram("ra", 0.005654137506161977, 8.387362158491787E-6);
			addBigram("el", 0.005564381233842369, 4.8807680590168596E-6);
			addBigram("om", 0.005421426214134925, 4.364357724194329E-6);
			addBigram("be", 0.005323026661497364, 3.919002503444112E-6);
			addBigram("ut", 0.0053210371675452405, 5.5637276804086945E-6);
			addBigram("ce", 0.005313196207132206, 6.819685716197874E-6);
			addBigram("us", 0.005236131339836454, 4.622092465580241E-6);
			addBigram("ts", 0.005228012396675142, 5.98444253227867E-6);
			addBigram("ho", 0.005154235694486821, 4.942335364223518E-6);
			addBigram("we", 0.005003906615602764, 1.3149386975735102E-5);
			addBigram("ur", 0.0049281240491357075, 5.09215281666865E-6);
			addBigram("si", 0.0049150721661981945, 5.863254130388409E-6);
			addBigram("ch", 0.004884192226946365, 5.7455288980501425E-6);
			addBigram("ma", 0.004819223830719124, 5.473232758415565E-6);
			addBigram("ta", 0.004782082271193102, 5.039772533279088E-6);
			addBigram("la", 0.0047800185155111585, 5.552817486837396E-6);
			addBigram("ly", 0.0047692890641926455, 3.1832917994031996E-6);
			addBigram("ot", 0.004717259313115157, 3.969840500964111E-6);
			addBigram("ow", 0.0046135152526300145, 1.1021251533778664E-5);
			addBigram("so", 0.004599393861836745, 6.307115751349741E-6);
			addBigram("yo", 0.004533793119785616, 2.6089826312105776E-5);
			addBigram("et", 0.004529924201236193, 3.726511523134591E-6);
			addBigram("ns", 0.004489332962249865, 6.41849699157509E-6);
			addBigram("di", 0.004396402029075391, 4.653220533396267E-6);
			addBigram("pe", 0.004372257157771916, 3.4551577351579836E-6);
			addBigram("fo", 0.0043594544793103245, 2.7810126019932838E-6);
			addBigram("il", 0.0042976549533747166, 4.9284601062521814E-6);
			addBigram("ec", 0.004220138988533537, 4.08314013371556E-6);
			addBigram("pr", 0.0042024706715131175, 5.426985694905687E-6);
			addBigram("rs", 0.0041404098258730655, 3.6507192326242844E-6);
			addBigram("ge", 0.004091950912488705, 3.6623803330846842E-6);
			addBigram("wa", 0.004060643198457562, 6.094035560722881E-6);
			addBigram("wh", 0.004037540926442514, 4.109107394472314E-6);
			addBigram("ee", 0.003913664732651536, 3.3387886369235954E-6);
			addBigram("ac", 0.003825578096742613, 3.5772159137188072E-6);
			addBigram("un", 0.003663300062890191, 3.1116653160593823E-6);
			addBigram("mo", 0.003608884653171028, 3.3531371131448244E-6);
			addBigram("lo", 0.0035190434040139294, 2.6927582821671416E-6);
			addBigram("do", 0.00351438149761673, 7.588937939566688E-6);
			addBigram("wi", 0.003496074165568671, 2.590247988418724E-6);
			addBigram("tr", 0.0034899958386242743, 3.8219906322253894E-6);
			addBigram("id", 0.003462046914393306, 3.27022321464106E-6);
			addBigram("ss", 0.0034568410381942568, 3.181410620272956E-6);
			addBigram("im", 0.003446545655529414, 2.6189409731998675E-6);
			addBigram("ad", 0.0034319176643807176, 3.4564846150415136E-6);
			addBigram("ol", 0.0034174189476325004, 3.6994211210011464E-6);
			addBigram("ay", 0.0033765321408368086, 3.684824133679217E-6);
			addBigram("ct", 0.003353868651445882, 5.11668041771461E-6);
			addBigram("rt", 0.003351430048191014, 2.9833473571619786E-6);
			addBigram("ke", 0.003328389304219286, 4.637757306275073E-6);
			addBigram("po", 0.0032740501657018734, 4.260354506931675E-6);
			addBigram("ie", 0.0032387573886999664, 3.792933019676268E-6);
			addBigram("uh", 0.0032098997584017568, 3.7284259611122887E-5);
			addBigram("pa", 0.0032087842562001857, 3.751616962765475E-6);
			addBigram("ul", 0.003193623624509281, 2.6700893451331543E-6);
			addBigram("na", 0.0031739427772438217, 4.4034332548445255E-6);

			ENGLISH.bigramSorted.sort(Comparator.reverseOrder());
		}

		private Map<Character, Double> charAvr = new HashMap<>();
		private Map<Character, Double> charVar = new HashMap<>();
		private Map<String, Double> bigramAvr = new HashMap<>();
		private Map<String, Double> bigramVar = new HashMap<>();
		private List<Util.Pair<Double, String>> bigramSorted = new ArrayList<>();

		private static void addChar(char c, Double avr, Double var){
			ENGLISH.charAvr.put(c, avr);
			ENGLISH.charVar.put(c, var);
		}

		private static void addBigram(String s, Double avr, Double var){
			ENGLISH.bigramAvr.put(s, avr);
			ENGLISH.bigramVar.put(s, var);
			ENGLISH.bigramSorted.add(new Util.Pair<>(avr, s));
		}

		/** Analyses a corpus of `.txt` files to calculate the mean and variance
		 *  of the relative frequencies of characters and bigrams.
		 *  The results are stored in `Data` and used by `CharacterFreq` to
		 *  decode text encoded by substitution ciphers. */
		static class Analyse {
			public static void main(String[] args) {
				File f = new File(args[0]);
				System.out.println("Running corpus analysis on " + f.getPath());
				try {
					Data stats = Data.fromCorpus(f);

					for (char c = 'a'; c <= 'z'; c++) {
						double mean = stats.charAvr(c);
						double var = stats.charVar(c);
						System.out.println("addChar('" + c + "', " + mean + ", " + var + ");");
					}

					int i = 0;
					for (Util.Pair<Double, String> e : stats.sortedBigrams()) {
						if (++i > 100) break;
						String bigram = e.second;
						double mean = e.first;
						double var = stats.bigramVar(bigram);
						System.out.println("addBigram(\"" + bigram + "\", " + mean + ", " + var + ");");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
