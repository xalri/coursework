package uk.xa1.crypto;

import java.math.BigInteger;

class LinearCongruence {
   /** Solve the linear congruence ax + b = 0 (mod n). If there are multiple solutions the
    * smallest is given. Returns null if there are no solutions. */
   static BigInteger solve(BigInteger a, BigInteger b, BigInteger n) {
      BigInteger d = a.gcd(n);

      // If a and n are co-prime there is a single solution
      if (d.equals(new BigInteger("1")))
         return (n.subtract(b)).multiply(a.modInverse(n)).mod(n);

      // Convert to the form ax = c (mod n)
      BigInteger c = b.negate().mod(n);

      // There are solutions iff d divides c, otherwise return null
      if(!c.remainder(d).equals(new BigInteger("0"))) return null;

      // Calculate the first solution by solving ax/d = c/d (mod N/d).
      return solve(a.divide(d), c.divide(d).negate().mod(n), n.divide(d));
   }

   public static void main(String[] args) {
      // ============== Q2 a =================
      BigInteger a1 = new BigInteger("34325464564574564564768795534569998743457687643234566579654234676796634378768434237897634345765879087764242354365767869780876543424");
      BigInteger b1 = new BigInteger("45292384209127917243621242398573220935835723464332452353464376432246757234546765745246457656354765878442547568543334677652352657235");
      BigInteger n1 = new BigInteger("643808006803554439230129854961492699151386107534013432918073439524138264842370630061369715394739134090922937332590384720397133335969549256322620979036686633213903952966175107096769180017646161851573147596390153");

      System.out.println("Q2 a.");
      printSolution(a1, b1, n1);

      // ============== Q2 b =================
      BigInteger a2 = new BigInteger("34325464564574564564768795534569998743457687643234566579654234676796634378768434237897634345765879087764242354365767869780876543424");
      BigInteger b2 = new BigInteger("24243252873562935279236582385723952735639239823957923562835832582635283562852252525256882909285959238420940257295265329820035324646");
      BigInteger n2 = new BigInteger("342487235325934582350235837853456029394235268328294285895982432387582570234238487625923289526382379523573265963293293839298595072093573204293092705623485273893582930285732889238492377364284728834632342522323422");

      System.out.println("Q2 b.");
      printSolution(a2, b2, n2);
   }

   static void printSolution(BigInteger a, BigInteger b, BigInteger n) {
      // Solve for x then check that the solution is correct.
      BigInteger x = solve(a, b, n);
      System.out.println("x: " + x);
      System.out.println("ax + b (mod N) = " + a.multiply(x).add(b).mod(n));
      System.out.println();
   }
}
