package uk.xa1.crypto;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/** Assorted functions etc. */
class Util {
	/** Read a stream to the end, passing each character to a consumer */
	static void readToEnd(InputStream s, Consumer<Character> consumer) throws IOException {
		int b;
		while ((b = s.read()) != -1) consumer.accept((char) b);
	}

	/** Read a stream to a string */
	static String readToString(InputStream s) {
		StringBuilder sb = new StringBuilder();
		try {
			Util.readToEnd(s, sb::append);
		} catch (Exception ignored) {}

		return sb.toString();
	}

	/** Recursively walk a directory */
	static void walkDir(File p, Consumer<File> fn) {
		WalkDir walk = new WalkDir(p);
		File f;
		while((f = walk.next()) != null) { fn.accept(f); }
	}

	/** Display a map to stdout */
	static void displayMap(Map<?, ?> map) {
		for(Map.Entry e : map.entrySet()) System.out.println(e.getKey() + " -> " + e.getValue());
	}

	/** Test if a character is alphabetic (a-z, A-Z) */
	static boolean isAlphabetic(char c) {
		return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
	}

	/** Wrapping add for alphanumeric characters */
	static char wrap(char c, int offset) {
		boolean upper = Character.isUpperCase(c);
		c += offset;
		if(upper && c > 90) c -= 26;
		else if(!upper && c > 122) c -= 26;
		return c;
	}

	/** Calculate a cost distribution from a frequency map */
	static <T> Map<T, Double> relativeFreq(Map<T, Integer> map) {
		double total = (double) map.values().stream().mapToInt(i -> i).sum();
		return map.entrySet().stream()
				  .map(e -> new Pair<>(e.getKey(), (double) e.getValue() / total))
				  .collect(Collectors.toMap(p -> (T) p.first, p -> (Double) p.second));
	}

	/** Recursively visits files in a directory */
	static class WalkDir {
		private List<File> dirs = new ArrayList<>();

		WalkDir(File dir) { dirs.add(dir); }

		/** Get the next file */
		File next() {
			if(dirs.isEmpty()) return null;
			File f = dirs.remove(dirs.size() - 1);
			if(f.isDirectory()) { Collections.addAll(dirs, f.listFiles()); }
			return f;
		}
	}

	/** A pair of values; A class which is missing from the standard library...
	 *  Implements `Comparable`, though compareTo(..) will fail if the inner types
	 *  don't also implement comparable. */
	static class Pair<A, B> implements Comparable<Pair<A,B>>{
		final A first;
		final B second;

		Pair(A first, B second) {
			this.first = first;
			this.second = second;
		}

		@Override
		public int hashCode() {
			return first.hashCode() ^ second.hashCode();
		}

		@Override
		public boolean equals(Object o) {
			return o instanceof Pair &&
				  this.first.equals(((Pair) o).first)
				  && this.second.equals(((Pair) o).second);
		}

		@Override
		public int compareTo(Pair<A, B> other) {
			if (this.first instanceof Comparable && this.second instanceof Comparable) {
				int a = ((Comparable<A>) this.first).compareTo(other.first);
				if (a == 0) return ((Comparable<B>)this.second).compareTo(other.second);
				else return a;
			} else return 0;
		}
	}
}
