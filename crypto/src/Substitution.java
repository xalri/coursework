package uk.xa1.crypto;

import java.io.*;
import java.util.*;
import java.util.function.BiPredicate;

/** Breaking monoalphabetic substitution ciphers */
public class Substitution {

	/** Decrypts ciphertext assuming it's encoded with a substitution cipher */
	public static class Solve {
		public static void main(String[] args) {
			if (args.length < 1) System.err.println("Missing argument");
			try {
				File source = new File(args[0]);
				CharacterFreq.Profile p = CharacterFreq.Profile.analyseFile(source);
				Solver solver = new Solver();
				solver.applyProfile(p);
				Map<Character, Character> match = solver.match();
				Util.displayMap(match);

				InputStream s = new BufferedInputStream(new FileInputStream(source));

				Util.readToEnd(s, c -> {
					if (Util.isAlphabetic(c)) c = match.get(Character.toLowerCase(c));
					System.out.print(c);
				});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/** Decrypts ciphertext assuming it's encoded with a caeser/shift cipher */
	public static class SolveShift {
		public static void main(String[] args) {
			if (args.length < 1) System.err.println("Missing argument");
			try {
				File source = new File(args[0]);
				CharacterFreq.Profile p = CharacterFreq.Profile.analyseFile(source);
				Solver solver = new Solver();
				solver.applyProfile(p);
				int shift = solver.matchShift();
				System.out.println(shift);
				System.out.println();

				InputStream s = new BufferedInputStream(new FileInputStream(source));

				Util.readToEnd(s, c -> {
					if (Util.isAlphabetic(c)) c = Util.wrap(c, shift);
					System.out.print(c);
				});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/** Attempts to find a character map for data encoded using any substitution
	 * cipher. */
	static class Solver {
		private double[][] m = new double[26][26];
		// Use english data by default
		CharacterFreq.Data data = CharacterFreq.Data.ENGLISH;

		/** Add some measure of confidence that a maps to b */
		void guess(char a, char b, double x) {
			m[a - 97][b - 97] += x;
		}

		/** Populate the matrix based on a profile */
		void applyProfile(CharacterFreq.Profile p) {
			// For each character calculate a measure of confidence that it matches
			// to every other character using a known mean and variance.
			for(char i = 'a'; i < 'z'; i++) {
				for(char j = 'a'; j < 'z'; j++) {
					double x = measure(p.charRelative.get(i), data.charAvr(j), data.charVar(j));
					guess(i, j, x);
				}
			}

			// The same for bigrams, e.g. "ab" -> "cd" adds to the confidence
			// that 'a' -> 'c' and 'b' -> 'd'.
			for(Map.Entry<String, Double> e : p.bigramRelative.entrySet()) {
				String bigram = e.getKey();
				double freq = e.getValue();
				// Limit to those which we have data to match to. Going any lower than this
				// is counter-productive since the frequencies are so close.
				if (freq < 0.003) continue;
                for(Util.Pair<Double, String> f : data.sortedBigrams()) {
                    String to = f.second;
                	  double avr = f.first;
						  double var = data.bigramVar(to);
                    double x = measure(freq, avr, var);
                    guess(bigram.charAt(0), to.charAt(0), x);
                    guess(bigram.charAt(1), to.charAt(1), x);
                }
			}
		}

		/** Calculate the most likely matching */
		Map<Character, Character> match() {
			// Solve for the maximum matching using the hungarian algorithm.
			int[] matching = new Hungarian(m).solve();

			// Convert the array to a map of characters.
			Map<Character, Character> map = new HashMap<>();
			for(char c = 'a'; c <= 'z'; c++) {
				map.put(c, (char)(matching[c - 97] + 97));
			}

			return map;
		}

		/** Calculate the most likely shift assuming a Ceaser cipher */
		int matchShift() {
			double max = 0;
			int shift = 0;
			for(int i = 0; i < 26; i++) {
				double sum = 0;
				for(int j = 0; j < 26; j++) sum += m[j][(j+i) % 26];
				if(sum > max) {
					max = sum;
					shift = i;
				}
			}
			return shift;
		}

		/** A measure of confidence in a match based on the normal distribution. */
		private static double measure(double x, double mean, double var) {
			double a = 1 / (Math.sqrt(2 * Math.PI * var));
			double b = Math.pow(Math.E, -(Math.pow(x - mean, 2.0)) / (2 * var));
			return a * b;
		}
	}

	/** Calculates the maximal matching for a complete weighted bipartite graph
	 *  using the hungarian algorithm. Used to find the most probable mapping
	 *  between two sets of characters for any monoalphabetic substitution */
	static class Hungarian {
		private double[][] weight;
		private double[] row;
		private double[] col;
		private int n;

		Hungarian(double[][] weight) {
			this.n = weight.length;
			this.weight = weight;
			this.row = new double[n];
			this.col = new double[n];

			for(int i = 0; i < n; i++)
				for(int j = 0; j < n; j++)
					if(weight[i][j] > row[i]) row[i] = weight[i][j];
		}

		private boolean edge(int x, int y) {
			return Math.abs(row[x] + col[y] - weight[x][y]) < 0.00001;
		}

		int[] solve() {
			while (true) {
				int[] match = unweighted(n, this::edge);
				if(is_solved(match)) return match;

				int[] inv = flip(match);

				boolean[] marked_rows = new boolean[n];
				boolean[] marked_cols = new boolean[n];
				Queue<Integer> queue = new LinkedList<>();

				for(int x = 0; x < n; x++){
					if(match[x] == -1) {
						marked_rows[x] = true;
						queue.add(x);
						break;
					}
				}

				while(!queue.isEmpty()) {
					int x = queue.remove();

					for(int y = 0; y < n; y++) {
						if(edge(x, y)) {
							marked_cols[y] = true;
							if(!marked_rows[inv[y]]) {
								marked_rows[inv[y]] = true;
								queue.add(inv[y]);
							}
						}
					}
				}

				double delta = Double.MAX_VALUE;
				for(int x = 0; x < n; x++){
					if(marked_rows[x]) {
						for (int y = 0; y < n; y++) {
							if (!marked_cols[y] && row[x] + col[y] - weight[x][y] < delta) {
								delta = row[x] + col[y] - weight[x][y];
							}
						}
					}
				}

				for(int x = 0; x < n; x++) {
					if (marked_rows[x]) row[x] -= delta;
				}

				for(int y = 0; y < n; y++) {
					if (marked_cols[y]) col[y] += delta;
				}
			}
		}

		/** Calculates the maximum matching for an unweighted bipartite graph */
		static int[] unweighted(int n, BiPredicate<Integer, Integer> edge) {
			int[] match = new int[n];
			Arrays.fill(match, -1);
			for (int y = 0; y < n; y++) {
				boolean[] tested = new boolean[n];
				search(n, edge, match, tested, y);
			}
			return match;
		}

		private static boolean search(int n, BiPredicate<Integer, Integer> edge, int[] match, boolean[] tested, int y) {
			for (int x = 0; x < n; x++) {
				if (edge.test(x, y) && !tested[x]) {
					tested[x] = true;
					if (match[x] < 0 || search(n, edge, match, tested, match[x])) {
						match[x] = y;
						return true;
					}
				}
			}
			return false;
		}

		private boolean is_solved(int[] match) {
			return Arrays.stream(match).noneMatch(a -> a == -1);
		}

		private static int[] flip(int[] match) {
			int[] inv = new int[match.length];
			Arrays.fill(inv, -1);
			for(int i = 0; i < match.length; i++) if (match[i] != -1) inv[match[i]] = i;
			return inv;
		}
	}
}
