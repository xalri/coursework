#!/bin/bash
export OUT_DIR=./out

mkdir -p $OUT_DIR
javac \
   -cp out:$(find ./lib -name "*.jar" | tr "\n" ":") \
   -d $OUT_DIR \
   $(find ./ -name "*.java")
