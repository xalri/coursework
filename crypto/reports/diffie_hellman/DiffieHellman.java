package uk.xa1.crypto;

import java.math.BigInteger;
import java.security.SecureRandom;

public class DiffieHellman {
	/** Parameters for DH key exchange */
	public static class Params {
		BigInteger prime;
		BigInteger generator;

		/** Construct `Params` with the given prime and generator */
		public Params(BigInteger p, BigInteger g) {
			prime = p;
			generator = g;
		}

		/** Params for the 2048 bit group defined in RFC3526 */
		public static Params rfc3526_2048() {
			return new Params(
				  new BigInteger(
						"FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD1" +
						"9024E088A67CC74020BBEA63B139B22514A08798E3404DD" +
						"F9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245" +
						"485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7ED" +
						"E386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3D" +
						"2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F" +
						"3655D23DCA3AD961C62F356208552BB9ED529077096966D" +
						"70C354E4ABC9804F1746C08CA18217C32905E462E36CE3B" +
						"39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9" +
						"DE2BCBF6955817183995497CEA956AE515D2261898FA0510" +
						"15728E5A8AACAA68FFFFFFFFFFFFFFFF", 16),
					new BigInteger("2", 16)
			);
		}
	}

	public final Params params;
	/** Our part of the key */
	public final BigInteger a;
	/** The value to share with the other party, g^a */
	public final BigInteger A;

	public DiffieHellman(Params params) {
		this.params = params;
		SecureRandom rng = new SecureRandom();
		byte[] bytes = params.prime.toByteArray();
		rng.nextBytes(bytes);
		this.a = new BigInteger(bytes).mod(params.prime);
		this.A = params.generator.modPow(a, params.prime);
	}

	public BigInteger sharedKey(BigInteger B) {
		return B.modPow(a, params.prime);
	}

	public static void main(String[] args) {
		normalDH();
	}

	public static void normalDH() {
		Params params = Params.rfc3526_2048();
		DiffieHellman alice = new DiffieHellman(params);
		System.out.println("secretA = " + alice.a);

		DiffieHellman bob = new DiffieHellman(params);
		System.out.println("secretB = " + bob.a);

		System.out.println("msg1.modulus = " + params.prime);
		System.out.println("msg1.base = " + params.generator);
		System.out.println("msg1.valueA = " + alice.A);
		System.out.println("msg2.valueB = " + bob.A);

		BigInteger sa = alice.sharedKey(bob.A);
		BigInteger sb = bob.sharedKey(alice.A);

		System.out.println("keyA = " + sa);
		System.out.println("keyB = " + sb);
	}
}
