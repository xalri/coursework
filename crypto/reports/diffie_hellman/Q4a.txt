If the key exchange is done without authentication then a man-in-the-middle
(MITM) attack is trivial.

We'll call the adversary Mallory. Mallory intercepts messages from Alice and
pretends to be Bob, completing the key exchange as usual giving her a key A.
Mallory simultaneously performs a key exchange with Bob (pretending to be
Alice), giving her a second key B.

Now for any messages sent between Alice and Bob, Mallory can decrypt the
message with the corresponding key, read or modify it arbitrarily, then
re-encrypt it with the other key and forward it on.
