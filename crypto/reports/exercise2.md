---
title: "Breaking polyalphabetic ciphers"
author: "Lewis Hallam"
geometry: margin=2.5cm
output: pdf_document
---

## Implementing the Vigenere cipher

The `Vigenere.Stream` class implements en/decoding of vigenere ciphertext
by wrapping an `InputStream` and en/decoding characters as they're read.
We store an offset which is incremented with each character, and is used to
select which character of the password is used. The `read()` method calls
`read()` on the inner stream, then if the character given is alphabetic
shifts it by `password[offset]` and increments the offset, wrapping at the
password length. The password is stored as an array of offsets, so `Stream`
can be used for both encoding and decoding. The static methods
`Stream.encode()` and `Stream.decode()` set up the stream, the latter
inverting the password.


The `Encode` and `Decode` classes provide a command line interface for
`Stream`. The file path and password are passed as arguments, and the
output is printed to the console.

    java -cp out 'uk.xa1.crypto.Vigenere$Encode' ./file password 
    java -cp out 'uk.xa1.crypto.Vigenere$Decode' ./file password

## Character frequency distribution

The character distribution for vigenere encoded text differs significantly
from that of english text or text encoded with monoalphabetic substitution ciphers.
The character frequency distribution for the given
ciphertext is shown in Figure 1.
We see that the most and least common letters differ by a factor of 10,
rather than the factor of over 100 expected for english text. 
   
    $ java -cp out 'uk.xa1.crypto.CharacterFreq$Analyse' ./data/ex2_crypto
    
![Vigenre character distribution](images/vigenere_freq.png){ width=400px }

## Cryptanalysis of Vigenere encoded text

My solution to the cryptanalysis in this exercise builds on the code
written for exercise 1.

Since the password is repeated, for a password length `n` the offset of
every `n`th character is the same. We can split the text into `n`
different texts, where the first character goes to the first text,
 the second character to the second, and so on, wrapping after `n`
characters, then solve each one with the caeser cipher solver
written for the first exercise. Combining the offsets in sequence gives
the password used to encode the text.

### The implementation

First, a method is added to `CharacterFreq.Profile` to calculate the index
of coincidence -- the sum of the squares of the relative frequencies.
This gives a measure of the chance of two randomly selected characters being
the same, which is much higher for english text than random text. The index
of coincidence in english is ~0.065. Note that a substitution cipher has
no effect on the index of coincidence, the _set_ of values for the relative
frequencies are the same, they just map to different character, so the sum
of their squares is also unchanged.

A new class is added, `Vigenere.Solver.Split`, which takes a value `n` in
it's constructor and creates `n` `Profiles`. Characters passed to the `Split`
are forwarded to the profiles in sequence, the first character goes to the first profile,
the second to the second profile, and so on, wrapping around after `n`
characters. We skip incrementing the counter if the character isn't counted, though
it isn't required for this exercise since non-alphabetic characters were removed
from the ciphertext.

`Split` has a method `indexOfCoincidence` which returns the average
of the index of coincidence values given by it's profiles, and a method `solve`
which, assuming the key length is `n`, uses `Substitution.Solver` to get an
offset for each of it's profiles, and combines these offsets in sequence to
build the password. 

Finally, the class `Vigenere.Solver` contains a collection of `Splits` with values
of `n` from 1 to a statically defined constant. Each character given to the `Solver`
is forwarded to all of the splits. The method `Solver.solve()` looks for the first
`Split` with an index of coincidence greater than `0.62` then calls `Split.solve()`
on that split to construct the password.

#### Solving the given ciphertext

Not much experimentation was needed for this problem -- the initial attempt used
the `Split` with the highest index of coincidence, but while this correctly decodes
the text, it gives the password as a long repeating string (`platoplatoplato...`)
which isn't ideal.

Using the first split with an index of coincidence over a threshold
of `0.062` (chosen for being slightly below the average for english text,
a more optimal threshold could be found by testing more texts) fixes
this problem, correctly giving the password as `plato`.

    $ java -cp out 'uk.xa1.crypto.Vigenere$Solve' ./data/ex2_crypto                                                                                                                                                         ~/c/crypto master
    password: plato
    decoded: 
    neithermustweforgetthattherepublicisbutthethirdpartofastilllargerdesign...

The full text is saved as `data/ex2_plain`