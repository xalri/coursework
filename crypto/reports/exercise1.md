---
title: "Exploiting non-uniform character distribution in cryptanalysis"
author: "Lewis Hallam"
geometry: margin=2.2cm
output: pdf_document
---

In english text some characters occur more often than others. The most common
letter `e` is seen more than 100x more often than the least common `z`. The
same applies to n-grams -- sequences of `n` characters inside words.
Of the bigrams (two letters next to each other) "th" and "he" are the most
common. Knowledge of the distributions of these characters and n-grams can
be exploited to break some traditional text-based ciphers.

## Analysis of a corpus of texts

The class `CharacterFreq.Profile` analyses the relative frequency of characters
and bigrams for some stream of characters. `CharacterFreq.Analyse` is a
console interface to this, running the profiler on a text file given
as a command line parameter.

    $ java -cp out 'uk.xa1.crypto.CharacterFreq$Analyse' ./data/plain_a
    
The relative character frequencies in a single text isn't necessarily
representitive of the distribution in all texts, so we need more data.
`CharacterFreq.Data.Analyse` analyses the character distribution for a
large corpus of texts.

For each text the frequencies of single letters 
and bigrams are counted then normalised, giving the relative frequencies
in each text. Every text seems to give a slightly different distribution,
I found that modeling frequencies using the normal distribution gave
better results than just summing the frequencies of all the texts, so
after finding the relative frequencies for every text in the corpus
we calculate the mean and variance for each letter and bigram.

The program outputs the data as java code which constructs
a hashmap of the data, to be used for comparisons to data from ciphertext.
For the cryptanalysis I ran this program against the open american national
corpus (OANC) and added the generated data as `Data.ENGLISH`.

    $ java -cp out 'uk.xa1.crypto.CharacterFreq$Data$Analyse' ./corpus

### Results

Figure 1 shows the relative frequency & variance of each letter for
the open american national corpus.

![OANC character distribution](images/chart.png){ width=600px }

The letter `e` is shown to occur far more frequently than any other, and the
bottom four far less frequently. Between these extremes the changes are gradual.
There are no obvious patterns in the variance, though the differences seem
significant. Perhaps some other model would better show the differences in
letter frequencies across texts.

A comparison can be made to the letter frequency data on Wikipeida.
The positions of the first 7 characters are the same, but after that
many characters differ by a few places. The values
themselves are relatively similar.

## Cryptanalysis

Assuming the given ciphertext is encrypted using a monoalphabetic
substitution cipher, to break it we must find a matching between
character sets which when applied to the ciphertext produces the plaintext.
`CharacterFreq.Solver` implements one method of finding this matching.
 
### An algorithm for breaking monoalphabetic substitution ciphers

We visualise the problem as a bipartite graph, with the set of characters
in the ciphertext on one side and the set of characters in the plain text
on the other. The graph is complete, meaning any character could map to any
other. In code the graph is represented as a square matrix `m`, where
`m[a][b]` gives the weight of the edge connecting `a` and `b`.

Using what we know about character distributions we can add weights to the
graph representing how likely it is that each edge belongs to the mapping
from the ciphertext to the plaintext.
For example, if the letter 'q' in the ciphertext makes up about 13%
of the characters, it's quite likely that it maps to 'e' in the plain text
since 'e' in english text has about the same relative frequency, so we give
the edge `q -> e` a high weight. Conversely, 'q' is very unlikely to map to
'z', so we give the edge `q -> z` a low weight.

We can calculate these weights using the probability distribution function 
for the normal distribution. For two characters `a` and `b`, the probability
that `a` in the ciphertext maps to `b` in the plain text is given by
 
```normal(relativeFreq[a], mean[b], var[b])```

where `relativeFreq[a]` gives the relative frequency of character `a`
in the ciphertext and `mean[b]` and `var[b]` are the mean and variance
respectively of character `b` calculated from a corpus of english text.

After weights are assigned to every edge existing algorithms can be
used to calculate the _maximum matching_, the set of edges where every
character maps to only one other which gives the greatest sum of the
weights. I decided on the hungarian algorithm, for being relatively 
simple to implement.

The accuracy of the algorithm can be increased by also considering
bigrams. For each pair of bigrams we add the probability that they
match to both of the edges which make up the bigram. For example
if "ex" in the ciphertext has a similar relative frequency to "th"
in english text, we increase the weights of `e -> t` and `x -> h`.

This algorithm isn't the best use of the data, since we discard
information about the _connection_ between the two characters in
a bigram. It would also be nice to generalise to n-grams, rather
than only using bigrams. 

#### Decoding the given ciphertext

Initial attempts used various early iterations of the above algorithm,
notably a few different measures were used before trying the normal
distribution , including the difference between the relative frequencies
and the ratio between the relative frequencies, but neither made the text
much more legible. I tested the algorithm against a long english text and
counted the number of characters it correctly matched
(` CharacterFreqTest.matchEnglish`). Initial versions gave ~10 correct
characters , the most recent iteration correctly matches 24 characters.

When I was relatively happy with the results from testing on english
text I tried again to decode the given ciphertext:
 
    $ java -cp out 'uk.xa1.crypto.Substitution$Solve' ./data/ex1_cipher
    
    "coeuarely."

    "then yid msy entaoely oely in me."

    "thst ar ejuellent. a thanz, ceohscr, at ar slmirt tame thst a
    coecsoe fio the new oile a hsve ti clsy."

The result still isn't perfect but a few words become obvious;
"ejuellent" is most likely "excellent", "thst" becomes "that", and
from context the "ar" between them is probably "is".

I substituted characters manually, adding if statements in the loop
which decodes and prints characters.
After printing the mapping generated by the program and starting to
combine that with my manual changes I noticed that the every character
was offset by the same amount, indicating that the text was encrypted
with a simple shift cipher. 

#### Updating the algorihtm for a shift cipher

Knowing now that the ciphertext was encrypted with a shift cipher, a
simple change can be made to the above algorithm to give the offset we
should use to decode it. After building the matrix `m` we calculate
the sums of it's 26 diagonals:

 `diagonal[n] = sum(m[i][(i + n) % 26])`

Since a caeser cipher shifts every character by the same value, the `n`th
diagonal gives the mapping for a caeser cipher with shift `n`. By calculating
the sum of the values in the diagonals we get a measure of likelyhood of
every possible shift, from which we pick the largest to get the offset
to use to decode the text .
 
Running this matcher on the given ciphertext we get the calculated offset (22)
and the decoded text. The offset used to _encode_ the text would be 
the inverse of the offset calculated to decode it, so the given ciphertext
was encoded by shifting each character 4 places.

    $ java -cp out 'uk.xa1.crypto.Substitution$SolveShift' ./data/ex1_cipher
    22

    "precisely."

    "then you may entirely rely on me."

    "that is excellent. i think, perhaps, it is almost time that i
    prepare for the new role i have to play."


The full decoded text is saved as `data/ex1_plain`